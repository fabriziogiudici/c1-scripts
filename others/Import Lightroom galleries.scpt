use framework "Foundation"
use scripting additions

global DEBUG, CRLF
set DEBUG to true

tell application "Finder" to set _myPath to container of (path to me) as alias
my trace("My path: " & _myPath)
# no debug when running inside Capture One, too much stuff stalls the script
set DEBUG to _myPath as string does not contain ("Library" & ":" & "Scripts")
set CRLF to "
"

set _collection to missing value
set _brokenImageNames to {}
set _newImagePaths to {}
set _currentVariants to {}

tell front document of application "Capture One 20"

	set _reallyAllVariants to every variant in collection named "All"
	set _reallyAllVariantNames to the name of every variant in collection named "All"

	set _filters to collection named "Filters"
	tell _filters to set _workflow to collection named "Workflow"
	tell _workflow to set _published to collection named "Published (green label)"
	tell _published to set _allVariants to collection named "All"

	set _currentVariants to {}
	set _newImagePaths to {}

	try
		set _baseImport to make collection with properties {kind:group, name:"__IMPORT__"}
	on error
		set _baseImport to collection named "__IMPORT__"
	end try

	tell application "Capture One 20"
		my trace("Getting all the image names...")
		set progress total units to 1
		set progress completed units to 0
		set progress text to "Getting all images..."

		tell _allVariants
			set _images to every variant
			set _imageNames to the name of every variant
		end tell

		set progress completed units to 1
	end tell

	set _srcFile to "macOs:tmp:collections.txt"
	tell application "System Events" to set _lns to paragraphs of (read file _srcFile as «class utf8»)

	tell application "Capture One 20"
		set progress total units to the count of _lns
		set progress completed units to 0
		set progress text to "Processing"
	end tell

	repeat with _ln in _lns
		if _ln starts with " " then
			set _imageName to my trim(_ln)
			--my trace("Searching for " & _imageName)
			set _i to my indexOf(_imageName, _imageNames)

			if (_i ≤ 0) then
				my trace("WARNING: NOT FOUND " & _imageName)
				set _baseImageName to text 1 thru 13 of _imageName
				-- find the original image
				set _i to my indexOf(_baseImageName, _reallyAllVariantNames)

				if (_i ≤ 0) then
					my trace("ERROR: EVEN THE BASE IMAGE NOT FOUND " & _baseImageName)
					set _brokenImageNames to _brokenImageNames & _imageName
				else
					set _baseImage to (item _i of _reallyAllVariants)
					set _baseImageFile to file of _baseImage
					tell application "Finder" to set _baseImageFolder to ((container of (_baseImageFile as alias)) as string)
					set _baseImagePath to _baseImageFile as string
					set _baseImageExtension to text -3 thru -1 of _baseImagePath
					set _destinationFile to _baseImageFolder & _imageName & "." & _baseImageExtension
					--					my trace("BASE IMAGE PATH " & _baseImagePath)
					--					my trace("BASE IMAGE EXTENSION " & _baseImageExtension)
					--					my trace("BASE IMAGE FOLDER " & _baseImageFolder)
					my trace("DESTINATION FILE " & _destinationFile)

					my copyFile("/" & my replace_chars(_baseImageFile as text, ":", "/"), "/" & my replace_chars(_destinationFile as text, ":", "/"))

					set _baseXmpFile to my replace_chars(_baseImagePath, _baseImageExtension, "xmp")
					set _destinationXmpFile to _baseImageFolder & _imageName & ".xmp"
					my copyFile("/" & my replace_chars(_baseXmpFile as text, ":", "/"), "/" & my replace_chars(_destinationXmpFile as text, ":", "/"))

					set _newImagePaths to _newImagePaths & {_destinationFile as alias}

					-- tag green -- I think you can't, because if Capture One writes metadata screwes up hierarchical tags
					-- rather add to another collection to be fixed manually
				end if
			else
				set _image to (item _i of _images)
				set _currentVariants to _currentVariants & {_image}
			end if
		else
			if (count of _currentVariants) > 0 then
				tell application "Capture One 20" to add inside _collection variants _currentVariants
				set _currentVariants to {}
			end if

			if (count of _ln) is 0 then
				tell application "Capture One 20" to set progress completed units to progress completed units + 1
				exit repeat
			end if

			my trace("CREATE COLLECTION " & _ln & "/ " & (count of _ln))

			if _ln does not start with "Stopping Down" then set _ln to "Stopping Down:" & _ln

			set _collection to _baseImport
			set _month to missing value

			repeat until false
				set _colon to offset of ":" in _ln
				if _colon is 0 then
					set _name to _ln
					if (_month is not missing value) then
						set _name to _month & "/" & _name
					end if
					tell _collection to set _subCollection to make collection with properties {kind:album, name:_name}
					tell application "Capture One 20" to set progress text to "Processing " & _name
					set _collection to _subCollection
					exit repeat
				end if

				set _item to text 1 thru (_colon - 1) of _ln
				set _ln to text (_colon + 1) thru -1 of _ln
				--				my trace("  ITEM " & _item)

				if _item ends with "January" then
					set _month to "01"
				else if _item ends with "February" then
					set _month to "02"
				else if _item ends with "March" then
					set _month to "03"
				else if _item ends with "April" then
					set _month to "04"
				else if _item ends with "May" then
					set _month to "05"
				else if _item ends with "June" then
					set _month to "06"
				else if _item ends with "July" then
					set _month to "07"
				else if _item ends with "August" then
					set _month to "08"
				else if _item ends with "September" then
					set _month to "09"
				else if _item ends with "October" then
					set _month to "10"
				else if _item ends with "November" then
					set _month to "11"
				else if _item ends with "December" then
					set _month to "12"
				else
					if _item is "With Lightroom" then set _item to "Diary"

					try
						tell _collection to set _subCollection to collection named _item
					on error
						tell _collection to set _subCollection to make collection with properties {kind:group, name:_item}
					end try
					set _collection to _subCollection
				end if
			end repeat
		end if

		tell application "Capture One 20" to set progress completed units to progress completed units + 1

	end repeat

	if (count of _currentVariants) > 0 then
		tell application "Capture One 20" to add inside _collection variants _currentVariants
	end if

	if (count of _brokenImageNames) > 0 then
		my writeToFile(my list2String(_brokenImageNames, CRLF), "macOS:tmp:missingImages.txt")
	end if

	if (count of _newImagePaths) > 0 then
		my trace("IMPORTING " & _newImagePaths)
		display notification _newImagePaths as string
		repeat with _newImagePath in _newImagePaths
			my trace("IMPORTING " & _newImagePath)
			--			tell application "Capture One 12" to import front document of application "Capture One 12" source _newImagePath
		end repeat
		tell application "Capture One 20" to import front document of application "Capture One 20" source _newImagePaths
	end if
end tell

on copyFile(_sourcePath, _destPath)
	set _bash to "cp -fv \"" & _sourcePath & "\" \"" & _destPath & "\""
	my trace("BASH " & _bash)
	do shell script _bash
end copyFile

on indexOf2(_item, _list)
	set _left to 1
	set _right to count of _list

	repeat while _left ≤ _right
		set _middle to round ((_left + _right) / 2) -- FIXME should be floor
		set _check to item _middle of _list

		if _check < _item then
			set _left to _middle + 1
		else if _check > _item then
			set _right to _middle - 1
		else
			return _middle
		end if
	end repeat

	return 0
end indexOf2

on indexOf(theItem, theList) -- credits Emmanuel Levy
	set oTIDs to AppleScript's text item delimiters
	set AppleScript's text item delimiters to return
	set theList to return & theList & return
	set AppleScript's text item delimiters to oTIDs
	try
		-1 + (count (paragraphs of (text 1 thru (offset of (return & theItem & return) in theList) of theList)))
	on error
		0
	end try
end indexOf

on trim(txt)
	repeat with i from 1 to (count txt) - 1
		if (txt begins with space) then
			set txt to text 2 thru -1 of txt
		else
			exit repeat
		end if
	end repeat
	repeat with i from 1 to (count txt) - 1
		if (txt ends with space) then
			set txt to text 1 thru -2 of txt
		else
			exit repeat
		end if
	end repeat
	if (txt is space) then set txt to ""

	return txt
end trim

--
-- Writes a text to a file.
--
-- _string:			the text
-- _path:				the path of the file
--
on writeToFile(_string, _path)
	trace("Writing to " & _path & ": " & _string)
	tell application "System Events"
		#	try
		set _file to open for access file _path with write permission
		set eof of the _file to 0
		write _string to the _file starting at eof as «class utf8»
		close access _file
		(*
	on error _error
		try
			log (_error & " writing file " & _path)
			close access file _path
		end try
	end try*)
	end tell
end writeToFile

on list2String(theList, theDelimiter)
	set theBackup to AppleScript's text item delimiters
	set AppleScript's text item delimiters to theDelimiter
	set theString to theList as string
	set AppleScript's text item delimiters to theBackup

	return theString
end list2String

--
on replace_chars(this_text, search_string, replacement_string)
	set theBackup to AppleScript's text item delimiters
	set AppleScript's text item delimiters to the search_string
	set the item_list to every text item of this_text
	set AppleScript's text item delimiters to the replacement_string
	set this_text to the item_list as string
	set AppleScript's text item delimiters to theBackup
	return this_text
end replace_chars

--
-- Checks whether a file exists.
--
-- _file:				the file
--
on fileExists(_file)
	tell application "System Events" to return exists file _file
end fileExists

--
-- Logs a message for debugging purposes.
--
-- _message: 			the message to log
--
on trace(message)
	if DEBUG then
		log ">>>> " & message
	end if
end trace

--	set _imageNames to items 1 through 30 of _imageNames

(*
	my trace("Indexing images...")
	tell application "Capture One 12"
		set progress total units to the count of _imageNames
		set progress completed units to 0
		set progress text to "Computing the image map"
	end tell

	set _imageMap to {}
	repeat with _i from 1 to count of _imageNames
		set _imageName to item _i in _imageNames
		set _imageMap to _imageMap & {{key:_imageName, value:_i}}
		tell application "Capture One 12" to set progress completed units to progress completed units + 1
		--		my trace("" & _imageId & " -> " & _imageName)
	end repeat
	*)

--	set _z to the _value of every record in _imageMap whose key is "20040102-0075"
--	my trace("Z " & z)

--	tell _imageMap to set _x to the value of _imageMap whose key is "20040102-0075"
--	my trace("XXXX " & _x)

--	return

--			set _image to (first variant of (_images whose name is _imageName))
--tell application "Capture One 12" to get (the variant whose name is _imageName)

-- tell application "Capture One 12" to set _image to (the variant whose name is _imageName)
--			set _image to (_imageNames whose name is _imageName)

(*
			set _image to missing value
			repeat with _i from 1 to count of _imageNames
				if ((item _i of _imageNames) as string) is _imageName then
					set _image to (item _i of _images)
					--					set _ii to (value of _imageMap whose key is _imageName)
					--					my trace("FOUND AT INDEX " & _i & " vs " & _ii)
					exit repeat
				end if
			end repeat
			*)




-- Scripts for Capture One
-- ===============
-- 
-- Copyright (C) 2019-2021 by Tidalwave s.a.s. (http://tidalwave.it)
-- 
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
-- 
--     http://www.apache.org/licenses/LICENSE-2.0
-- 
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.

--
-- Creates a set of smart albums which filter by keywords starting from the exported catalog of PhotoSupreme.
--

global DEBUG, DRY_RUN, EXCLUDED_GROUPS, CRLF, CLASSES_CHURCH, CLASSES_CASTLE, CLASSES_BRIDGE, CLASSES_LIGHTHOUSE, CLASSES_INHABITED_PLACE, CLASSES_MOUNTAIN, CLASSES_RIVER, CLASSES_LAKE, CLASSES_SEA, CLASSES_ISLAND, CLASSES_VALLEY, _dump

on run argv
	set EXCLUDED_GROUPS to {"Events", "Objects", "Styles", "Workflow", "AI Labels"}
	set DEBUG to true
	set DRY_RUN to (argv contains "--dry-run")
	set CRLF to "
"
	set _dump to ""
	
	set CLASSES_CHURCH to {"church", "monastery", "abbey", "cathedral", "baptistery", "basilica", "chapel", "hermitage", "oratory", "priory"}
	set CLASSES_CASTLE to {"castle", "fort", "citadel", "fortification", "tower"}
	set CLASSES_BRIDGE to {"bridge"}
	set CLASSES_LIGHTHOUSE to {"lighthouse"}
	set CLASSES_INHABITED_PLACE to {"town", "village", "hamlet"}
	
	set CLASSES_MOUNTAIN to {"mountain", "massif", "mountain range", "mountain pass"}
	set CLASSES_VALLEY to {"valley", "plateau", "canyon"}
	set CLASSES_RIVER to {"river", "creek", "canal"}
	set CLASSES_LAKE to {"lake"}
	set CLASSES_SEA to {"sea"}
	set CLASSES_ISLAND to {"island"}
	
	set _photoSupremeCatalogExport to ((path to home folder) as text) & "Personal:Photography:PhotoSupreme:Output:Catalog.utf8"
	my trace("Processing " & _photoSupremeCatalogExport)
	
	set _folder to my createGroup(missing value, "Catalog " & my currentDateTime())
	
	if DRY_RUN then
		set _folder to "Catalog" -- no timestamp for test comparison
	end if
	
	set _biodiversityFolder to my createGroup(_folder, "Biology")
	set _vernacularNamesFolder to my createGroup(_biodiversityFolder, "Vernacular names")
	set _plantsVernacularNamesFolder to my createGroup(_vernacularNamesFolder, "Plants")
	set _animalsVernacularNamesFolder to my createGroup(_vernacularNamesFolder, "Animals")
	set _invertebratesVernacularNamesFolder to my createGroup(_animalsVernacularNamesFolder, "Invertebrates")
	set _butterfliesVernacularNamesFolder to my createGroup(_invertebratesVernacularNamesFolder, "Butterflies")
	set _amphibiansVernacularNamesFolder to my createGroup(_animalsVernacularNamesFolder, "Amphibians")
	set _reptilesVernacularNamesFolder to my createGroup(_animalsVernacularNamesFolder, "Reptiles")
	set _birdsVernacularNamesFolder to my createGroup(_animalsVernacularNamesFolder, "Birds")
	set _mammalsVernacularNamesFolder to my createGroup(_animalsVernacularNamesFolder, "Mammals")
	
	set _architectureFolder to my createGroup(_folder, "Architecture")
	set _churches to my createGroup(_architectureFolder, "Churches and other religious buildings")
	set _castles to my createGroup(_architectureFolder, "Castles and other military buildings")
	set _bridges to my createGroup(_architectureFolder, "Bridges")
	set _lighthouses to my createGroup(_architectureFolder, "Lighthouses")
	set _inhabitedPlaces to my createGroup(_architectureFolder, "Inhabited places")
	
	set _geographyFolder to my createGroup(_folder, "Geography")
	set _mountains to my createGroup(_geographyFolder, "Mountains")
	set _rivers to my createGroup(_geographyFolder, "Rivers")
	set _lakes to my createGroup(_geographyFolder, "Lakes")
	set _valleys to my createGroup(_geographyFolder, "Valleys, canyons and plateaux")
	set _seas to my createGroup(_geographyFolder, "Sea")
	set _islands to my createGroup(_geographyFolder, "Islands")
	
	set _vernacularFolders to {Plants:_plantsVernacularNamesFolder, Animals:_animalsVernacularNamesFolder, Invertebrates:_invertebratesVernacularNamesFolder, Butterflies:_butterfliesVernacularNamesFolder, Amphibians:_amphibiansVernacularNamesFolder, Reptiles:_reptilesVernacularNamesFolder, Birds:_birdsVernacularNamesFolder, Mammals:_mammalsVernacularNamesFolder, Churches:_churches, Castles:_castles, Bridges:_bridges, Lighthouses:_lighthouses, InhabitedPlaces:_inhabitedPlaces, Mountains:_mountains, Rivers:_rivers, Lakes:_lakes, Seas:_seas, Islands:_islands, Valleys:_valleys}
	
	my process_PhotoSupreme_catalog(_photoSupremeCatalogExport, _folder, _vernacularFolders)
	
	if DRY_RUN then
		my writeToFile(_dump, "/Users/fritz/Personal/Photography/Capture One/Scripts/target/test/actual-results.txt")
	end if
end run

--
--
--
on process_PhotoSupreme_catalog(_photoSupremeCatalogExport, _folder, _vernacularFolders)
	set _group to missing value
	set _groupFolder to missing value
	set _classes to {"", "", "", "", "", "", "", "", "", "", "", "", ""}
	set _objects to {"", "", "", "", "", "", "", "", "", "", "", "", ""}
	set _folders to {"", "", "", "", "", "", "", "", "", "", "", "", ""}
	
	my trace("Loading from " & _photoSupremeCatalogExport)
	set _lines to paragraphs of (read file _photoSupremeCatalogExport as �class utf8�)
	
	tell application "Capture One 22"
		set progress total units to the count of _lines
		set progress completed units to 0
	end tell
	
	set _processedLines to 0
	
	repeat with _line in _lines
		my trace("line [" & (_processedLines + 1) & "/" & (count of _lines) & "]: " & _line)
		
		if _line starts with "[" then
			set _group to text 2 thru -2 of _line
			
			if EXCLUDED_GROUPS contains _group then
				set _group to missing value
			else
				tell application "Capture One 22" to set progress text to "Importing Photo Supreme catalog: " & _group
				set _groupFolder to createGroup(_folder, _group)
			end if
			
		else if _group is not missing value then
			set _level to my countLeadingTabs(_line)
			set _line to text (_level + 1) thru -1 of _line
			
			if _line contains ":" then
				set _j to offset of ":" in _line
				set _class to text 1 thru (_j - 1) of _line
				set _object to text (_j + 1) thru -1 of _line
			else
				set _class to missing value
				set _object to _line
			end if
			
			if _group is "Taxonomy" then
				set _object to my withoutParenthesis(_object)
			end if
			
			set item _level of _classes to _class
			set item _level of _objects to _object
			
			if _level is 1 then
				set _parentFolder to _groupFolder
			else
				set _parentFolder to item (_level - 1) of _folders
			end if
			
			set _path to ""
			
			repeat with _j from 1 to _level
				set _path to _path & " / " & my fqName(item _j of _classes, item _j of _objects)
			end repeat
			
			my trace("    " & _group & ": " & _level & " : " & _path)
			
			if _line does not start with "{" then
				set _newFolder to my createGroup(_parentFolder, my fqName(_class, _object))
				set item _level of _folders to _newFolder
				my createSmartAlbum(_newFolder, "_All", _object) -- FIXME: also class: but doesn't work
			end if
			
			if _group is "Taxonomy" and _line starts with "{" then
				set _vernacularName to text 2 thru -2 of _line
				set _scientificName to item (_level - 1) of _objects
				
				if _scientificName ends with "?" then
					set _vernacularName to _vernacularName & "?"
				end if
				
				my trace("        vernacular name: " & _vernacularName & ", scientific name: " & _scientificName)
				
				my createSmartAlbum(my chooseParentFolder(_objects, _vernacularFolders), _vernacularName & " (" & _scientificName & ")", "scientific name:" & _scientificName)
			end if
			
			if _group is "geo tags" then
				set _parentFolder to missing value
				
				if CLASSES_CHURCH contains _class then
					set _parentFolder to Churches of _vernacularFolders
				else if CLASSES_CASTLE contains _class then
					set _parentFolder to Castles of _vernacularFolders
				else if CLASSES_BRIDGE contains _class then
					set _parentFolder to Bridges of _vernacularFolders
				else if CLASSES_LIGHTHOUSE contains _class then
					set _parentFolder to Lighthouses of _vernacularFolders
				else if CLASSES_INHABITED_PLACE contains _class then
					set _parentFolder to InhabitedPlaces of _vernacularFolders
				else if CLASSES_MOUNTAIN contains _class then
					set _parentFolder to Mountains of _vernacularFolders
				else if CLASSES_RIVER contains _class then
					set _parentFolder to Rivers of _vernacularFolders
				else if CLASSES_LAKE contains _class then
					set _parentFolder to Lakes of _vernacularFolders
				else if CLASSES_SEA contains _class then
					set _parentFolder to Seas of _vernacularFolders
				else if CLASSES_ISLAND contains _class then
					set _parentFolder to Islands of _vernacularFolders
				else if CLASSES_VALLEY contains _class then
					set _parentFolder to Valleys of _vernacularFolders
				end if
				
				if _parentFolder is not missing value then
					set _name to _object & " ("
					set _separator to ""
					
					repeat with _i from (_level - 1) to 1 by -1
						if _i is not 2 then -- skip region/department
							set _item to item _i of _objects
							
							if _item starts with "Provincia di " then
								set _item to text 14 thru (length of _item) of _item
							end if
							
							set _name to _name & _separator & _item
							set _separator to ", "
						end if
					end repeat
					
					set _name to _name & ")"
					
					-- FIXME: need to add conditions for hierarchy, to avoid name clashes
					my createSmartAlbum(_parentFolder, _name, _class & ":" & _object)
				end if
			end if
		end if
		
		my trace(CRLF)
		
		set _processedLines to _processedLines + 1
		tell application "Capture One 22" to set progress completed units to _processedLines
	end repeat
end process_PhotoSupreme_catalog

--
-- Choose the parent folder for vernacular names
--
on chooseParentFolder(_objects, _vernacularFolders)
	set _kingdom to my withoutParenthesis(item 2 of _objects)
	set _phylum to my withoutParenthesis(item 3 of _objects)
	set _class to my withoutParenthesis(item 4 of _objects)
	set _order to my withoutParenthesis(item 5 of _objects)
	my trace(".       kingdom:" & _kingdom & " / phylum:" & _phylum & " / class:" & _class & " / order:" & _order)
	
	if _kingdom is "Plantae" then
		return Plants of _vernacularFolders
	else if _kingdom is "Animalia" then
		if _phylum is "Chordata" then
			if _class is "Amphibia" then
				return Amphibians of _vernacularFolders
			else if _class is "Reptilia" then
				return Reptiles of _vernacularFolders
			else if _class is "Aves" then
				return Birds of _vernacularFolders
			else if _class is "Mammalia" then
				return Mammals of _vernacularFolders
			else
				my fail("Cannot assign parent folder for Chordata/" & _class)
			end if
		else if _phylum is "Arthropoda" then
			if _order is "Lepidoptera" then
				return Butterflies of _vernacularFolders
			else
				return Invertebrates of _vernacularFolders
			end if
		end if
	end if
	
	my fail("Cannot assign parent folder for " & _kingdom)
end chooseParentFolder

--
-- Creates a group.
--
-- _parentFolder: the folder where to create the group
-- _name: the group name
--
on createGroup(_parentFolder, _name)
	my trace("            Create group " & _name)
	
	if DRY_RUN then
		if _parentFolder is missing value then
			set _parentFolder to ""
		end if
		
		set _dump to _dump & "group|parent:" & _parentFolder & "|name:" & _name & CRLF
		
		return _parentFolder & "/" & _name -- for testing
	end if
	
	if _parentFolder is missing value then
		tell front document of application "Capture One 22" to set _newFolder to make collection with properties {kind:group, name:_name}
	else
		tell application "Capture One 22" to tell _parentFolder to set _newFolder to make collection with properties {kind:group, name:_name}
	end if
	
	return _newFolder
end createGroup

--
-- Creates a smart album
-- 
-- _parentFolder: the folder where to create the smart album
-- _name: the smart album name
-- _text: the text that must be contained in keywords
--
on createSmartAlbum(_parentFolder, _name, _text)
	my trace("            Create smart album " & _name)
	set _xml to "<?xml version=\"1.0\" encoding=\"UTF-8\"?> <MatchOperator Kind=\"AND\"> "
	set _xml to _xml & "<Condition Enabled=\"YES\"> <Key>IB_S_CONTENT_KEYWORDS</Key> <Operator>6</Operator> <Criterion>" & _text & "</Criterion> </Condition> "
	set _xml to _xml & "</MatchOperator>"
	
	if DRY_RUN then
		set _dump to _dump & "smart album|parent:" & _parentFolder & "|name:" & _name & "|xml:" & _xml & CRLF
		return _name -- for testing
	end if
	
	tell application "Capture One 22" to tell _parentFolder to set _newFolder to make collection with properties {kind:smart album, name:_name, rules:_xml}
	return _newFolder
end createSmartAlbum

--
--
--
on fqName(_class, _object)
	if _class is missing value then
		return _object
	else
		return _class & ":" & _object
	end if
end fqName
--
-- Writes a text to a file.
--
-- _string:			the text
-- _path:				the path of the file
--
to writeToFile(_string, _path)
	if DEBUG then
		my trace("Writing to " & _path)
	end if
	
	try
		tell current application
			set _file to (open for access _path with write permission)
			set eof of the _file to 0
			write _string to the _file starting at eof as �class utf8�
			close access _file
		end tell
	on error _error
		log "Error: " & _error
		try
			close access _file
		end try
	end try
end writeToFile

--
-- Returns the first word in the given string, using " " as separator
--
on withoutParenthesis(_string)
	if _string does not contain " [" then
		return _string
	end if
	
	return text 1 thru ((offset of " [" in _string) - 1) of _string
end withoutParenthesis

--
-- Counts the leading tabs in a string
--
on countLeadingTabs(_string)
	repeat with _i from 1 to length of _string
		if character _i of _string as string is not equal to tab then
			return _i - 1
		end if
	end repeat
	
	return length of _string
end countLeadingTabs

--
--
--
on currentDateTime()
	set _now to (current date)
	
	set result to (year of _now as integer) as string
	set result to result & "-"
	set result to result & zeroPad(month of _now as integer, 2)
	set result to result & "-"
	set result to result & zeroPad(day of _now as integer, 2)
	set result to result & " "
	set result to result & zeroPad(hours of _now as integer, 2)
	set result to result & ":"
	set result to result & zeroPad(minutes of _now as integer, 2)
	set result to result & ":"
	set result to result & zeroPad(seconds of _now as integer, 2)
	
	return result
end currentDateTime

-- The zero_pad function taken from:
-- http://www.nineboxes.net/2009/10/an-applescript-function-to-zero-pad-integers/
on zeroPad(_value, _stringLength)
	set _stringZeroes to ""
	set _digitsToPad to _stringLength - (length of (_value as string))
	
	if _digitsToPad > 0 then
		repeat _digitsToPad times
			set _stringZeroes to _stringZeroes & "0"
		end repeat
	end if
	
	return _stringZeroes & _value
end zeroPad

--
-- Fails the script because of the given reason.
--
-- _reason: 			the reason to fail
--
to fail(_reason)
	trace("Failing: " & _reason)
	error number -128
end fail

--
-- Logs a message for debugging purposes.
--
-- _message: 			the message to log
--
on trace(message)
	if DEBUG then
		log ">>>> " & message
	end if
end trace
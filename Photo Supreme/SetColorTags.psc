var
  ACatItem: TCatalogItem;
  AOldColorLabel: WideString
  AColorLabel: WideString;
  APublished: Boolean;
  APrinted: Boolean;
  ABackgrounds: Boolean;
  APropName: WideString;
begin
  if Selected.Count = 0 then
  begin
    Say ('No image selected');
    exit;
  end;
  ACatItem := TCatalogItem.Create(nil);
  for i := 0 to Selected.Count - 1 do
  begin
    ImageItem := Selected.Items[i];
    if PublicCatalog.FindImageCombined (ImageItem, ACatItem, False, vptNone) then
    begin
      AColorLabel := ACatItem.ColorLabel;
      AProps := TCatalogItemProps.Create(TCatalogItemProp, '');
      PublicCatalog.EnumPropsForItem(ACatItem, AProps, False, False);

      APublished := False;
      APrinted := False;
      ABackgrounds := False;
      AElement := False;
      ATest := False;
      ASpecimen := False;

      for j := 0 to AProps.Count - 1 do
      begin
        APropName := AProps.Items[j].PropName;

        if APropName = 'stoppingdown' then
          APublished := True;
        else if (APropName = 'printed') or (APropName = 'book') then
          APrinted := True;
        else if APropName = 'workflow:published#backgrounds' then
          ABackgrounds := True;
        else if (APropName = 'workflow:pano-element') or (APropName = 'workflow:hdr-element') or (APropName = 'workflow:focus-stack-element') or (APropName = 'workflow:denoised-element') then
          AElement := True;
        else if (APropName = 'test') then
          ATest := True;
        else if (APropName = 'taxonomy:specimen') then
          ASpecimen := True;
      end;

      AProps.Free;
      AOldColorLabel := AColorLabel;

      if APublished then
        AColorLabel := 'Green'
      else if APrinted then
        AColorLabel := 'Blue'
      else if AElement or ATest or ASpecimen then
        AColorLabel := 'Purple'
      else if (AColorLabel <> 'Red') and (AColorLabel <> 'Yellow') then
        AColorLabel := '';

      if AOldColorLabel <> AColorLabel then
      begin
        ACatItem.ColorLabel := AColorLabel;
        if PublicCatalog.StoreItemToDataBase(ACatItem, False) then PublicCatalog.UpdateItemSyncState(ACatItem, [], [idcsDBXmp], False, True);
      end;
    end;
  end;
  ACatItem.Free;
end;


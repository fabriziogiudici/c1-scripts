-- Scripts for Capture One
-- ===============
-- 
-- Copyright (C) 2019-2021 by Tidalwave s.a.s. (http://tidalwave.it)
-- 
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
-- 
--     http://www.apache.org/licenses/LICENSE-2.0
-- 
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.

set _collection to missing value
set _monthNames to {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"}

tell front document of application "Capture One 22"
	set _folder to make collection with properties {kind:group, name:"__MONTHLY__"}
	
	repeat with _month from 1 to 12
		set _month2 to text -2 thru -1 of ("0" & _month)
		set _xml to "<?xml version=\"1.0\" encoding=\"UTF-8\"?> <MatchOperator Kind=\"OR\"> "
		
		repeat with _year from 1998 to 2030
			set _xml to _xml & "<Condition Enabled=\"YES\"> <Key>_date_yearMonth</Key> <Operator>0</Operator> <Criterion>" & _year & "-" & _month2 & "</Criterion> </Condition>"
		end repeat
		
		set _xml to _xml & "</MatchOperator>"
		
		tell application "Capture One 22" to tell _folder to set _filter to make collection with properties {kind:smart album, name:"" & _month2 & ". " & item _month of _monthNames, rules:_xml}
		
	end repeat
end tell

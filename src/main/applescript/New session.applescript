-- Scripts for Capture One
-- =======================
-- 
-- Copyright (C) 2024 by Tidalwave s.a.s. (http://tidalwave.it)
-- 
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
-- 
--     http://www.apache.org/licenses/LICENSE-2.0
-- 
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.

use framework "Foundation"
use scripting additions

global DEBUG

on run argv
	set DEBUG to true
	
	tell application "Capture One"
		tell current document
			set _sessions to some collection whose name is "Sessions"
			set _stoppingDown to some collection whose name is "StoppingDown"
			tell _stoppingDown to set _diary to some collection whose name is "Diary"
		end tell
		
		set {year:_year, month:_month, day:_day} to current date
		set _today to "" & _day & "-" & (_month * 1) & "-" & _year
		set _dialog to display dialog "Start date:" default answer _today with icon note buttons {"Cancel", "Ok"} default button "Ok"
		
		if button returned of _dialog is "Ok" then
			set _startDate to text returned of _dialog
			set _day to text 1 thru 2 of _startDate
			set _month to text 4 thru 5 of _startDate
			set _year to text 7 thru 10 of _startDate
			tell _sessions to set _sessionYear to some collection whose name is _year
			tell _diary to set _diaryYear to some collection whose name is _year
			set _dialog to display dialog "Session name" default answer "" with icon note buttons {"Cancel", "Ok"} default button "Ok"
			
			if button returned of _dialog is "Ok" then
				set _name to text returned of _dialog
				set _fullName to "" & _month & "/" & _day & ". " & _name
				set _x to "" & _year & "-" & _month & "-" & _day
				log "_fullName: " & _fullName
				
				tell _sessionYear to make collection with properties {kind:smart album, name:_fullName, rules:"<?xml version=\"1.0\"?><MatchOperator Kind=\"AND\"><MatchOperator Kind=\"AND\"><Condition Enabled=\"YES\"><Key>IB_S_EXP_DATE</Key><Operator>0</Operator><Criterion>" & _x & "</Criterion></Condition></MatchOperator></MatchOperator>"}
				tell _diaryYear to make collection with properties {kind:smart album, name:_fullName, rules:"<?xml version=\"1.0\"?><MatchOperator Kind=\"AND\"><MatchOperator Kind=\"AND\"><Condition Enabled=\"YES\"><Key>IB_S_CONTENT_KEYWORDS</Key><Operator>6</Operator><Criterion>stoppingdown:diary-item#" & _x & "</Criterion></Condition></MatchOperator></MatchOperator>"}
			end if
		end if
	end tell
end run

--
-- Logs a message for debugging purposes.
--
-- _message: 			the message to log
--
on trace(message)
	if DEBUG then
		log ">>>> " & message
	end if
end trace

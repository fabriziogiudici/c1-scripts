-- Scripts for Capture One
-- =======================
-- 
-- Copyright (C) 2023-2024 by Tidalwave s.a.s. (http://tidalwave.it)
-- 
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
-- 
--     http://www.apache.org/licenses/LICENSE-2.0
-- 
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.

use framework "Foundation"
use scripting additions

global DEBUG

on run argv
	set DEBUG to true
	
	set _HEIC_FOLDER to "macOS:Volumes:Photos:Archived:HEIC"
	set _HEIC_SUFFIX to "@90%.heic"
	
	tell application "Capture One"
		tell front document
			with timeout of 3600 seconds
				set _selectedVariants to every variant whose selected is true and pick is true
			end timeout
		end tell
		
		set progress total units to the count of _selectedVariants
		set progress completed units to 0
		set progress text to "Processing..."
		
		set _index to 1
		
		repeat with _variant in _selectedVariants
			set _variantName to name of _variant as text
			set progress additional text to "" & _index & "/" & (count of _selectedVariants) & ": " & _variantName
			
			-- FIXME: this is latest export, latest modification seems to be unavailable
			set _latestExportDateTime to missing value
			set _outputEvents to output events of _variant
			set _outputEventsCount to count of _outputEvents
			
			if _outputEventsCount > 0 then
				set _latestExportDateTime to date of item _outputEventsCount of _outputEvents
			end if
			
			set _year to text 1 thru 4 of _variantName
			set _heicFile to _HEIC_FOLDER & ":" & _year & ":" & _variantName & _HEIC_SUFFIX
			
			try
				tell application "System Events" to set _heicPath to file _heicFile
				set _heicDateTime to modification date of _heicPath
				log "" & _variantName & ": latest exported: " & (_latestExportDateTime as text) & " HEIC latest modified: " & (_heicDateTime as text)
			on error _errorMessage number _errorNumber
				log "    " & _variantName & ": Error " & _errorNumber & ": " & _errorMessage
			end try
			
			set _index to _index + 1
			set progress completed units to _index
		end repeat
		
		set progress completed units to the count of _selectedVariants
	end tell
end run

--
-- Logs a message for debugging purposes.
--
-- _message: 			the message to log
--
on trace(message)
	if DEBUG then
		log ">>>> " & message
	end if
end trace

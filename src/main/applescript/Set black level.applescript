-- Scripts for Capture One
-- =======================
-- 
-- Copyright (C) 2024 by Tidalwave s.a.s. (http://tidalwave.it)
-- 
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
-- 
--     http://www.apache.org/licenses/LICENSE-2.0
-- 
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.

use framework "Foundation"
use scripting additions

global DEBUG

on run argv
	set DEBUG to true
	
	tell application "Capture One"	
		set _dialog to display dialog "Black level:" default answer "0" with icon note buttons {"Cancel", "Ok"} default button "Ok"
		
		if button returned of _dialog is "Ok" then
			set _blackLevel to text returned of _dialog
			log "Black level: " & _blackLevel
			
			tell front document
				with timeout of 3600 seconds
					set _selectedVariants to every variant whose selected is true
				end timeout
			end tell
			
			set progress total units to the count of _selectedVariants
			set progress completed units to 0
			set progress text to "Processing..."
			
			set _index to 1
			
			repeat with _variant in _selectedVariants
				set _variantName to name of _variant as text
				log _variantName
				set progress additional text to "" & _index & "/" & (count of _selectedVariants) & ": " & _variantName
				
				set _adjustments to adjustments of _variant
				set level shadow rgb of _adjustments to _blackLevel
				
				set _index to _index + 1
				set progress completed units to _index
			end repeat
			
			set progress completed units to the count of _selectedVariants
		end if
	end tell
end run

--
-- Logs a message for debugging purposes.
--
-- _message: 			the message to log
--
on trace(message)
	if DEBUG then
		log ">>>> " & message
	end if
end trace

-- Scripts for Capture One
-- =======================
-- 
-- Copyright (C) 2019-2023 by Tidalwave s.a.s. (http://tidalwave.it)
-- 
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
-- 
--     http://www.apache.org/licenses/LICENSE-2.0
-- 
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.

-- For each primary variant currently selected:
--   + ignore it if not tagged as processed with Capture One;
--   + export using the recipe 'Backgrounds' if tagged as 'background';
--   + export using the recipes 'StoppingDown *' if color tagged green;
--   + export always using recipe 'Long term archive (JPEG 80%)'.
--
-- Issues:
--   + Large batches are truncated; see https://support.captureone.com/hc/en-us/community/posts/8216795931165
--
on run argv
	tell application "Capture One"
		set my text item delimiters to ", "
		set _green to 4
		set _blue to 5
		set _pink to 6
		set _purple to 7
		
		set _dryRun to "--dry-run" is in argv
		
		set _pick to button returned of (display dialog "Also run printing export recipes?" buttons {"Yes", "No"} default button "No" with title "Export variants")
		set _processPrinted to _pick is "Yes"
		log "Will process prints: " & _processPrinted
		
		set progress text to "Submitting variants to be exported"
		set progress additional text to "Retrieving selected variants..."
		set progress total units to 1
		set progress completed units to 0
		
		tell front document
			with timeout of 7200 seconds
				set _selectedVariants to every variant whose selected is true and pick is true
				set _stoppingDownRecipeNames to name of recipes whose name starts with "StoppingDown"
			end timeout
		end tell
		
		set progress total units to the count of _selectedVariants
		set _variantProcessedCount to 0
		set _variantExportedCount to 0
		set _errorCount to 0
		
		repeat with _variant in _selectedVariants
			set _variantName to name of _variant as text
			set _keywordNames to name of keywords of _variant
			set progress text to "Submitted " & _variantProcessedCount + 1 & " out of " & (count of _selectedVariants) & " variants to be exported"
			set progress additional text to _variantName
			set _recipeNames to {}
			
			if "workflow:edit-process#capture-one" is in _keywordNames then
				set end of _recipeNames to "Long term archive (HEIF 90%)"
				set end of _recipeNames to "Thumbnails"
				
				if "workflow:published#backgrounds" is in _keywordNames then set end of _recipeNames to "Backgrounds (JPG 70%)"
				if "stoppingdown" is in _keywordNames then set _recipeNames to _recipeNames & _stoppingDownRecipeNames
				
				if _processPrinted then
					if "DigitalPix Fujifilm Crystal Archive Supreme Lustre 20x30cm" is in _keywordNames then set end of _recipeNames to "DigitalPix Fujifilm Crystal Archive Supreme Lustre 20x30cm"
					if "Saal Digital Opaque 20x30cm" is in _keywordNames then set end of _recipeNames to "Saal Digital Opaque 20x30cm"
					if "Stampe per Fotografi HP Matt Litho-Realistic 20x30cm" is in _keywordNames then set end of _recipeNames to "Stampe per Fotografi HP Matt Litho-Realistic 20x30cm"
					if "book:Fiori 2017-2023" is in _keywordNames then set end of _recipeNames to "Book - Fiori 2017-2023"
				end if
				
				log "" & (_variantProcessedCount + 1) & "/" & (count of _selectedVariants) & " " & _variantName & ": " & (_recipeNames as text)
				
				repeat with _recipeName in _recipeNames
					try
						if not _dryRun then tell _variant to process recipe _recipeName
						set _variantExportedCount to _variantExportedCount + 1
					on error _errorMessage number _errorNumber
						log "    " & name of _variant & ", " & _recipeName & ": Error " & _errorNumber & ": " & _errorMessage
						set _errorCount to _errorCount + 1
					end try
				end repeat
			end if
			
			set _variantProcessedCount to _variantProcessedCount + 1
			set progress completed units to _variantProcessedCount
		end repeat
		
		if _errorCount > 0 then
			display dialog "" & _errorCount & " errors" buttons "Ok"
		end if
		
		return "Completed: exported " & _variantExportedCount & " variants; " & _errorCount & " errors."
	end tell
end run

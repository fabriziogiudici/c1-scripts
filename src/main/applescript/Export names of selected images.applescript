-- Scripts for Capture One
-- =======================
-- 
-- Copyright (C) 2024 by Tidalwave s.a.s. (http://tidalwave.it)
-- 
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
-- 
--     http://www.apache.org/licenses/LICENSE-2.0
-- 
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.

use framework "Foundation"
use scripting additions

global CRLF, DEBUG, OUTPUT_FILE

on run argv
	set DEBUG to true
	set CRLF to "
"
	set OUTPUT_FILE to "macOS:Users:fritz:Personal:Photography:Capture One:Output:SelectedImages.txt"
	
	tell application "Capture One"
		set progress total units to 1
		set progress completed units to 0
		set progress text to "Retrieving selected variants..."
		-- set progress additional text to "..."
		
		tell front document
			with timeout of 3600 seconds
				set _selectedVariants to every variant whose selected is true and pick is true
			end timeout
		end tell
		
		set progress text to "Processing..."
		set _index to 1
		set _list to ""
		
		repeat with _variant in _selectedVariants
			set _variantName to name of _variant as text
			set progress additional text to "" & _index & "/" & (count of _selectedVariants) & ": " & _variantName
			set _path to path of parent image of _variant
			log _variantName & ": " & _path
			set _list to _list & _path & CRLF
			set _index to _index + 1
			set progress completed units to _index
		end repeat
		
		my writeToFile(_list, OUTPUT_FILE)
		set progress completed units to the count of _selectedVariants
		display dialog "Names exported." with icon note buttons {"Ok"} default button "Ok"
	end tell
end run

--
-- Writes a text to a file.
--
-- _string:			the text
-- _path:			the path of the file
--
on writeToFile(_string, _path)
	if DEBUG then
		trace("Writing to " & _path & ": " & CRLF & _string)
	end if
	
	tell application "System Events"
		--	try
		set _file to open for access file _path with write permission
		set eof of the _file to 0
		write _string to the _file starting at eof as �class utf8�
		close access _file
		(*
	on error _error
		try
			log (_error & " writing file " & _path)
			close access file _path
		end try
	end try*)
	end tell
end writeToFile

--
-- Logs a message for debugging purposes.
--
-- _message: 			the message to log
--
on trace(message)
	if DEBUG then
		log ">>>> " & message
	end if
end trace

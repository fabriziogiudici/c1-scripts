-- Scripts for Capture One
-- =======================
--
-- Copyright (C) 2019-2022 by Tidalwave s.a.s. (http://tidalwave.it)
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.

use framework "Foundation"
use scripting additions

global DIARY_PATH, PRIVATE_DIARY_PATH, THEMES_PATH, PRIVATE_THEMES_PATH, RESOURCES_PATH, STOPPINGDOWN_PATH, CRLF, PS, XML_HEADER, COMPONENTS_XML, PROPERTIES_XML, IMAGES_XML, DEBUG

on run (_args)
	set CRLF to "
"
	set PS to ":"
	set DEBUG to true
	
	set XML_HEADER to "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" & CRLF
	
	set STOPPINGDOWN_PATH to "macOS:Users:fritz:Personal:WebSites:StoppingDown.net"
	set DIARY_PATH to STOPPINGDOWN_PATH & ":structure:Diary"
	set PRIVATE_DIARY_PATH to STOPPINGDOWN_PATH & ":structure:Private:Diary"
	set THEMES_PATH to STOPPINGDOWN_PATH & ":structure:Themes"
	set PRIVATE_THEMES_PATH to STOPPINGDOWN_PATH & ":structure:Private:Themes"
	
	set COMPONENTS_XML to "Components.xml"
	set PROPERTIES_XML to "Properties.xml"
	set IMAGES_XML to "images.xml"
	
	set _groups to {"Diary", "Others", "Places", "Themes"}
	set _selectedGroups to choose from list _groups with prompt "Select groups: " default items {"Diary"} with multiple selections allowed
	
	if _selectedGroups is false or length of _selectedGroups is 0 then return
	my trace("selected groups: " & _selectedGroups)
	set _selectedFolders to missing value
	
	if _selectedGroups contains "Diary" then
		set _folders to {}
		set _currentYear to 2024
		repeat with _year from 2003 to _currentYear
			set end of _folders to _year as text
		end repeat
		set _selectedFolders to choose from list _folders with prompt "Select years: " default items {_currentYear as text} with multiple selections allowed
		if _selectedFolders is false then return
	end if
	
	my trace("selected folders: " & _selectedFolders)
	
	tell application "Finder" to set _myPath to "Volumes:" & (container of (path to me) as alias)
	my trace("My path: " & _myPath)
	# no debug when running inside Capture One, too much stuff stalls the script
	set DEBUG to _myPath as string does not contain ("Library" & PS & "Scripts")
	set RESOURCES_PATH to "macOS:" & _myPath & "resources"
	
	--try
	tell front document of application "Capture One" to my exportStoppingDown(collection named "StoppingDown", _selectedGroups, _selectedFolders)
	display notification "StoppingDown export completed." sound name "Beep"
end run


--on error _error
--	display notification "Error: " & _error
--end try

--
-- Exports StoppingDown.
--
-- _project:			the project containing StoppingDown
-- _selectedGroups:	the groups to process
-- _selectedFolders:	the folders to process
--
on exportStoppingDown(_project, _selectedGroups, _selectedFolders)
	tell application "Capture One" to tell _project to set _groups to collections
	
	repeat with _group in _groups
		if _selectedGroups contains name of _group then
			my exportGroup(_group, _selectedFolders)
		end if
	end repeat
end exportStoppingDown

--
-- Exports a group (e.g. 'Diary', 'Themes', 'Places')
--
-- _group:			the group
-- _selectedFolders:	the folders to process
--
on exportGroup(_group, _selectedFolders)
	set _groupName to name of _group
	
	if {"Places", "Themes", "Others"} contains _groupName then -- it's a folder, not a group
		my exportFolder(_groupName, _group)
	else
		my trace("Processing group " & _groupName)
		tell application "Capture One" to tell _group to repeat with _folder in collections
			if _selectedFolders is missing value or _selectedFolders contains name of _folder then
				my exportFolder(_groupName, _folder)
			end if
		end repeat
	end if
end exportGroup

--
-- Exports a folder (e.g. '2018', 'Provence', 'Flowers').
--
-- _groupName		the name of the group
-- _folder:			the folder containing collections
--
on exportFolder(_groupName, _folder)
	tell application "Capture One"
		set _folderName to name of _folder
		my trace("Processing folder " & _folderName & " in group " & _groupName)
		tell _folder to set _collections to collections
		set progress total units to the count of _collections
		set progress completed units to 0
		
		repeat with _collection in _collections
			set progress text to "Exporting StoppingDown galleries: " & _folderName
				set _name to name of _collection

			if _name does not end with "candidates" and _name does not end with "NEW" and _name does not end with "OLD" then
				my exportCollection(_groupName, _folderName, _collection)
			end if
			
			set progress completed units to progress completed units + 1
		end repeat
	end tell
end exportFolder

--
-- Exports a single collection.
--
-- _groupName:		the name of the group (used to pick the target path)
-- _folderName:		the name of the folder
-- _collection:			the collection
--
on exportCollection(_groupName, _folderName, _collection)
	set _name to name of _collection
	my trace("Processing collection " & _name & " in folder " & _folderName & " in group " & _groupName)
	
	if _name is "All" then
		my trace("Skipping " & _name)
		return
	end if
	
	tell application "Capture One" to tell _collection
		set _imageCount to count of name of variants
		set _kind to kind of _collection
		set _kindIsAlbum to {album, smart album} contains _kind
	end tell
	
	if not _kindIsAlbum then
		my trace("" & _groupName & "/" & _folderName & " not an album " & (_kind as string) & ", skipping")
		return
	end if
	
	set _temporary to _imageCount is 0
	
	if _name ends with "*" then
		set _name to text 1 thru -3 of _name
		set _temporary to true
	end if
	
	if _groupName is "Diary" then
		set _year to _folderName
		set _month to text 1 thru 2 of _name
		set _day to text 4 thru 5 of _name
		
		if {".", "-"} does not contain character 6 of _name then
			set _day to text 4 thru 6 of _name -- eg "25a", "25b"
		end if
		
		if not _temporary then
			set _path to DIARY_PATH & PS & _year & PS & _month & PS & _day
		else
			set _path to PRIVATE_DIARY_PATH & PS & _year & _month & _day
		end if
	else if (_groupName is "Places") or (_groupName is "Themes") then
		if not _temporary then
			set _path to THEMES_PATH & PS & my urlEncode(my flattened(_name))
		else
			set _path to PRIVATE_THEMES_PATH & PS & my urlEncode(my flattened(_name))
		end if
	else if (_groupName is "Galleries") then
		set _path to THEMES_PATH & PS & "Lens" & PS & my urlEncode(my flattened(_name))
		(*
	else if _name is "Splash Slideshow" then
		set _path to STOPPINGDOWN_PATH & PS & "structure"
		*)
	else
		my trace("  Skipped")
		return
	end if
	
	my trace("base path " & _path)
	my mkdirs(_path)
	my createOrUpdateFiles(_path, _temporary)
	my writeToFile(createImagesXml(_collection), _path & PS & IMAGES_XML)
end exportCollection

--
-- Creates the images.xml content out of a collection.
--
-- _collection:			the collection
--
on createImagesXml(_collection)
	set _name to name of _collection
	tell application "Capture One"
		set _kindAsString to kind of _collection as string
		my assert({"album", "smart album"} contains _kindAsString, "Collection " & _name & " is not an album: " & _kindAsString)
		tell _collection to set _imageNames to name of variants whose pick is true
	end tell
	
	set _r to XML_HEADER & "<gallery>" & CRLF
	set _sep to offset of ". " in _name
	
	if (_sep > 0) then
		set _name to text (_sep + 2) thru -1 of _name
	end if
	
	set _r to _r & "    <title>" & _name & "</title>" & CRLF
	set _r to _r & "    <album>" & CRLF
	
	repeat with _imageName in _imageNames
		set _r to _r & "        <img src=\"" & _imageName & ".jpg\"/>" & CRLF
		tell application "Capture One" to set progress additional text to (name of _collection) & ": " & _imageName
		my trace("IMAGE: " & _imageName)
	end repeat
	
	return _r & "    </album>" & CRLF & "</gallery>" & CRLF
end createImagesXml

--
-- Creates or updates Components.xml and Properties.xml files in the given folder.
--
-- _path:				the path of the destination folder
-- _temporary:		whether it's a temporary gallery
--
on createOrUpdateFiles(_path, _temporary)
	set _componentsXmlPath to _path & PS & COMPONENTS_XML
	set _propertiesXmlPath to _path & PS & PROPERTIES_XML
	
	set _componentsExists to fileExists(_componentsXmlPath)
	set _propertiesExists to fileExists(_propertiesXmlPath)
	
	if (not _componentsExists and not _temporary) then
		my copyResourceFile(COMPONENTS_XML, _path)
	end if
	
	if (not _propertiesExists) then
		my copyResourceFile(PROPERTIES_XML, _path)
	end if
	
	set _currentDateRfc3339 to my dateAsRfc3339(current date)
	
	my trace("Current timestamp: " & _currentDateRfc3339)
	
	tell application "System Events"
		(*	
		set _xml to XML file _propertiesXmlPath
		tell _xml
			tell XML element "properties"
				set _properties to every XML element whose name is "property"
				
				repeat with _property in _properties
					tell _property
						set _propertyName to value of XML attribute named "name"
						tell XML element "value"
							set _propertyValue to value
							
							# FIXME: apparently modifying XML elements doesn't work							
							# FIXME: latestModificationTime only if images.xml has been changed!
							
							if _propertyName is "latestModificationDateTime" then
								my trace("latestModificationDateTime: " & _propertyValue)
								set value to _currentDateRfc3339
							else if _propertyName is "creationDateTime" and not _propertiesExists then
								set value to _currentDateRfc3339
							end if
						end tell
					end tell
				end repeat
			end tell
			
			my writeToFile(my xmlToString(_xml), _propertiesXmlPath)
		end tell
		*)
		
		(* tell _xml to save as text in file _propertiesXmlPath *)
	end tell
end createOrUpdateFiles

--
-- Writes a text to a file.
--
-- _string:			the text
-- _path:				the path of the file
--
on writeToFile(_string, _path)
	if DEBUG then
		trace("Writing to " & _path & ": " & CRLF & _string)
	end if
	
	tell application "System Events"
		#	try
		set _file to open for access file _path with write permission
		set eof of the _file to 0
		write _string to the _file starting at eof as �class utf8�
		close access _file
		(*
	on error _error
		try
			log (_error & " writing file " & _path)
			close access file _path
		end try
	end try*)
	end tell
end writeToFile

--
-- Converts an XML document to a string.
--
-- _xml:				the XML document
--
on xmlToString(_xml)
	tell application "System Events" to tell _xml to return XML_HEADER & my elementsToString("", every XML element)
end xmlToString

on elementsToString(_indent, _elements)
	set _r to ""
	
	repeat with _element in _elements
		set _r to _r & my elementToString(_indent, _element)
	end repeat
	
	return _r
end elementsToString

on elementToString(_indent, _element)
	tell application "System Events" to tell _element
		set _name to name of _element
		set _value to value of _element
		set _attributes to every XML attribute
		set _a to ""
		
		repeat with _attribute in _attributes
			set _a to " " & (name of _attribute) & "=\"" & (value of _attribute) & "\""
		end repeat
		
		set _r to _indent & "<" & _name & _a & ">"
		
		if _value � missing value then
			set _r to _r & _value & "</" & _name & ">" & CRLF
		else
			set _r to _r & CRLF & my elementsToString("    " & _indent, every XML element) & _indent & "</" & _name & ">" & CRLF
		end if
		
		return _r
	end tell
end elementToString

--
-- Copies a resource file to a folder.
--
-- _file: 				the file to copy (must be inside the RESOURCES_PATH folder)
-- _destinationFolder: 	the folder to copy the file into
--
on copyResourceFile(_file, _destinationFolder)
	my trace("Copying " & _file & " to " & _destinationFolder)
	-- set _file to POSIX path of (RESOURCES_PATH & PS & _file)
	-- set _destinationFolder to POSIX path of _destinationFolder
	-- do shell script "cp \"" & _file & "\" \"" & _destinationFolder & "\""
	tell application "Finder" to copy file (RESOURCES_PATH & PS & _file) to folder _destinationFolder
end copyResourceFile

--
-- Creates folders.
--
-- _path:				the path to create
--
on mkdirs(_path)
	my mkdirs2("macOS", text ((offset of PS in _path) + 1) thru -1 of _path)
end mkdirs

on mkdirs2(_folder, _path)
	set _colon to offset of PS in _path
	
	if (_colon > 0) then
		set _head to text 1 thru (_colon - 1) of _path
		set _tail to text (_colon + 1) thru -1 of _path
	else
		set _head to _path
		set _tail to missing value
	end if
	#	trace("PARENT FOLDER " & _folder & " HEAD " & _head & " TAIL " & _tail)
	try
		tell application "Finder" to make new folder at alias _folder with properties {name:_head}
	end try
	
	if _tail is not missing value then
		my mkdirs2(_folder & PS & _head, _tail)
	end if
end mkdirs2

--
-- Checks whether a file exists.
--
-- _file:				the file
--
on fileExists(_file)
	tell application "System Events" to return exists file _file
end fileExists

--
-- Converts a date to a string to RFC 3339 format.
--
-- _date: 			the date to convert
--
on dateAsRfc3339(_date)
	set _formatter to current application's NSDateFormatter's new()
	_formatter's setLocale:(current application's NSLocale's localeWithLocaleIdentifier:"en_US_POSIX")
	_formatter's setDateFormat:"yyyy'-'MM'-'dd'T'HH':'mm':'ssXXX"
	return (_formatter's stringFromDate:_date) as text
end dateAsRfc3339

-- flatten characters with accents

on flattened(_string)
	set _result to ""
	
	repeat with _c in characters of _string
		set _char to _c as string
		
		if _char is "�" or _char is "�" then
			set _char to "e"
		else if _char is "�" or _char is "�" then
			set _char to "o"
		end if
		
		set _result to _result & _char
	end repeat
	
	return _result
end flattened

-- https://stackoverflow.com/questions/23852182/i-need-to-url-encode-a-string-in-applescript

on urlEncode(theText)
	set theTextEnc to ""
	repeat with eachChar in characters of theText
		set useChar to eachChar
		set eachCharNum to ASCII number of eachChar
		
		if eachCharNum = 32 then
			set useChar to "+"
		else if (eachCharNum � 42) and (eachCharNum � 95) and (eachCharNum < 45 or eachCharNum > 46) and (eachCharNum < 48 or eachCharNum > 57) and (eachCharNum < 65 or eachCharNum > 90) and (eachCharNum < 97 or eachCharNum > 122) then
			set firstDig to round (eachCharNum / 16) rounding down
			set secondDig to eachCharNum mod 16
			if firstDig > 9 then
				set aNum to firstDig + 55
				set firstDig to ASCII character aNum
			end if
			if secondDig > 9 then
				set aNum to secondDig + 55
				set secondDig to ASCII character aNum
			end if
			set numHex to ("%" & (firstDig as string) & (secondDig as string)) as string
			set useChar to numHex
		end if
		set theTextEnc to theTextEnc & useChar as string
	end repeat
	return theTextEnc
end urlEncode

--
-- Asserts a mandatory condition and eventually fails.
--
-- _condition:			the condition to check
-- _reason: 			the reason to fail
--
on assert(_condition, _reason)
	if not _condition then
		my fail(_reason)
	end if
end assert
--
-- Fails the script because of the given reason.
--
-- _reason: 			the reason to fail
--
on fail(_reason)
	trace("Failing: " & _reason)
	error number -128
end fail

--
-- Logs a message for debugging purposes.
--
-- _message: 			the message to log
--
on trace(message)
	if DEBUG then
		log ">>>> " & message
	end if
end trace


-- Scripts for Capture One
-- =======================
-- 
-- Copyright (C) 2019-2023 by Tidalwave s.a.s. (http://tidalwave.it)
-- 
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
-- 
--     http://www.apache.org/licenses/LICENSE-2.0
-- 
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.

use framework "Foundation"
use scripting additions

global CRLF, DEBUG

on run argv
	set DEBUG to true
	set CRLF to "
"
	set _firstPage to 1
	
	tell application "Capture One"
		tell front document
			with timeout of 3600 seconds
				set _selectedVariants to every variant whose selected is true and pick is true
			end timeout
		end tell
		
		set _index to _firstPage
		
		set _gpx to "<?xml version='1.0' encoding='UTF-8' standalone='no' ?>" & CRLF
		set _gpx to _gpx & "<gpx xmlns='http://www.topografix.com/GPX/1/1' version='1.1'>" & CRLF
		
		set _kml to "<?xml version='1.0' encoding='UTF-8'?>" & CRLF
		set _kml to _kml & "<kml xmlns='http://www.opengis.net/kml/2.2'>" & CRLF
		
		set _places to ""
		
		set progress total units to the count of _selectedVariants
		set progress completed units to 0
		set progress text to "Processing..."
		
		repeat with _variant in _selectedVariants
			set _variantName to name of _variant as text
			set progress additional text to "" & _index & "/" & (count of _selectedVariants) & ": " & _variantName
			set _outputEventsCount to count of output events of _variant
			set _captureDate to EXIF capture date of parent image of _variant
			
			set _latitude to latitude of _variant
			set _longitude to longitude of _variant
			set _altitude to altitude of _variant
			
			set _imageLocation to image location of _variant
			set _imageCity to image city of _variant
			set _imageState to image state of _variant
			-- set _imageCountry to image country of _variant
			set _imageCountryCode to image country code of _variant
			
			log ("" & _index & ": " & _variantName)
			
			set _gpxWaypoint to "  <wpt lat='" & my formatNumber(_latitude) & "' lon='" & my formatNumber(_longitude) & "'>" & CRLF
			
			if _altitude is not missing value then
				set _gpxWaypoint to _gpxWaypoint & "    <ele>" & my formatNumber(_altitude) & "</ele>" & CRLF
			end if
			
			set _gpxWaypoint to _gpxWaypoint & "    <time>" & (my dateAsRfc3339(_captureDate)) & "</time>" & CRLF
			set _gpxWaypoint to _gpxWaypoint & "    <name><![CDATA[" & (_index as text) & "]]></name>" & CRLF
			set _gpxWaypoint to _gpxWaypoint & "    <sym>Waypoint</sym>" & CRLF
			set _gpxWaypoint to _gpxWaypoint & "  </wpt>" & CRLF
			set _gpx to _gpx & _gpxWaypoint
			
			set _kmlWaypoint to "  <Placemark>" & CRLF
			set _kmlWaypoint to _kmlWaypoint & "    <name>" & (_index as text) & "</name>" & CRLF
			set _kmlWaypoint to _kmlWaypoint & "    <description>" & _variantName & "</description>" & CRLF
			set _kmlWaypoint to _kmlWaypoint & "    <Point>" & CRLF
			set _kmlWaypoint to _kmlWaypoint & "      <coordinates>" & my formatNumber(_latitude) & "," & my formatNumber(_longitude)
			
			if _altitude is not missing value then
				set _kmlWaypoint to _kmlWaypoint & "," & my formatNumber(_altitude)
			end if
			
			set _kmlWaypoint to _kmlWaypoint & "</coordinates>" & CRLF
			set _kmlWaypoint to _kmlWaypoint & "    </Point>" & CRLF
			set _kmlWaypoint to _kmlWaypoint & "  </Placemark>" & CRLF
			set _kml to _kml & _kmlWaypoint
			
			set _location to _imageCity & " � " & _imageState & " (" & _imageCountryCode & ")"
			
			if _imageLocation is not missing value and _imageLocation is not "" then
				set _location to _imageLocation & ", " & _location
			end if
			
			set _places to _places & (_index as text) & ": " & _location & CRLF
			
			set _index to _index + 1
			set progress completed units to progress completed units + 1
			
			my trace("" & _index & ": " & _gpxWaypoint)
			my trace("" & _index & ": " & _location)
		end repeat
	end tell
	
	set _gpx to _gpx & "</gpx>" & CRLF
	set _kml to _kml & "</kml>" & CRLF
	
	my writeToFile(_gpx, "macOS:Users:fritz:gpx.gpx")
	my writeToFile(_kml, "macOS:Users:fritz:kml.kml")
	my writeToFile(_places, "macOS:Users:fritz:txt.txt")
end run

--
-- Writes a text to a file.
--
-- _string:			the text
-- _path:				the path of the file
--
on writeToFile(_string, _path)
	trace("Writing to " & _path & ": " & CRLF & _string)
	
	tell application "System Events"
		-- try
		set _file to open for access file _path with write permission
		set eof of the _file to 0
		write _string to the _file starting at eof as �class utf8�
		close access _file
		(*
	on error _error
		try
			log (_error & " writing file " & _path)
			close access file _path
		end try
	end try*)
	end tell
end writeToFile

--
-- Logs a message for debugging purposes.
--
-- _message: 			the message to log
--
on trace(message)
	if DEBUG then
		log ">>>> " & message
	end if
end trace

--
-- Converts a number to a string with a given format.
--
-- _number: 			the number to convert
--
on formatNumber(_number)
	if class of _number is text then set _number to _number as real
	set _formatter to current application's NSNumberFormatter's new()
	_formatter's setLocale:(current application's NSLocale's localeWithLocaleIdentifier:"en_US_POSIX")
	_formatter's setFormat:"####.######"
	return (_formatter's stringFromNumber:_number) as text
end formatNumber

--
-- Converts a date to a string to RFC 3339 format.
--
-- _date: 			the date to convert
--
on dateAsRfc3339(_date)
	set _formatter to current application's NSDateFormatter's new()
	_formatter's setLocale:(current application's NSLocale's localeWithLocaleIdentifier:"en_US_POSIX")
	_formatter's setDateFormat:"yyyy'-'MM'-'dd'T'HH':'mm':'ssXXX"
	return (_formatter's stringFromDate:_date) as text
end dateAsRfc3339

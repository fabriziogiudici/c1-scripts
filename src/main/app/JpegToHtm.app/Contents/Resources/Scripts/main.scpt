-- Scripts for Capture One
-- ===============
--
-- Copyright (C) 2019-2023 by Tidalwave s.a.s. (http://tidalwave.it)
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.

on open _droppedItems
	set _dialog to missing value

	repeat with a from 1 to length of _droppedItems
		set _droppedItem to item a of _droppedItems
		tell application "Finder" to set _fileName to name of (info for _droppedItem)
		set _offset to offset of "-thm" in _fileName

		if _offset < 0 then
			set _dialog to "Invalid file: " & _fileName
		else
			set _prefix to text 1 thru (_offset - 1) of _fileName
			set _suffix to text _offset thru -1 of _fileName

			if _suffix is not "-thm.jpg" then
				set _dialog to "Invalid file: " & _fileName & " - " & _suffix
			else
				tell application "Finder"
					set _newFileName to _prefix & ".thm"
					set _folder to container of _droppedItem
					set _oldFile to (_folder as text) & _newFileName

					if exists file _oldFile then
						delete file _oldFile
					end if

					set name of _droppedItem to _newFileName
				end tell
			end if
		end if

		if _dialog is not missing value then
			display dialog _dialog
		end if
	end repeat
end open



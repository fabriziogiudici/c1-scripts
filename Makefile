TARGET=target
APPLESCRIPT_SRC=src/main/applescript
TEST_SRC=src/test
C1_TARGET=$(HOME)/Library/Scripts/Capture One Scripts

all:	clean compile

clean:
	rm -rfv $(TARGET)
	mvn clean
	mkdir $(TARGET) $(TARGET)/test $(TARGET)/resources

compile:	
	mvn clean install -DskipTests

test-catalog:
	mvn -f modules/Tools/pom.xml surefire:test

run-catalog:
	mvn -q -o -f modules/Tools/pom.xml -Pcatalog
	time osascript target/CreateCatalog.applescript

run-export-galleries:
	time osascript "$(APPLESCRIPT_SRC)/Export collections.applescript"

run-export-variants:
	time osascript "$(APPLESCRIPT_SRC)/Export variants.applescript" $(args)

run-prune:
	mvn -q -o -f modules/Tools/pom.xml -Pprune

test-prune:
	mvn -q -o -f modules/Tools/pom.xml -Pprune -Dexec.arguments=--dry-run

copy-export-variants:
	cp "$(APPLESCRIPT_SRC)/Export variants.applescript" "$(C1_TARGET)"

copy-export-collections:
	cp "$(APPLESCRIPT_SRC)/Export collections.applescript" "$(C1_TARGET)"

copy-set-black-level:
	cp "$(APPLESCRIPT_SRC)/Set black level.applescript" "$(C1_TARGET)"

copy-export-names-of-selected-images:
	cp "$(APPLESCRIPT_SRC)/Export names of selected images.applescript" "$(C1_TARGET)"

copy-new-session:
	cp "$(APPLESCRIPT_SRC)/New session.applescript" "$(C1_TARGET)"

on run (_args)
	tell front document of application "Capture One 22"
		set _command to item 1 of _args
		
		if _command is "--byId" then
			set _collection to my findCollectionByIdPath(_args)
			my collectionToJson(_collection)
			
		else if _command is "--byName" then
			set _collection to collection named (item 2 of _args)
			
			repeat with _i from 3 to count of _args
				tell _collection to set _collection to collection named (item _i of _args)
			end repeat
			
			my collectionToJson(_collection)
			
		else if _command is "--children" then
			set _collection to my findCollectionByIdPath(_args)
			tell _collection to my collectionsToJson(collections)
			
		else if _command is "--variants" then
			set _collection to my findCollectionByIdPath(_args)
			tell _collection to my variantsToJson(variants)
		else
			log "Unknown command: " & _command
		end if
	end tell
end run

on findCollectionByIdPath(_args)
	tell front document of application "Capture One 22"
		set _collection to first collection whose its id is item 2 of _args
		
		repeat with _i from 3 to count of _args
			tell _collection to set _collection to (first collection whose its id is (item _i of _args))
		end repeat
		
		return _collection
	end tell
end findCollectionByIdPath

on collectionsToJson(_collections)
	set _result to "["
	set _separator to ""
	repeat with _collection in _collections
		set _result to _result & _separator & my collectionToJson(_collection)
		set _separator to ","
	end repeat
	return _result & "]"
end collectionsToJson

on collectionToJson(_collection)
	tell application "Capture One 22" to return "{\"name\": " & "\"" & name of _collection & "\", \"id\": \"" & id of _collection & "\", \"kind\": " & "\"" & kind of _collection & "\", \"user\": " & user of _collection & "}"
end collectionToJson

on variantsToJson(_variants)
	set _result to "["
	set _separator to ""
	repeat with _variant in _variants
		set _result to _result & _separator & my variantToJson(_variant)
		set _separator to ","
	end repeat
	return _result & "]"
end variantsToJson

on variantToJson(_variant)
	-- FIXME: it seems that this 'tell' slows down a lot
	tell application "Capture One 22" to return ("{\"name\": \"" & (name of _variant) as text) & "\", \"id\": \"" & id of _variant & "\", \"rating\": " & rating of _variant & ", \"colorTag\": " & color tag of _variant & "}"
end variantToJson


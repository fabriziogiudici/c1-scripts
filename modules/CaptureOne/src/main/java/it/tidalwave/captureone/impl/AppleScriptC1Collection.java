/*
 * *********************************************************************************************************************
 *
 * Photography Scripts
 * http://stoppingdown.net
 *
 * Copyright (C) 2021 - 2023 by Tidalwave s.a.s. (http://tidalwave.it)
 *
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * git clone https://bitbucket.org/fabriziogiudici/c1-scripts
 *
 * *********************************************************************************************************************
 */
package it.tidalwave.captureone.impl;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import it.tidalwave.captureone.C1Collection;
import it.tidalwave.captureone.C1;
import it.tidalwave.captureone.C1Variant;
import it.tidalwave.util.Id;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

/***********************************************************************************************************************
 *
 *
 *
 **********************************************************************************************************************/
@Builder @Getter @EqualsAndHashCode(exclude = "c1") @ToString(exclude = "c1") @Slf4j
public class AppleScriptC1Collection implements C1Collection
  {
    private final C1 c1 = C1.getInstance();

    @Nonnull
    private final String name;

    private final Id id;

    @Nonnull
    private final Kind kind;

    private final boolean user;

    // One or the other is surely not null
    @Nullable
    private final NamePath nameParentPath;

    @Nullable
    private final IdPath idParentPath;

    /*******************************************************************************************************************
     *
     * {@inheritDoc}
     *
     ******************************************************************************************************************/
    @Override @Nonnull
    public Path getPath()
      {
        var path = Path.empty();

        // for performance heavily relies on cache
        if (idParentPath != null)
          {
            for (int i = 0; i < idParentPath.size(); i++)
              {
                path = path.with(c1.findCollectionByIdPath(idParentPath.head(i + 1)).get());
              }
          }
        else
          {
            for (int i = 0; i < nameParentPath.size(); i++)
              {
                path = path.with(c1.findCollectionByNamePath(nameParentPath.head(i + 1)).get());
              }
          }

        return path.with(this);
      }

    /*******************************************************************************************************************
     *
     * {@inheritDoc}
     *
     ******************************************************************************************************************/
    @Override
    public void walk (@Nonnull final Consumer<Path> consumer, @Nonnull final BiConsumer<Path, Throwable> errorHandler)
      {
        var path = getPath();
        consumer.accept(path);

        if (path.last().getKind().isCanHaveChildren())
          {
            try
              {
                c1.findChildrenCollections(path.toIdPath()).forEach(c -> c.walk(consumer, errorHandler));
              }
            catch (Exception e)
              {
                errorHandler.accept(path, e);
              }
          }
      }

    /*******************************************************************************************************************
     *
     * {@inheritDoc}
     *
     ******************************************************************************************************************/
    @Nonnull @Override
    public List<C1Variant> findVariants()
      {
        return c1.findVariants(getPath().toIdPath());
      }
  }

/*
 * *********************************************************************************************************************
 *
 * Photography Scripts
 * http://stoppingdown.net
 *
 * Copyright (C) 2021 - 2023 by Tidalwave s.a.s. (http://tidalwave.it)
 *
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * git clone https://bitbucket.org/fabriziogiudici/c1-scripts
 *
 * *********************************************************************************************************************
 */
package it.tidalwave.captureone.impl;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import it.tidalwave.captureone.C1Collection;
import it.tidalwave.captureone.C1Variant;
import it.tidalwave.util.Id;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/***********************************************************************************************************************
 *
 *
 **********************************************************************************************************************/
@RequiredArgsConstructor
public class FakeC1Collection implements C1Collection
  {
    @Getter @Nonnull
    private final String name;

    @Getter @Nonnull
    private final Kind kind;

    /*******************************************************************************************************************
     * This method is not available in this implementation.
     ******************************************************************************************************************/
    @Override
    public boolean isUser()
      {
        return true;
      }

    /*******************************************************************************************************************
     * This method is not available in this implementation.
     ******************************************************************************************************************/
    @Override @Nonnull
    public Path getPath()
      {
        throw new UnsupportedOperationException("Not available in batch implementation");
      }

    /*******************************************************************************************************************
     * This method is not available in this implementation.
     ******************************************************************************************************************/
    @Override
    public void walk (@Nonnull Consumer<Path> consumer, @Nonnull BiConsumer<Path, Throwable> errorHandler)
      {
        throw new UnsupportedOperationException("Not available in batch implementation");
      }

    /*******************************************************************************************************************
     * This method is not available in this implementation.
     ******************************************************************************************************************/
    @Override @Nonnull
    public List<C1Variant> findVariants()
      {
        throw new UnsupportedOperationException("Not available in batch implementation");
      }

    /*******************************************************************************************************************
     * This method is not available in this implementation.
     ******************************************************************************************************************/
    @Override @Nonnull
    public Id getId()
      {
        throw new UnsupportedOperationException("Not available in batch implementation");
      }
  }

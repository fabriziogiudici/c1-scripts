/*
 * *********************************************************************************************************************
 *
 * Photography Scripts
 * http://stoppingdown.net
 *
 * Copyright (C) 2021 - 2023 by Tidalwave s.a.s. (http://tidalwave.it)
 *
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * git clone https://bitbucket.org/fabriziogiudici/c1-scripts
 *
 * *********************************************************************************************************************
 */
package it.tidalwave.captureone.impl;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import java.text.Normalizer;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.io.PrintWriter;
import it.tidalwave.captureone.C1;
import it.tidalwave.captureone.C1Collection;
import it.tidalwave.captureone.C1Variant;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/***********************************************************************************************************************
 *
 * An implementation of {@link C1} that, instead of talking to the application, generates a batch of AppleScript to be
 * executed later.
 * All query methods are not implemented. Other methods that return something provide fake results.
 *
 **********************************************************************************************************************/
@RequiredArgsConstructor
public class C1Batch implements C1
  {
    @Getter @Nonnull
    private final PrintWriter pw;

    @Getter
    private int progressTotalUnits;

    @Getter
    private double percentCompleted;

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override
    public void connect()
      {
        pw.printf("global TRACE\nset TRACE to true\n");
        pw.printf("tell application \"Capture One\"\n");
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override
    public void close()
      {
        pw.printf("end tell\n");
      }

    /*******************************************************************************************************************
     * This method is not available in this implementation.
     ******************************************************************************************************************/
    @Override @Nonnull
    public Optional<C1Collection> findCollectionByNamePath (@Nonnull C1Collection.NamePath namePath)
      {
        throw new UnsupportedOperationException("Not available in batch implementation");
      }

    /*******************************************************************************************************************
     * This method is not available in this implementation.
     ******************************************************************************************************************/
    @Override @Nonnull
    public Optional<C1Collection> findCollectionByIdPath (@Nonnull C1Collection.IdPath idPath)
      {
        throw new UnsupportedOperationException("Not available in batch implementation");
      }

    /*******************************************************************************************************************
     * This method is not available in this implementation.
     ******************************************************************************************************************/
    @Override @Nonnull
    public List<C1Collection> findChildrenCollections (@Nonnull C1Collection.IdPath idPath)
      {
        throw new UnsupportedOperationException("Not available in batch implementation");
      }

    /*******************************************************************************************************************
     * This method is not available in this implementation.
     ******************************************************************************************************************/
    @Override @Nonnull
    public List<C1Variant> findVariants (@Nonnull C1Collection.IdPath idPath)
      {
        throw new UnsupportedOperationException("Not available in batch implementation");
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override @Nonnull
    public C1Collection createGroupCollection (@Nonnull final String parentName,
                                               @Nonnull final String variableName,
                                               @Nonnull final String name)
      {
        final var statement = String.format(
                "tell %s to set %s to make collection with properties {kind:group, name:\"%s\"}",
                  parentName.isEmpty() ? "front document" : asId(parentName),
                  asId(variableName),
                  escaped(name));
        execute(statement);
        return new FakeC1Collection(name, C1Collection.Kind.GROUP);
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override @Nonnull
    public C1Collection createFilterCollection (@Nonnull final String parentName,
                                                @Nonnull final String name,
                                                @Nonnull final String keyword)
      {
        var xml = """
                  <?xml version="1.0" encoding="UTF-8"?>
                  <MatchOperator Kind="AND">
                    <Condition Enabled="YES">
                      <Key>IB_S_CONTENT_KEYWORDS</Key>
                      <Operator>6</Operator>
                      <Criterion>%s</Criterion>
                    </Condition>
                  </MatchOperator>
                  """;
        final var statement = String.format(
                "tell %s to make collection with properties {kind:smart album, name:\"%s\", rules:\"%s\"}",
                  asId(parentName),
                  escaped(name),
                  String.format(xml.replaceAll("\n *", "")
                                   .replaceAll("\n", "")
                                   .replaceAll("\"", "\\\\\""),
                                escaped(keyword)));
        execute(statement);
        return new FakeC1Collection(name, C1Collection.Kind.SMART_ALBUM);
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override @Nonnull
    public C1Collection createFilterCollection (@Nonnull final String parentName,
                                                @Nonnull final String name,
                                                @Nonnull final String keyword1,
                                                @Nonnull final String keyword2)
      {
        if (keyword2.isEmpty())
          {
            return createFilterCollection(parentName, name, keyword1);
          }

        var xml = """
                  <?xml version="1.0" encoding="UTF-8"?>
                  <MatchOperator Kind="AND">
                    <MatchOperator Kind="AND">
                      <Condition Enabled="YES">
                        <Key>IB_S_CONTENT_KEYWORDS</Key>
                        <Operator>6</Operator>
                        <Criterion>%s</Criterion>
                      </Condition>
                      <Condition Enabled="YES">
                        <Key>IB_S_CONTENT_KEYWORDS</Key>
                        <Operator>6</Operator>
                        <Criterion>%s</Criterion>
                      </Condition>
                    <MatchOperator Kind="AND">
                  <MatchOperator Kind="AND">
                  """;
        final var statement = String.format(
                "tell %s to make collection with properties {kind:smart album, name:\"%s\", rules:\"%s\"}",
                  asId(parentName),
                  escaped(name),
                  String.format(xml.replaceAll("\n *", "")
                                   .replaceAll("\n", "")
                                   .replaceAll("\"", "\\\\\""),
                                keyword1, keyword2));
        execute(statement);
        return new FakeC1Collection(name, C1Collection.Kind.SMART_ALBUM);
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    public void createDateCollection (@Nonnull final String parentName,
                                      @Nonnull final String name,
                                      @Nonnull final LocalDate date)
      {
        var xml = """
                  <?xml version="1.0"?>
                  <MatchOperator Kind="AND">
                    <MatchOperator Kind="AND">
                      <Condition Enabled="YES">
                        <Key>IB_S_EXP_DATE</Key>
                        <Operator>0</Operator>
                        <Criterion>%s</Criterion>
                      </Condition>
                    </MatchOperator>
                  </MatchOperator>
                  """;
        final var statement = String.format(
                "tell %s to make collection with properties {kind:smart album, name:\"%s\", rules:\"%s\"}\n",
                  asId(parentName),
                  escaped(name),
                  String.format(xml.replaceAll("\n *", "")
                                   .replaceAll("\n", "")
                                   .replaceAll("\"", "\\\\\""), date));
        execute(statement);
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    public void createDateRangeCollection (@Nonnull final String parentName,
                                           @Nonnull final String name,
                                           @Nonnull final LocalDate date1,
                                           @Nonnull final LocalDate date2)
      {
        var xml = """
                  <?xml version="1.0"?>
                  <MatchOperator Kind="AND">
                    <MatchOperator Kind="AND">
                      <Condition Enabled="YES">
                        <Key>IB_S_EXP_DATE</Key>
                        <Operator>14</Operator>
                        <Criterion>%s</Criterion>
                      </Condition>
                      <Condition Enabled="YES">
                        <Key>IB_S_EXP_DATE</Key>
                        <Operator>15</Operator>
                        <Criterion>%s</Criterion>
                      </Condition>
                    </MatchOperator>
                  </MatchOperator>
                  """;
        final var statement = String.format(
                "tell %s to make collection with properties {kind:smart album, name:\"%s\", rules:\"%s\"}\n",
                  asId(parentName),
                  escaped(name),
                  String.format(xml.replaceAll("\n *", "")
                                   .replaceAll("\n", "")
                                   .replaceAll("\"", "\\\\\""),
                                date1.minusDays(1),
                                date2.plusDays(1)));
        execute(statement);
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override
    public void setProgressTotalUnits (@Nonnegative final int units)
      {
        this.progressTotalUnits = units;
        pw.printf("    set progress total units to %d\n", units);
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override
    public void setProgressText (@Nonnull final String text, @Nonnull final Object... args)
      {
        pw.printf("    set progress text to \"%s\"\n", escaped(String.format(text, args)));
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override
    public void setProgressAdditionalText (@Nonnull final String text, @Nonnull final Object... args)
      {
        pw.printf("    set progress additional text to \"%s\"\n", escaped(String.format(text, args)));
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override
    public void setProgress (@Nonnegative final int progress)
      {
        pw.printf("    set progress completed units to %d\n", progress);

        if (progress == 0)
          {
            pw.printf("    set ___basetime to (current date)\n");
          }
        else
          {
            pw.printf("    set ___elapsedtime to (current date) - ___basetime\n");
            percentCompleted = progress / (double)progressTotalUnits;

            if (percentCompleted > 0)
              {
                final var k = (1.0d - percentCompleted) / percentCompleted;
                pw.printf("    set ___ct to ___elapsedtime * %f\n", k);
                pw.printf("    set ___completionTime to \"\" & (___ct div 60) & \" minutes to complete\"\n");
                pw.printf("    set ___eta to time string of ((current date) + ___ct)\n");
              }
            else
              {
                pw.printf("    set ___completionTime to \"\"\n");
                pw.printf("    set ___eta to \"\"\n");
              }
          }
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override
    public void startTryCatch()
      {
        execute("try");
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override
    public void endTryCatch (@Nonnull final String statement)
      {
        execute(String.format("""
                              on error errorMessage number errorNumber
                                    log "errorMessage: " & errorMessage & ", errorNumber: " & errorNumber
                                    %s
                                  end try
                              """, statement));
      }

    /*******************************************************************************************************************
     *
     * Executes a statement, adding it to the batch.
     *
     * @param     statement   the statement to execute
     *
     ******************************************************************************************************************/
    private void execute (@Nonnull final String statement)
      {
        //pw.printf("      if TRACE then log \"%s\"\n", statement.replace("\\", "\\\\").replace("\"", "\\\""));
        pw.println("    " + statement);
      }

    /*******************************************************************************************************************
     *
     * Transform a name into a valid Appescript identifier, replacing invalid characters with '_'.
     *
     * @param     name      the name
     * @return              the valid identifier
     *
     ******************************************************************************************************************/
    @Nonnull
    private static String asId (@Nonnull final String name)
      {
        return "_" + Normalizer.normalize(name, Normalizer.Form.NFKD)
                               .replaceAll("\\p{M}", "")
                               .replace("/", "__")
                               .replaceAll("[^A-Za-z0-9]", "_");
      }

    /*******************************************************************************************************************
     *
     * Properly escapes a string literal for Applescript syntax, and expands meta-variables.
     *
     * @param     string    the string to escape
     * @return              the escaped string
     *
     ******************************************************************************************************************/
    @Nonnull
    public static String escaped (@Nonnull final String string)
      {
        return string.replaceAll("\"", "\\\\\"")
                     .replaceAll("@eta@", "\" & ___eta & \"")
                     .replaceAll("@completionTime@", "\" & ___completionTime & \"");
      }
  }

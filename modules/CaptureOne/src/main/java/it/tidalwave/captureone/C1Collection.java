/*
 * *********************************************************************************************************************
 *
 * Photography Scripts
 * http://stoppingdown.net
 *
 * Copyright (C) 2021 - 2023 by Tidalwave s.a.s. (http://tidalwave.it)
 *
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * git clone https://bitbucket.org/fabriziogiudici/c1-scripts
 *
 * *********************************************************************************************************************
 */
package it.tidalwave.captureone;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;
import it.tidalwave.util.Id;
import it.tidalwave.role.Identifiable;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/***********************************************************************************************************************
 *
 * A Capture One collection of media.
 *
 **********************************************************************************************************************/
public interface C1Collection extends Identifiable
  {
    /*******************************************************************************************************************
     *
     * The kind of a collection.
     *
     ******************************************************************************************************************/
    @RequiredArgsConstructor
    public enum Kind
      {
        ALBUM(false),
        CATALOG_FOLDER(true),
        FAVORITE(false),
        GROUP(true),
        PROJECT(true),
        SMART_ALBUM(false);

        @Getter
        private final boolean canHaveChildren;
      }

    /*******************************************************************************************************************
     *
     *
     *
     ******************************************************************************************************************/
    @EqualsAndHashCode
    static abstract class PathSupport<T, U> implements Iterable<T>
      {
        @Nonnull
        private final List<T> items;

        @Nonnull
        private final Function<List<T>, U> constructor;

        protected PathSupport (@Nonnull final List<T> items, @Nonnull final Function<List<T>, U> constructor)
          {
            this.items = Collections.unmodifiableList(items);
            this.constructor = constructor;
          }

        @Nonnull
        public T get (@Nonnegative int index)
          {
            return items.get(index);
          }

        @Nonnegative
        public int size()
          {
            return items.size();
          }

        @Nonnull
        public U head (@Nonnegative int size)
          {
            return constructor.apply(items.subList(0, size));
          }

        @Nonnull
        public T last ()
          {
            return get(size() - 1);
          }

        @Nonnull
        public U with (@Nonnull final T item)
          {
            final List<T> result = new ArrayList<>(this.items);
            result.add(item);
            return constructor.apply(result);
          }

        @Nonnull
        public Stream<T> stream()
          {
            return items.stream();
          }

        @Override @Nonnull
        public Iterator<T> iterator()
          {
            return items.iterator();
          }

        @Override @Nonnull
        public String toString()
          {
            return items.toString();
          }

        @Nonnull
        public String[] toStrings()
          {
            return stream().map(Object::toString).toArray(String[]::new);
          }

        @Nonnull
        public U getParent()
          {
            final List<T> result = new ArrayList<>(this.items);
            return constructor.apply(result.subList(0, result.size() - 1));
          }
      }

    /*******************************************************************************************************************
     *
     * A path of names.
     *
     ******************************************************************************************************************/
    public static final class NamePath extends PathSupport<String, NamePath>
      {
        private NamePath (@Nonnull final List<String> names)
          {
            super(names, NamePath::new);
          }

        @Nonnull
        public static NamePath empty()
          {
            return of();
          }

        @Nonnull
        public static NamePath of (@Nonnull final List<String> names)
          {
            return new NamePath(names);
          }

        @Nonnull
        public static NamePath of (@Nonnull final String ... names)
          {
            return of(List.of(names));
          }
      }

    /*******************************************************************************************************************
     *
     * A path of names.
     *
     ******************************************************************************************************************/
    public static final class IdPath extends PathSupport<Id, IdPath>
      {
        private IdPath (@Nonnull final List<Id> names)
          {
            super(names, IdPath::new);
          }

        @Nonnull
        public static IdPath empty()
          {
            return of();
          }

        @Nonnull
        public static IdPath of (@Nonnull final List<Id> ids)
          {
            return new IdPath(ids);
          }

        @Nonnull
        public static IdPath of (@Nonnull final Id ... ids)
          {
            return of(List.of(ids));
          }

        @Nonnull
        public static IdPath of (@Nonnull final Object ... ids)
          {
            return of(Stream.of(ids).map(Id::of).toList());
          }
      }

    /*******************************************************************************************************************
     *
     * A path of {@code MediaCollection}s.
     *
     ******************************************************************************************************************/
    public static final class Path extends PathSupport<C1Collection, Path>
      {
        private Path (@Nonnull final List<C1Collection> items)
          {
            super(items, Path::new);
          }

        @Nonnull
        public static Path empty()
          {
            return Path.of(Collections.emptyList());
          }

        @Nonnull
        public static Path of (@Nonnull final List<C1Collection> items)
          {
            return new Path(items);
          }

        @Nonnull
        public static Path of (@Nonnull final Optional<C1Collection> item)
          {
            return new Path(item.stream().toList());
          }

        @Nonnull
        public NamePath toNamePath()
          {
            return NamePath.of(stream().map(C1Collection::getName).toList());
          }

        @Nonnull
        public IdPath toIdPath()
          {
            return IdPath.of(stream().map(C1Collection::getId).toList());
          }
      }

    /*******************************************************************************************************************
     *
     * Returns the name of this collection.
     *
     * @return  the name
     *
     ******************************************************************************************************************/
    @Nonnull
    public String getName();

    /*******************************************************************************************************************
     *
     * Returns the kind of this collection.
     *
     * @return  the kind
     *
     ******************************************************************************************************************/
    @Nonnull
    public Kind getKind();

    /*******************************************************************************************************************
     *
     * Returns the user flag.
     *
     * @return  the user flag
     *
     ******************************************************************************************************************/
    public boolean isUser();

    /*******************************************************************************************************************
     *
     * @return  the path of this collection
     *
     ******************************************************************************************************************/
    @Nonnull
    public Path getPath();

    /*******************************************************************************************************************
     *
     * Recursively walks children.
     *
     * @param   consumer        the consumer of each discovered collection.
     * @param   errorHandler    the handler of errors
     *
     ******************************************************************************************************************/
    public void walk (@Nonnull Consumer<Path> consumer, @Nonnull BiConsumer<Path, Throwable> errorHandler);

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Nonnull
    public List<C1Variant> findVariants();
  }

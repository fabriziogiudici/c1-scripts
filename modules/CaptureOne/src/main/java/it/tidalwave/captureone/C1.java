/*
 * *********************************************************************************************************************
 *
 * Photography Scripts
 * http://stoppingdown.net
 *
 * Copyright (C) 2021 - 2023 by Tidalwave s.a.s. (http://tidalwave.it)
 *
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * git clone https://bitbucket.org/fabriziogiudici/c1-scripts
 *
 * *********************************************************************************************************************
 */
package it.tidalwave.captureone;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import java.util.List;
import java.util.Optional;
import java.io.IOException;
import it.tidalwave.captureone.C1Collection.IdPath;
import it.tidalwave.captureone.C1Collection.NamePath;
import it.tidalwave.captureone.impl.AppleScriptC1;

/***********************************************************************************************************************
 *
 * An adapter to talk to Capture One by means of Applescript.
 *
 **********************************************************************************************************************/
public interface C1 extends AutoCloseable
  {
    public static final C1 instance = new AppleScriptC1();

    /*******************************************************************************************************************
     *
     * Returns the singleton instance.
     *
     * @return    the instance
     *
     ******************************************************************************************************************/
    @Nonnull
    public static C1 getInstance()
      {
        return instance;
      }

    /*******************************************************************************************************************
     *
     * Connects to the application.
     *
     ******************************************************************************************************************/
    public void connect()
            throws IOException, InterruptedException;

    /*******************************************************************************************************************
     *
     * Disconnects from the application.
     *
     ******************************************************************************************************************/
    public void close()
            throws IOException, InterruptedException;

    // FIXME: use Finders
    /*******************************************************************************************************************
     *
     * Finds a {@link C1Collection} given a path of names.
     *
     * @param   namePath  the path
     * @return            the collection
     *
     ******************************************************************************************************************/
    @Nonnull
    public Optional<C1Collection> findCollectionByNamePath (@Nonnull NamePath namePath);

    /*******************************************************************************************************************
     *
     * Finds a {@link C1Collection} given a path of ids.
     *
     * @param   idPath  the path
     * @return          the collection
     *
     ******************************************************************************************************************/
    @Nonnull
    public Optional<C1Collection> findCollectionByIdPath (@Nonnull IdPath idPath);

    /*******************************************************************************************************************
     *
     * FIXME: move to MediaCollection
     *
     ******************************************************************************************************************/
    @Nonnull
    public List<C1Collection> findChildrenCollections (@Nonnull IdPath idPath);

    /*******************************************************************************************************************
     *
     * FIXME: move to MediaCollection
     *
     ******************************************************************************************************************/
    @Nonnull
    public List<C1Variant> findVariants (@Nonnull IdPath idPath);

    /*******************************************************************************************************************
     *
     * Creates a group collection with the given name, as a child of a given parent.
     *
     * @param     parentName    the name of the parent collection
     * @param     name          the name of the new collection
     * @return                  the new collection
     *
     ******************************************************************************************************************/
    @Nonnull
    public default C1Collection createGroupCollection (@Nonnull String parentName, @Nonnull String name)
      {
        return createGroupCollection(parentName, name, name);
      }

    /*******************************************************************************************************************
     *
     * Creates a group collection with the given name, as a child of a given parent. This version allows to specify
     * a specific name for the Applescript variable that references the new collection.
     *
     * @param     parentName    the name of the parent collection
     * @param     variableName  the name of the parent collection
     * @param     name          the name of the new collection
     * @return                  the new collection
     *
     ******************************************************************************************************************/
    @Nonnull
    public C1Collection createGroupCollection (@Nonnull String parentName,
                                               @Nonnull String variableName,
                                               @Nonnull String name);

    /*******************************************************************************************************************
     *
     * Creates a filter collection with the given name, as a child of a given parent. The collection will select
     * photos with the given keyword.
     *
     * @param     parentName    the name of the parent collection
     * @param     name          the name of the new collection
     * @param     keyword       the keyword to match
     * @return                  the new collection
     *
     ******************************************************************************************************************/
    @Nonnull
    public C1Collection createFilterCollection (@Nonnull String parentName, @Nonnull String name, @Nonnull String keyword);

    /*******************************************************************************************************************
     *
     * Creates a filter collection with the given name, as a child of a given parent. The collection will select
     * photos with both keywords.
     *
     * @param     parentName    the name of the parent collection
     * @param     name          the name of the new collection
     * @param     keyword1      the 1st keyword to match
     * @param     keyword2      the 2nd keyword to match
     * @return                  the new collection
     *
     ******************************************************************************************************************/
    @Nonnull
    public C1Collection createFilterCollection (@Nonnull String parentName,
                                                @Nonnull String name,
                                                @Nonnull String keyword1,
                                                @Nonnull String keyword2);

    /*******************************************************************************************************************
     *
     * @param     n
     *
     ******************************************************************************************************************/
    public void setProgressTotalUnits (@Nonnegative int n);

    /*******************************************************************************************************************
     *
     * @param     text
     * @param     args
     *
     ******************************************************************************************************************/
    public void setProgressText (@Nonnull String text, @Nonnull Object... args);

    /*******************************************************************************************************************
     *
     * @param     text
     * @param     args
     *
     ******************************************************************************************************************/
    public void setProgressAdditionalText (@Nonnull String text, @Nonnull Object... args);

    /*******************************************************************************************************************
     *
     * @param     progress
     *
     ******************************************************************************************************************/
    public void setProgress (@Nonnegative int progress);

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    public void startTryCatch();

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    public void endTryCatch (@Nonnull String statement);
  }

/*
 * *********************************************************************************************************************
 *
 * Photography Scripts
 * http://stoppingdown.net
 *
 * Copyright (C) 2021 - 2023 by Tidalwave s.a.s. (http://tidalwave.it)
 *
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * git clone https://bitbucket.org/fabriziogiudici/c1-scripts
 *
 * *********************************************************************************************************************
 */
package it.tidalwave.captureone.impl;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.io.IOException;
import java.io.StringReader;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import it.tidalwave.captureone.C1Collection;
import it.tidalwave.captureone.C1;
import it.tidalwave.captureone.C1Collection.IdPath;
import it.tidalwave.captureone.C1Collection.NamePath;
import it.tidalwave.captureone.C1Variant;
import jakarta.json.Json;
import jakarta.json.JsonObject;
import jakarta.json.JsonReader;
import it.tidalwave.util.Id;
import it.tidalwave.util.ProcessExecutor;
import lombok.extern.slf4j.Slf4j;
import static java.util.Collections.emptyList;

/***********************************************************************************************************************
 *
 *
 *
 **********************************************************************************************************************/
@Slf4j
public class AppleScriptC1 implements C1
  {
    private final ConcurrentMap<Id, C1Collection> cacheById = new ConcurrentHashMap<>();

    private final ConcurrentMap<NamePath, C1Collection> cacheByNamePath = new ConcurrentHashMap<>();

    private final Path SCRIPT_COLLECTIONS;

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    public AppleScriptC1 ()
      {
        try
          {
            // Not using compiled form since there is no performance gain:
            // https://stackoverflow.com/questions/74256917/applescript-behaves-differently-when-compiled
            SCRIPT_COLLECTIONS = Files.createTempFile("Collections", ".applescript");

            try (var is = getClass().getResourceAsStream("/Collections.applescript"))
              {
                Files.copy(is, SCRIPT_COLLECTIONS, StandardCopyOption.REPLACE_EXISTING);
              }
          }
        catch (IOException e)
          {
            throw new UncheckedIOException(e);
          }
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override
    public void connect()
            throws IOException, InterruptedException
      {
        log.info("connect()");
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override
    public void close()
            throws IOException, InterruptedException
      {
        log.info("close()");
        cacheById.clear();
        cacheByNamePath.clear();
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override @Nonnull
    public Optional<C1Collection> findCollectionByNamePath (@Nonnull final NamePath namePath)
      {
        log.info("findCollectionByName({})", namePath);
        return run("--byName", namePath.toStrings()).map(jr -> collectionFromJson(jr.readObject(), namePath.getParent()));
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override @Nonnull
    public Optional<C1Collection> findCollectionByIdPath (@Nonnull final IdPath idPath)
      {
        log.info("findCollectionById({})", idPath);
        var id = idPath.last();

        if (cacheById.containsKey(id))
          {
            return Optional.of(cacheById.get(id));
          }

        return run("--byId", idPath.toStrings()).map(jr -> collectionFromJson(jr.readObject(), idPath.getParent()));
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override @Nonnull
    public List<C1Collection> findChildrenCollections (@Nonnull final IdPath idPath)
      {
        log.info("findChildrenCollections({})", idPath);
        return run("--children", idPath.toStrings())
            .map(jr -> jr.readArray().stream().map(j -> collectionFromJson(j.asJsonObject(), idPath)).toList())
            .orElse(emptyList());
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override @Nonnull
    public List<C1Variant> findVariants (@Nonnull final IdPath idPath)
      {
        log.info("findVariants({})", idPath);
        return run("--variants", idPath.toStrings())
            .map(jr -> jr.readArray().stream().map(j -> variantFromJson(j.asJsonObject())).toList())
            .orElse(emptyList());
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override @Nonnull
    public C1Collection createGroupCollection (@Nonnull final String parentName,
                                               @Nonnull final String variableName,
                                               @Nonnull final String name)
      {
        throw new UnsupportedOperationException("Not implemented yet.");
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override @Nonnull
    public C1Collection createFilterCollection (@Nonnull final String parentName,
                                                @Nonnull final String name,
                                                @Nonnull final String keyword)
      {
        throw new UnsupportedOperationException("Not implemented yet.");
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override @Nonnull
    public C1Collection createFilterCollection (@Nonnull String parentName,
                                                @Nonnull String name1,
                                                @Nonnull String keyword1,
                                                @Nonnull String keyword2)
      {
        throw new UnsupportedOperationException("Not implemented yet.");
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override
    public void setProgressTotalUnits (@Nonnegative final int n)
      {
        throw new UnsupportedOperationException("Not implemented yet.");
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override
    public void setProgressText (@Nonnull final String text, @Nonnull final Object... args)
      {
        throw new UnsupportedOperationException("Not implemented yet.");
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override
    public void setProgressAdditionalText (@Nonnull final String text, @Nonnull final Object... args)
      {
        throw new UnsupportedOperationException("Not implemented yet.");
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override
    public void setProgress (@Nonnegative final int progress)
      {
        throw new UnsupportedOperationException("Not implemented yet.");
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override
    public void startTryCatch()
      {
        throw new UnsupportedOperationException("Not implemented yet.");
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override
    public void endTryCatch (@Nonnull final String statement)
      {
        throw new UnsupportedOperationException("Not implemented yet.");
      }

    /*******************************************************************************************************************
     *
     * Executes a command on Capture One and returns a JSON.
     *
     * @param   command   the command to pass
     * @param   args      the argument of the command
     * @return            the result in form of a JSON
     *
     ******************************************************************************************************************/
    @Nonnull
    private Optional<JsonReader> run (@Nonnull final String command, @Nonnull final String ... args)
      {
        try
          {
            var baseTime = System.currentTimeMillis();
            var script = SCRIPT_COLLECTIONS.toAbsolutePath().toString();
            log.debug("osascript {} {} {}", script, command, String.join(" ", args));
            var pe = ProcessExecutor
                    .forExecutable("/usr/bin/osascript")
                    .withArgument(script)
                    .withArguments(command)
                    .withArguments(args)
                    .start();
            log.debug(">>>> Waiting for script to complete...");
            pe.waitForCompletion();
            var json = String.join("\n", pe.getStdout().getContent());
            log.debug(">>>> JSON returned in {} msec: {}", System.currentTimeMillis() - baseTime, json);
            return Optional.of(Json.createReader(new StringReader(json)));
          }
        catch (IOException | InterruptedException e)
          {
            log.error("", e);
            throw new RuntimeException(e);
          }
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Nonnull
    private C1Collection collectionFromJson (@Nonnull final JsonObject jsonObject, @Nonnull final NamePath namePath)
      {
        return collectionFromJson(jsonObject, namePath, null);
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Nonnull
    private C1Collection collectionFromJson (@Nonnull final JsonObject jsonObject, @Nonnull final IdPath idPath)
      {
        return collectionFromJson(jsonObject, null, idPath);
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Nonnull
    private synchronized C1Collection collectionFromJson (@Nonnull final JsonObject jsonObject,
                                                          @Nullable NamePath nameParentPath,
                                                          @Nullable IdPath idParentPath)
      {
        var id = Id.of(jsonObject.getString("id"));
        var name = jsonObject.getString("name");

        var oldById = cacheById.get(id);
        var oldByNamePath = nameParentPath == null ? null : cacheByNamePath.get(nameParentPath);

        if (oldById != null)
          {
            return oldById;
          }

        if (oldByNamePath != null)
          {
            return oldByNamePath;
          }

        if (nameParentPath != null && nameParentPath.size() == 0)
          {
            idParentPath = IdPath.empty();
          }

        if (idParentPath != null && idParentPath.size() == 0)
          {
            nameParentPath = NamePath.empty();
          }

        var c1c = AppleScriptC1Collection.builder()
                                         .name(name)
                                         .id(id)
                                         .kind(C1Collection.Kind.valueOf(jsonObject.getString("kind").toUpperCase(Locale.ROOT).replace(' ', '_')))
                                         .user(jsonObject.getBoolean("user"))
                                         .nameParentPath(nameParentPath)
                                         .idParentPath(idParentPath)
                                         .build();

        cacheById.put(id, c1c);

        if (nameParentPath != null)
          {
            cacheByNamePath.put(nameParentPath, c1c);
          }

        return c1c;
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Nonnull
    private C1Variant variantFromJson (@Nonnull final JsonObject jsonObject)
      {
        // TODO: use a cache for flyweight
        return AppleScriptC1Variant.builder().name(jsonObject.getString("name"))
                                   .id(Id.of(jsonObject.getString("id")))
                                   .rating(jsonObject.getInt("rating"))
                                   .colorTag(C1Variant.ColorTag.values()[jsonObject.getInt("colorTag")])
                                   .build();
      }
  }

/*
 * *********************************************************************************************************************
 *
 * Photography Scripts
 * http://stoppingdown.net
 *
 * Copyright (C) 2021 - 2023 by Tidalwave s.a.s. (http://tidalwave.it)
 *
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * git clone https://bitbucket.org/fabriziogiudici/c1-scripts
 *
 * *********************************************************************************************************************
 */
package it.tidalwave.captureone.impl;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;
import it.tidalwave.captureone.C1Collection;
import it.tidalwave.captureone.C1;
import it.tidalwave.util.Id;
import lombok.extern.slf4j.Slf4j;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.CoreMatchers.*;
import static it.tidalwave.captureone.C1Collection.Kind.*;
import it.tidalwave.captureone.C1Collection.IdPath;
import it.tidalwave.captureone.C1Collection.NamePath;

/***********************************************************************************************************************
 *
 *
 *
 **********************************************************************************************************************/
@Slf4j
public class AppleScriptC1Test
  {
    private C1 ut;

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @BeforeMethod
    private void setup()
            throws Exception
      {
        ut = C1.getInstance();
        ut.connect();
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @AfterMethod
    private void cleanup()
            throws Exception
      {
        ut.close();
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Test(dataProvider = "findByIdData", enabled = false)
    public void must_find_collection_by_IdPath (@Nonnull IdPath idPath, @Nonnull final C1Collection expected)
      {
        // WHEN
        final var actual = ut.findCollectionByIdPath(idPath).get();
        // THEN
        assertThat(actual, is(expected));
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Test(dataProvider = "findByNameData", enabled = false)
    public void must_find_collection_by_NamePath (@Nonnull final NamePath namePath, @Nonnull final C1Collection expected)
      {
        // WHEN
        final var actual = ut.findCollectionByNamePath(namePath).get();
        // THEN
        assertThat(actual, is(expected));
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Test(enabled = false)
    public void must_find_children_collections()
      {
        // WHEN
        final var actual = ut.findChildrenCollections(IdPath.of("14659"));
        // THEN
        final var expected = List.of(
                c("Blog", 1920, GROUP, null, IdPath.of("14659")),
                c("Diary", 8433, GROUP, null, IdPath.of("14659")),
                c("Galleries", 14339, GROUP, null, IdPath.of("14659")),
                c("Others", 14655, GROUP, null, IdPath.of("14659")),
                c("Places", 14287, GROUP, null, IdPath.of("14659")),
                c("Themes", 14381, GROUP, null, IdPath.of("14659")),
                c("Twitter", 15048, GROUP, null, IdPath.of("14659")),
                c("Fine arts attempts", 99149, ALBUM, null, IdPath.of("14659")));
        assertThat(actual, is(expected));
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @DataProvider
    private static Object[][] findByIdData()
      {
        return new Object[][]
          {
            { IdPath.of("14659"),
                    c("StoppingDown", 14659, PROJECT, NamePath.empty(), IdPath.empty()) },
            { IdPath.of("14659", "1920"),
                    c("Blog", 1920, GROUP, null, IdPath.of("14659")) }
          };
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @DataProvider
    private static Object[][] findByNameData()
      {
        return new Object[][]
          {
            { NamePath.of("StoppingDown"),
                    c("StoppingDown", 14659, PROJECT, NamePath.empty(), IdPath.empty()) },
            { NamePath.of("StoppingDown", "Diary"),
                    c("Diary", 8433, GROUP, NamePath.of("StoppingDown"), null) }
          };
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Nonnull
    private static C1Collection c (@Nonnull final String name,
                                   final int id,
                                   @Nonnull final C1Collection.Kind kind,
                                   @Nullable NamePath nameParentPath,
                                   @Nullable IdPath idParentPath)
      {
        return AppleScriptC1Collection.builder()
                                      .name(name)
                                      .id(Id.of("" + id))
                                      .kind(kind)
                                      .user(true)
                                      .nameParentPath(nameParentPath)
                                      .idParentPath(idParentPath)
                                      .build();
      }
  }

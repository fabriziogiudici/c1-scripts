/*
 * *********************************************************************************************************************
 *
 * Photography Scripts
 * http://stoppingdown.net
 *
 * Copyright (C) 2021 - 2023 by Tidalwave s.a.s. (http://tidalwave.it)
 *
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * git clone https://bitbucket.org/fabriziogiudici/c1-scripts
 *
 * *********************************************************************************************************************
 */
package it.tidalwave.captureone.impl;

import java.util.ArrayList;
import java.nio.file.Files;
import java.nio.file.Path;
import it.tidalwave.captureone.C1;
import lombok.extern.slf4j.Slf4j;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import it.tidalwave.captureone.C1Collection.NamePath;
import it.tidalwave.captureone.C1Collection.IdPath;
import it.tidalwave.captureone.C1Variant;
import static java.nio.charset.StandardCharsets.UTF_8;
import static it.tidalwave.util.test.FileComparisonUtils.assertSameContents;

/***********************************************************************************************************************
 *
 *
 *
 **********************************************************************************************************************/
@Slf4j
public class AppleScriptC1CollectionTest
  {
    private static final Path COLLECTION_EXPECTED_PATH = Path.of("src/test/resources/expected-results/collections.txt");
    private static final Path COLLECTION_ACTUAL_PATH = Path.of("target/test-results/collections.txt");
    private static final Path VARIANTS_EXPECTED_PATH = Path.of("src/test/resources/expected-results/variants.txt");
    private static final Path VARIANTS_ACTUAL_PATH = Path.of("target/test-results/variants.txt");

    private C1 c1;

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @BeforeMethod
    private void setup()
            throws Exception
      {
        c1 = C1.getInstance();
        c1.connect();
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @AfterMethod
    private void cleanup()
            throws Exception
      {
        c1.close();
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Test(enabled = false)
    public void test_walk()
            throws Exception
      {
        // GIVEN
        var ut = c1.findCollectionByNamePath(NamePath.of("StoppingDown")).get();
        var o = new ArrayList<String>();
        // WHEN
        ut.walk(
            p -> o.add(p.toNamePath() + ": " + p.last()),
            (p, t) -> log.warn(p.toNamePath().toString(), t));
        // THEN
        Files.createDirectories(COLLECTION_ACTUAL_PATH.getParent());
        Files.write(COLLECTION_ACTUAL_PATH, o.stream().sorted().toList(), UTF_8);
        assertSameContents(COLLECTION_EXPECTED_PATH, COLLECTION_ACTUAL_PATH);
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Test(enabled = false)
    public void test_findImages()
            throws Exception
      {
        // GIVEN
        var ut = c1.findCollectionByIdPath(IdPath.of("14659", "14287", "14500")).get();
        // WHEN
        var variants = ut.findVariants();
        // THEN
        Files.createDirectories(VARIANTS_ACTUAL_PATH.getParent());
        Files.write(VARIANTS_ACTUAL_PATH, variants.stream().map(C1Variant::toString).toList(), UTF_8);
        assertSameContents(VARIANTS_EXPECTED_PATH, VARIANTS_ACTUAL_PATH);
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Test(enabled = false)
    public void test_findImages2()
            throws Exception
      {
        // GIVEN
        var ut = c1.findCollectionByNamePath(NamePath.of("StoppingDown", "Places", "Liguria")).get();
        // WHEN
        var variants = ut.findVariants();
        // THEN
        Files.createDirectories(VARIANTS_ACTUAL_PATH.getParent());
        Files.write(VARIANTS_ACTUAL_PATH, variants.stream().map(C1Variant::toString).toList(), UTF_8);
        assertSameContents(VARIANTS_EXPECTED_PATH, VARIANTS_ACTUAL_PATH);
      }
  }

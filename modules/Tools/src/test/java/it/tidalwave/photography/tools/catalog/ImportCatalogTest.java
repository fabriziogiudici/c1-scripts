package it.tidalwave.photography.tools.catalog;

import javax.annotation.Nonnull;
import java.util.List;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import static it.tidalwave.util.test.FileComparisonUtils.assertSameContents;

/***********************************************************************************************************************
 *
 **********************************************************************************************************************/
public class ImportCatalogTest
  {
    @Test(dataProvider = "testSets")
    public void test (final @Nonnull String testSetName)
            throws IOException, InterruptedException
      {
        // GIVEN
        System.setProperty(C1CollectionGenerator.PROP_TEST, "true");
        final var testSet = Path.of(System.getenv(testSetName));
        final var files = List.of(testSet.resolve("files/Digital"), testSet.resolve("files/Incoming"));
        final var actualResult = Path.of("target/test-results/" + testSetName + "/CreateCatalog.applescript");
        final var expectedResult = testSet.resolve("expected-results/CreateCatalog.applescript");
        Files.createDirectories(actualResult.getParent());
        // WHEN
        ImportCatalog.run(testSet, files, actualResult);
        // THEN
        assertSameContents(expectedResult, actualResult);
      }

    @DataProvider
    private static Object[][] testSets()
      {
        return new Object[][] {/* { "TEST_SET_20221230_1550" }, */ { "TEST_SET_20241121_1034" } };
      }
  }

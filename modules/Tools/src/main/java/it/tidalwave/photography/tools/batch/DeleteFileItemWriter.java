/*
 * *********************************************************************************************************************
 *
 * Photography Scripts
 * http://stoppingdown.net
 *
 * Copyright (C) 2021 - 2023 by Tidalwave s.a.s. (http://tidalwave.it)
 *
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * git clone https://bitbucket.org/fabriziogiudici/c1-scripts
 *
 * *********************************************************************************************************************
 */
package it.tidalwave.photography.tools.batch;

import javax.annotation.Nonnull;
import java.util.Optional;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import org.springframework.batch.item.Chunk;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemStream;
import org.springframework.batch.item.ItemWriter;
import it.tidalwave.io.FileUtils;
import it.tidalwave.photography.tools.prune.GeneralOptions;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import static it.tidalwave.util.FunctionalCheckedExceptionWrappers.*;

/***********************************************************************************************************************
 *
 **********************************************************************************************************************/
@RequiredArgsConstructor @Slf4j
public class DeleteFileItemWriter implements ItemWriter<Path>, ItemStream
  {
    @Nonnull
    private final Path root;

    @Nonnull
    private final GeneralOptions generalOptions;

    @Nonnull
    private final FileUtils.ScanListener<?> listener;

    @Override
    public void open (@Nonnull final ExecutionContext executionContext)
      {
        listener.starting();
      }

    @Override
    public void close()
      {
        listener.completed(Optional.empty());
      }

    @Override
    public void write (@Nonnull final Chunk<? extends Path> chunk)
      {
        chunk.forEach(_c(this::delete));
      }

    private void delete (@Nonnull final Path path)
            throws IOException
      {
        listener.progress(root, path);

        if (!generalOptions.isDryRun())
          {
            Files.deleteIfExists(path);
          }
        else
          {
            System.err.printf("  not really deleting, dry-run%s\n", "\u001b[0K" /* ERASE_EOL */);
          }
      }
  }

/*
 * *********************************************************************************************************************
 *
 * Photography Scripts
 * http://stoppingdown.net
 *
 * Copyright (C) 2021 - 2023 by Tidalwave s.a.s. (http://tidalwave.it)
 *
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * git clone https://bitbucket.org/fabriziogiudici/c1-scripts
 *
 * *********************************************************************************************************************
 */
package it.tidalwave.photography.tools.catalog;

import javax.annotation.Nonnull;
import java.time.LocalDateTime;
import java.util.function.BiFunction;
import java.io.IOException;
import it.tidalwave.captureone.C1;
import it.tidalwave.photography.core.keywords.Keyword;
import it.tidalwave.photography.core.keywords.KeywordContainer;
import it.tidalwave.photography.core.keywords.KeywordSet;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

/***********************************************************************************************************************
 *
 **********************************************************************************************************************/
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class C1CollectionGenerator
  {
    public static interface Customiser
      {
        public void onInit (@Nonnull C1 c1, @Nonnull String catalogName);

        public default boolean test (@Nonnull C1 c1, @Nonnull Keyword keyword)
          {
            return true;
          }

        public void onKeyword (@Nonnull C1 c1, @Nonnull KeywordContainer parent, @Nonnull Keyword keyword);
      }

    public static final String PROP_TEST = C1CollectionGenerator.class.getSimpleName() + ".test";

    @Nonnull
    private final C1 c1;

    @Nonnull
    private final String catalogName;

    @Nonnull
    private final KeywordSet keywordSet;

    private int progress;

    private int total;

    /*******************************************************************************************************************
     *
     * @param     keywordSet
     * @param     c1
     *
     ******************************************************************************************************************/
    public static void generate (@Nonnull final C1 c1, @Nonnull final KeywordSet keywordSet)
            throws IOException, InterruptedException
      {
        final var catalogName = Boolean.getBoolean(PROP_TEST) ? "Test Catalog"
                                                              : String.format("Catalog %s", LocalDateTime.now());
        new C1CollectionGenerator(c1, catalogName, keywordSet).run();
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    private void run()
            throws IOException, InterruptedException
      {
        // FIXME: clone keywordSet? Don't, if you add a method to keywordSet to make it immutable
        c1.connect();
        final var customiser = new FGCustomiser();
        total = keywordSet.count(k -> customiser.test(c1, k));
        c1.createGroupCollection("", catalogName);
        c1.setProgressTotalUnits(total);
        c1.setProgress(0);
        customiser.onInit(c1, catalogName);
        keywordSet.visit((KeywordContainer)keywordSet, (p, k) -> processCollection(customiser, p, k));
        c1.close(); // FIXME: finally
      }

    /*******************************************************************************************************************
     *
     * Generates code for a keywords. The signature complies with the functional interface required by
     * {@link KeywordContainer#visit(Object, BiFunction)}.
     *
     * @param     parent    the keyword parent
     * @param     keyword   the keyword
     * @return              the keyword, in compliance with {@code KeywordContainer#visit(Object, BiFunction)}
     *
     ******************************************************************************************************************/
    @Nonnull
    private KeywordContainer processCollection (@Nonnull final Customiser customiser,
                                                @Nonnull final KeywordContainer parent,
                                                @Nonnull final Keyword keyword)
      {
        if (customiser.test(c1, keyword))
          {
            c1.startTryCatch();
            c1.setProgressText("Importing: %s", keyword.getPathAsString());
            customiser.onKeyword(c1, parent, keyword);
            c1.setProgress(++progress);
            c1.setProgressAdditionalText("%d / %d — %d%% @completionTime@ @eta@",
                                         progress, total, (100 * progress) / total);
            c1.endTryCatch(String.format("log \">>>> keyword: %s\"", keyword.getPathAsString()));
          }

        return keyword;
      }
  }

/*
 * *********************************************************************************************************************
 *
 * Photography Scripts
 * http://stoppingdown.net
 *
 * Copyright (C) 2021 - 2023 by Tidalwave s.a.s. (http://tidalwave.it)
 *
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * git clone https://bitbucket.org/fabriziogiudici/c1-scripts
 *
 * *********************************************************************************************************************
 */
package it.tidalwave.photography.tools.prune;

import javax.annotation.Nonnull;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import it.tidalwave.util.PreferencesHandler;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import static org.springframework.batch.core.BatchStatus.*;

/***********************************************************************************************************************
 *
 **********************************************************************************************************************/
@SpringBootApplication(scanBasePackageClasses = PruneConfiguration.class)
@RequiredArgsConstructor @Slf4j
public class Main implements CommandLineRunner
  {
    @Nonnull
    private final JobLauncher jobLauncher;

    @Nonnull
    private final Map<String, Job> jobMapByName;

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Override
    public void run (@Nonnull final String ... args)
            throws JobInstanceAlreadyCompleteException, JobExecutionAlreadyRunningException,
                   JobParametersInvalidException, JobRestartException, InterruptedException
      {
        final var job = jobMapByName.get("pruneJob");
        final var parameters = new JobParametersBuilder()
                .addLocalDateTime("time", LocalDateTime.now()) // FIXME: should be unneeded with runincrementer
                .toJobParameters();
        final var jobExecution = jobLauncher.run(job, parameters);

        while (List.of(STARTING, STARTED).contains(jobExecution.getStatus())) // FIXME
          {
            Thread.sleep(1000);
          }
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    public static void main (@Nonnull final String ... args)
      {
        PreferencesHandler.setAppName("PhotoScripts");
        System.exit(SpringApplication.exit(SpringApplication.run(Main.class, args)));
      }
  }

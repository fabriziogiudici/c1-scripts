/*
 * *********************************************************************************************************************
 *
 * Photography Scripts
 * http://stoppingdown.net
 *
 * Copyright (C) 2021 - 2023 by Tidalwave s.a.s. (http://tidalwave.it)
 *
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * git clone https://bitbucket.org/fabriziogiudici/c1-scripts
 *
 * *********************************************************************************************************************
 */
package it.tidalwave.photography.tools.batch;

import javax.annotation.Nonnull;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.nio.file.Path;
import org.springframework.batch.item.ItemProcessor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/***********************************************************************************************************************
 *
 * An item processor that filters items using a {@link Predicate}.
 *
 **********************************************************************************************************************/
@RequiredArgsConstructor @Slf4j
public class FilterItemProcessor<T> implements ItemProcessor<T, T>
  {
    private final Path home = Path.of(System.getProperty("user.home"));

    @Nonnull
    private final Supplier<Predicate<T>> predicateSupplier;

    @Override
    public T process (@Nonnull final T item)
      {
        log.debug("process({})", item);
        Object x = item;

        if (item instanceof Path path)
          {
            x = "~/" + home.relativize(path);
          }

        System.err.printf("Checking: %s\u001B[0K\r", x); // FIXME: listener
        return predicateSupplier.get().test(item) ? item : null;
      }
  }

/*
 * *********************************************************************************************************************
 *
 * Photography Scripts
 * http://stoppingdown.net
 *
 * Copyright (C) 2021 - 2023 by Tidalwave s.a.s. (http://tidalwave.it)
 *
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * git clone https://bitbucket.org/fabriziogiudici/c1-scripts
 *
 * *********************************************************************************************************************
 */
package it.tidalwave.photography.tools.catalog;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import it.tidalwave.captureone.impl.C1Batch;
import it.tidalwave.io.FileUtils;
import it.tidalwave.photography.tools.ConsoleListeners;
import it.tidalwave.photography.tools.model.Photo;
import it.tidalwave.photography.tools.Settings;
import it.tidalwave.photography.core.keywords.KeywordSet;
import it.tidalwave.photography.core.keywords.KeywordLoader;
import lombok.extern.slf4j.Slf4j;
import static it.tidalwave.photography.tools.Utils.*;
import static java.nio.charset.StandardCharsets.UTF_8;
import static it.tidalwave.util.FunctionalCheckedExceptionWrappers.*;

/***********************************************************************************************************************
 *
 **********************************************************************************************************************/
@Slf4j
public class ImportCatalog
  {
    private static final Settings settings = new Settings();

    public static void main (@Nonnull final String ... args)
            throws IOException, InterruptedException
      {
        run(settings.getPhotos(), settings.getPhotoRoots(), Path.of("target/CreateCatalog.applescript"));
      }

    public static void run (@Nonnull final Path basePath,
                            @Nonnull final List<Path> roots,
                            @Nonnull final Path applescript)
            throws IOException, InterruptedException
      {
        var listeners = new ConsoleListeners(System.err, basePath);
        final FileUtils.FunctionalScanListener<List<Photo>> l =
                (FileUtils.FunctionalScanListener)listeners.newDiscoverListener();
        var files = FileUtils.<List<Photo>>scan(roots)
                                           .withFilter(hasExtension(settings.getPhotoExtensions()))
                                           .withCollector(toPhotos())
                                           .withListener(l)
                                           .run();

        var keywordCatalog = new KeywordSet();
        var count = new AtomicInteger();
        files.stream()
             .sorted()
             .parallel()
             .peek(p -> System.err.printf("%d/%d: %s\u001b[0K\r",
                                         count.incrementAndGet(), files.size(), basePath.relativize(p.getPath())))
             .forEach(_c(p -> ImportCatalog.processXmp(p.getPath(), keywordCatalog)));

        var path = Path.of("target/catalog.txt");
        var buffer = new ArrayList<String>();
        keywordCatalog.dump(buffer::add);
        Files.write(path, buffer);

        try (var pw = new PrintWriter(Files.newBufferedWriter(applescript, UTF_8)))
          {
            C1CollectionGenerator.generate(new C1Batch(pw), keywordCatalog);
          }
      }

    private static void processXmp (@Nonnull final Path path, @Nonnull final KeywordSet keywordCatalog)
      {
        try
          {
            log.info("Processing {} ...", path);
            var keywordLoader = new KeywordLoader();
            var keywordSet = keywordLoader.loadKeywords(path); // TODO: filter out /Places
            keywordSet.dump(s -> log.debug("    {}", s));
            keywordCatalog.merge(keywordSet);
          }
        catch (Exception e)
          {
            log.error("Processing " + path, e);
            System.err.printf("ERROR: %s: %s\n", path, e);
          }
      }
  }

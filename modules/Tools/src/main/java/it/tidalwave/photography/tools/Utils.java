/*
 * *********************************************************************************************************************
 *
 * Photography Scripts
 * http://stoppingdown.net
 *
 * Copyright (C) 2021 - 2023 by Tidalwave s.a.s. (http://tidalwave.it)
 *
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * git clone https://bitbucket.org/fabriziogiudici/c1-scripts
 *
 * *********************************************************************************************************************
 */
package it.tidalwave.photography.tools;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.TreeMap;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.nio.file.Files;
import java.nio.file.Path;
import it.tidalwave.io.FileUtils;
import it.tidalwave.photography.tools.model.Photo;
import static it.tidalwave.io.FileUtils.*;
import static java.util.Locale.ROOT;
import static java.util.stream.Collectors.*;
import static it.tidalwave.util.FunctionalCheckedExceptionWrappers.*;

public class Utils
  {
    @Nonnull
    public static Collector<Path, ?, List<Photo>> toPhotos()
      {
        // pass through a map so duplicates can be detected
        return collectingAndThen(toMap(FileUtils::nameOf, _f(Photo::new), Utils::rejectDuplicates, TreeMap::new),
                                 m -> new ArrayList<>(m.values()));
      }

    @Nonnull
    public static Predicate<Path> hasExtension (@Nonnull final String extension)
      {
        return p -> Files.isRegularFile(p) && extension.equals(extensionOf(p).toLowerCase(ROOT));
      }

    @Nonnull
    public static Predicate<Path> hasExtension (@Nonnull final Collection<String> extensions)
      {
        return p -> Files.isRegularFile(p) && extensions.contains(extensionOf(p).toLowerCase(ROOT));
      }

    public static boolean isJpeg (@Nonnull final Path path)
      {
        return hasExtension("jpg").test(path);
      }

    public static boolean isHeic (@Nonnull final Path path)
      {
        return hasExtension("heic").test(path);
      }

    public static boolean isThm (@Nonnull final Path path)
      {
        return hasExtension("thm").test(path);
      }

    @Nonnull
    public static <T> T rejectDuplicates (@Nonnull final T a, @Nonnull final T b)
      {
        if (a.equals(b))
          {
            throw new RuntimeException("Duplicate: " + a);
          }

        return a;
      }
  }

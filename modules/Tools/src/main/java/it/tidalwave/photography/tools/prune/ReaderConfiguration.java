/*
 * *********************************************************************************************************************
 *
 * Photography Scripts
 * http://stoppingdown.net
 *
 * Copyright (C) 2021 - 2023 by Tidalwave s.a.s. (http://tidalwave.it)
 *
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * git clone https://bitbucket.org/fabriziogiudici/c1-scripts
 *
 * *********************************************************************************************************************
 */
package it.tidalwave.photography.tools.prune;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.List;
import java.nio.file.Path;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import it.tidalwave.io.FileUtils;
import it.tidalwave.photography.tools.Listeners;
import it.tidalwave.photography.tools.Settings;
import it.tidalwave.photography.tools.batch.FileScanItemReader;
import it.tidalwave.photography.tools.model.Photo;
import lombok.RequiredArgsConstructor;
import static it.tidalwave.photography.tools.Utils.*;

/***********************************************************************************************************************
 *
 * Common Spring Batch configuration for item readers and writers.
 *
 **********************************************************************************************************************/
@Configuration @RequiredArgsConstructor
public class ReaderConfiguration
  {
    @Nonnull
    private final Settings settings;

    @Nonnull
    private final Listeners listeners;

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Bean @StepScope
    public FileScanItemReader<Photo> photoReader()
      {
        final FileUtils.FunctionalScanListener<List<Photo>> l =
                (FileUtils.FunctionalScanListener)listeners.newDiscoverListener();
        return FileScanItemReader.<Photo>builder()
                                 .name("photoItemReader")
                                 .scanRoots(settings.getPhotoRoots())
                                 .filter(hasExtension(settings.getPhotoExtensions()))
                                 .mapper(Photo::new)
                                 .collector(toPhotos())
                                 .listener(l)
                                 .build();
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Bean @StepScope
    public FileScanItemReader<Path> thmReader()
      {
        return newFileScanReader("thmReader",
                                 settings.getPhotoRoots(),
                                 List.of("thm"),
                                 listeners.newDiscoverListener());
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Bean @StepScope
    public FileScanItemReader<Path> longTermAndBackgroundsReader()
      {
        return newFileScanReader("longTermAndBackgroundsReader",
                                 List.of(settings.getLongTermArchivalRoots(), settings.getBackgrounds()),
                                 List.of("jpg", "heic"),
                                 listeners.newDiscoverListener());
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Bean @StepScope
    public FileScanItemReader<Path> backgroundsReader()
      {
        return newFileScanReader("backgroundsReader",
                                 List.of(settings.getBackgrounds()),
                                 List.of("jpg", "heic"),
                                 listeners.newDiscoverListener());
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Bean @StepScope
    public FileScanItemReader<Path> stoppingDownReader()
      {
        return newFileScanReader("stoppingDownReader",
                                 List.of(settings.getStoppingDownImages()),
                                 List.of("jpg"),
                                 listeners.newDiscoverListener());
      }

    /*******************************************************************************************************************
     *
     * Convenience method for creating an item reader.
     *
     * @param     name          the name of the reader
     * @param     roots         the root paths to scan
     * @param     extensions    the extensions of the file to read
     * @param     listener      the listener
     * @return                  the reader
     *
     ******************************************************************************************************************/
    @Nonnull @StepScope
    private static FileScanItemReader<Path> newFileScanReader (
            @Nonnull final String name,
            @Nonnull final List<Path> roots,
            @Nonnull final Collection<String> extensions,
            @Nonnull final FileUtils.ScanListener<Collection<?>> listener)
      {
        return FileScanItemReader.<Path>builder()
                                 .name(name)
                                 .scanRoots(roots)
                                 .filter(hasExtension(extensions))
                                 .listener((FileUtils.ScanListener)listener)
                                 .build();
      }
  }

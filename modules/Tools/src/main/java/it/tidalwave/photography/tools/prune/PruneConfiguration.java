/*
 * *********************************************************************************************************************
 *
 * Photography Scripts
 * http://stoppingdown.net
 *
 * Copyright (C) 2021 - 2023 by Tidalwave s.a.s. (http://tidalwave.it)
 *
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * git clone https://bitbucket.org/fabriziogiudici/c1-scripts
 *
 * *********************************************************************************************************************
 */
package it.tidalwave.photography.tools.prune;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;
import java.util.function.Predicate;
import java.io.IOException;
import java.nio.file.Path;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.job.builder.JobBuilder;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.step.builder.StepBuilder;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.transaction.PlatformTransactionManager;
import it.tidalwave.photography.tools.Listeners;
import it.tidalwave.photography.tools.Settings;
import it.tidalwave.photography.tools.batch.AccumulatorWriter;
import it.tidalwave.photography.tools.batch.DeleteFileItemWriter;
import it.tidalwave.photography.tools.batch.FileScanItemReader;
import it.tidalwave.photography.tools.batch.FilterItemProcessor;
import it.tidalwave.photography.tools.batch.LoadKeywordsItemProcessor;
import it.tidalwave.photography.tools.model.Photo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import static it.tidalwave.io.FileUtils.*;
import static it.tidalwave.photography.tools.Utils.*;
import static java.util.stream.Collectors.*;
import static it.tidalwave.util.FunctionalCheckedExceptionWrappers.*;

/***********************************************************************************************************************
 *
 * The Job configuration for pruning files.
 *
 **********************************************************************************************************************/
@Configuration @RequiredArgsConstructor @Slf4j
public class PruneConfiguration
  {
    public static final String K_BACKGROUNDS = "Workflow/workflow:published/workflow:published#backgrounds";
    public static final String K_STOPPINGDOWN = "Workflow/workflow:published/stoppingdown";

    @Nonnull
    private final JobRepository jobRepository;

    @Nonnull
    private final PlatformTransactionManager transactionManager;

    @Nonnull
    private final TaskExecutor taskExecutor;

    @Nonnull
    private final Settings settings;

    @Nonnull
    private final Listeners listeners;

    @Value("${throttleLimit:16}") // FIXME: set default to processor count
    private int throttleLimit;

    @Value("${chunkSize:1}")
    private int chunkSize;

    /*******************************************************************************************************************
     *
     * The prune job.
     *
     ******************************************************************************************************************/
    @Bean
    public Job pruneJob (@Qualifier("discoverStep") final Step discoverStep,
                         @Qualifier("pruneThmsStep") final Step pruneThmsStep,
                         @Qualifier("pruneLongTermAndBackgroundsStep") final Step pruneLongTermAndBackgroundsStep,
                         @Qualifier("pruneBackgroundsStep") final Step pruneBackgroundsStep,
                         @Qualifier("pruneStoppingDownStep") final Step pruneStoppingDownStep)
      {
        return new JobBuilder("pruneJob", jobRepository)
                .incrementer(new RunIdIncrementer())
                .listener(listeners.newBatchListener())
                .start(discoverStep)
                .next(pruneThmsStep)
                .next(pruneLongTermAndBackgroundsStep)
                .next(pruneBackgroundsStep)
                .next(pruneStoppingDownStep)
                .build();
      }

    /*******************************************************************************************************************
     *
     * The discover step scans the photo files and stores them into the {@code AccumulatorWriter}.
     *
     ******************************************************************************************************************/
    @Bean
    public Step discoverStep (
            final FileScanItemReader<Photo> fileScanReader,
            @Qualifier("loadKeywordsProcessor") final ItemProcessor<Photo, Photo> loadKeywordsProcessor,
            final AccumulatorWriter<Photo> accumulatorWriter)
      {
        return createStep("discoverStep", fileScanReader, loadKeywordsProcessor, accumulatorWriter);
      }

    /*******************************************************************************************************************
     *
     * The pruneThmsStep scans the .THM files and deletes those that don't match existing photos any longer.
     *
     ******************************************************************************************************************/
    @Bean
    public Step pruneThmsStep (@Qualifier("thmReader") FileScanItemReader<Path> thmReader,
                               @Qualifier("redundantThmFilter") final ItemProcessor<Path, Path> redundantThmFilter,
                               final DeleteFileItemWriter deleteFileWriter)
      {
        return createStep("pruneThmsStep", thmReader, redundantThmFilter, deleteFileWriter);
        // FIXME: first scan galleries and blog posts, and stop if one of the images to be deleted is there.
      }

    /*******************************************************************************************************************
     *
     * The pruneLongTermAndBackgroundsStep scans long term archival files and backgrounds and deletes those that don't
     * match existing photos any longer.
     *
     ******************************************************************************************************************/
    @Bean
    public Step pruneLongTermAndBackgroundsStep (
        @Qualifier("longTermAndBackgroundsReader") final FileScanItemReader<Path> longTermAndBackgroundsReader,
        @Qualifier("redundantLongTermArchivalFilter") final ItemProcessor<Path, Path> redundantLongTermArchivalFilter,
        final DeleteFileItemWriter deleteFileWriter)
      {
        return createStep("pruneLongTermAndBackgroundsStep",
                          longTermAndBackgroundsReader,
                          redundantLongTermArchivalFilter,
                          deleteFileWriter);
      }

    /*******************************************************************************************************************
     *
     * The pruneBackgroundsStep scan backgrounds and deletes those that don't match photos with the related keyword any
     * longer.
     *
     ******************************************************************************************************************/
    @Bean
    public Step pruneBackgroundsStep (
        @Qualifier("backgroundsReader") final FileScanItemReader<Path> backgroundsReader,
        @Qualifier("redundantBackgroundKeywordFilter") final ItemProcessor<Path, Path> redundantBackgroundKeywordFilter,
        final DeleteFileItemWriter deleteFileWriter)
      {
        return createStep("pruneBackgroundsStep",
                          backgroundsReader,
                          redundantBackgroundKeywordFilter,
                          deleteFileWriter);
      }

    /*******************************************************************************************************************
     *
     * The pruneStoppingDownStep scan published photos and deletes those that don't match photos with the related
     * keyword any longer.
     *
     ******************************************************************************************************************/
    @Bean
    public Step pruneStoppingDownStep (
        @Qualifier("stoppingDownReader") final FileScanItemReader<Path> stoppingDownReader,
        @Qualifier("redundantPublishedKeywordFilter") final ItemProcessor<Path, Path> redundantPublishedKeywordFilter,
        final DeleteFileItemWriter deleteFileWriter)
      {
        return createStep("pruneStoppingDownStep",
                          stoppingDownReader,
                          redundantPublishedKeywordFilter,
                          deleteFileWriter);
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Bean
    public AccumulatorWriter<Photo> photosAccumulatorWriter()
      {
        return new AccumulatorWriter<>("photos");
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Bean @StepScope
    public DeleteFileItemWriter deleteFileWriter (@Nonnull final GeneralOptions generalOptions)
      {
        return new DeleteFileItemWriter(settings.getHome(), generalOptions, listeners.newDeleteListener());
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Bean
    public LoadKeywordsItemProcessor loadKeywordsProcessor()
      {
        return new LoadKeywordsItemProcessor(listeners.newLoadKeywordsListener());
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Bean
    public ItemProcessor<Path, Path> redundantThmFilter()
      {
        return new FilterItemProcessor<>(() -> isThmWhoseNameIsNotContainedIn(getPhotoNames()));
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Bean
    public ItemProcessor<Path, Path> redundantLongTermArchivalFilter()
      {
        return new FilterItemProcessor<>(() -> isHeicOrJpegWhoseNameIsNotContainedIn(getPhotoNames()));
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Bean
    public ItemProcessor<Path, Path> redundantBackgroundKeywordFilter()
      {
        return new FilterItemProcessor<>(() ->
                        doesntContainKeyword(photoMapByName(), K_BACKGROUNDS).or(isJpegReplacedByHeic()));
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Bean
    public ItemProcessor<Path, Path> redundantPublishedKeywordFilter()
      {
        return new FilterItemProcessor<>(() -> doesntContainKeyword(photoMapByName(), K_STOPPINGDOWN));
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Nonnull
    private Map<String, Photo> photoMapByName()
      {
        final var photos = (List<Photo>)AccumulatorWriter.MAP.get("photos");
        assert photos != null;
        return photos.stream().collect(toMap(Photo::getName, p -> p, (a, b) -> a, TreeMap::new));
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Nonnull
    private static List<String> getPhotoNames()
      {
        final var photos = (List<Photo>)AccumulatorWriter.MAP.get("photos");
        assert photos != null;
        return photos.stream().map(Photo::getName).toList();
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Nonnull
    private static Predicate<Path> isHeicOrJpegWhoseNameIsNotContainedIn (@Nonnull final Collection<String> fileNames)
      {
        return p -> (isHeic(p) || isJpeg(p)) && !fileNames.contains(cleaned(nameOf(p)));
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Nonnull
    private static Predicate<Path> isThmWhoseNameIsNotContainedIn (@Nonnull final Collection<String> fileNames)
      {
        return p -> isThm(p) && !fileNames.contains(nameOf(p));
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Nonnull
    private static Predicate<Path> doesntContainKeyword (@Nonnull final Map<String, Photo> photoMap,
                                                         @Nonnull final String keywordPath)
      {
        return p -> (isJpeg(p) || isHeic(p)) &&
                    Optional.ofNullable(photoMap.get(cleaned(nameOf(p))))
                            .flatMap(i -> i.getMetadata().getKeywordSet().findKeywordByPath(keywordPath))
                            .isEmpty();
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Nonnull
    private static Predicate<Path> isJpegReplacedByHeic()
      {
        return _p(p -> isJpeg(p) && heicExistsWithSimilarName(p));
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    private static boolean heicExistsWithSimilarName (@Nonnull final Path file)
            throws IOException
      {
        final var heicFile = Path.of(cleaned(nameOf(file)) + ".heic");
        return findFiles(file.getParent())
                .stream()
                .anyMatch(p -> isHeic(p) && cleaned(nameOf(p)).equals(cleaned(nameOf(heicFile))));
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Nonnull
    private static String cleaned (@Nonnull final String name)
      {
        return name.replaceAll("@[0-9,%]+%", "");
      }

    /*******************************************************************************************************************
     *
     * Convenience method for creating a step with the common configuration.
     *
     * @param     <T>           the static input record type
     * @param     <S>           the static output record type
     * @param     name          the name of the step
     * @param     reader        the item reader
     * @param     processor     the item processor
     * @param     writer        the item writer
     * @return                  the new step
     *
     ******************************************************************************************************************/
    @Nonnull
    private <T, S> Step createStep (@Nonnull final String name,
                                    @Nonnull final ItemReader<T> reader,
                                    @Nonnull final ItemProcessor<T, S> processor,
                                    @Nonnull final ItemWriter<S> writer)
      {
        return new StepBuilder(name, jobRepository)
                .listener(listeners.newBatchListener())
                .<T, S>chunk(chunkSize, transactionManager)
                .reader(reader)
                .processor(processor)
                .writer(writer)
                .faultTolerant()
                .skip(Exception.class)
                .noRetry(Exception.class)
                .skipLimit(Integer.MAX_VALUE)
                .taskExecutor(taskExecutor)
                .throttleLimit(throttleLimit)
                .build();
      }
  }

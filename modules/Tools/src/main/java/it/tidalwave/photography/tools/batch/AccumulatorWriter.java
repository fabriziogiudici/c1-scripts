/*
 * *********************************************************************************************************************
 *
 * Photography Scripts
 * http://stoppingdown.net
 *
 * Copyright (C) 2021 - 2023 by Tidalwave s.a.s. (http://tidalwave.it)
 *
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * git clone https://bitbucket.org/fabriziogiudici/c1-scripts
 *
 * *********************************************************************************************************************
 */
package it.tidalwave.photography.tools.batch;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.core.listener.ExecutionContextPromotionListener;
import org.springframework.batch.item.Chunk;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemStream;
import org.springframework.batch.item.ItemWriter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/***********************************************************************************************************************
 *
 **********************************************************************************************************************/
@RequiredArgsConstructor @Slf4j
public class AccumulatorWriter<T> implements ItemWriter<T>, ItemStream
  {
    public static final Map<String, Object> MAP = Collections.synchronizedMap(new HashMap<>()); // FIXME

    @Nonnull
    private final String keyName;

    private transient List<T> items;

    @BeforeStep
    public void beforeStep (@Nonnull final StepExecution stepExecution)
      {
        final var executionContext = stepExecution.getExecutionContext();
        MAP.put(keyName, items = Collections.synchronizedList(new ArrayList<T>()));
        // executionContext.put(keyName, items); FIXME
      }

    @Override
    public void open (@Nonnull final ExecutionContext executionContext)
      {
        items.clear();
      }

    @Override
    public void write (@Nonnull final Chunk<? extends T> chunk)
      {
        items.addAll(chunk.getItems());
      }

    @Override
    public void close()
      {
        MAP.put(keyName, items); // FIXME: should use promotion listener, but there are serialization problems
      }

    @Nonnull
    public ExecutionContextPromotionListener getPromotionListener()
      {
        final var listener = new ExecutionContextPromotionListener();
        listener.setKeys(new String[]{ keyName });
        listener.setStrict(false);
        return listener;
      }
  }

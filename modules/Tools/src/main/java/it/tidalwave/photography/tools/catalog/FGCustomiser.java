/*
 * *********************************************************************************************************************
 *
 * Photography Scripts
 * http://stoppingdown.net
 *
 * Copyright (C) 2021 - 2023 by Tidalwave s.a.s. (http://tidalwave.it)
 *
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * git clone https://bitbucket.org/fabriziogiudici/c1-scripts
 *
 * *********************************************************************************************************************
 */
package it.tidalwave.photography.tools.catalog;

import javax.annotation.Nonnull;
import java.util.Arrays;
import java.util.List;
import it.tidalwave.captureone.C1;
import it.tidalwave.photography.core.keywords.Keyword;
import it.tidalwave.photography.core.keywords.KeywordContainer;
import it.tidalwave.util.Pair;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import static java.util.stream.Collectors.*;
import static it.tidalwave.util.CollectionUtils.*;

/***********************************************************************************************************************
 *
 **********************************************************************************************************************/
@Slf4j
public class FGCustomiser implements C1CollectionGenerator.Customiser
  {
    private static final String AAT = "Art & Architecture Thesaurus® +";
    private static final String AAT_LANDSCAPE = "aat:landscapes (environments)";
    private static final String AAT_VEHICLES = "aat:vehicles (transportation)";

    private static final String COLL_ARCHITECTURE = "Architecture";
    private static final String COLL_HIERARCHICAL = "Hierarchical";
    private static final String COLL_LANDSCAPE = "Landscape";
    private static final String COLL_VEHICLES = "Vehicles";

    private static final List<Pair<String, List<String>>> geoPairs = pairsOf(
        """
        Architecture Items/Churches and other religious buildings -> church, monastery, abbey, cathedral, baptistery, \
                                                                     basilica, chapel, hermitage, oratory, priory
        Architecture Items/Castles and other military buildings   -> castle, fort, citadel, tower
        Architecture Items/Bridges                                -> bridge
        Architecture Items/Lighthouses                            -> lighthouse
        Architecture Items/Inhabited places                       -> town, village, hamlet
        Geography/Mountains                                       -> mountain, massif, mountain range, mountain pass
        Geography/Valleys                                         -> valley, plateau, canyon
        Geography/Rivers                                          -> river, creek, canal
        Geography/Lakes                                           -> lake
        Geography/Seas                                            -> sea
        Geography/Islands                                         -> island
        """);

    private static final List<Pair<String, List<String>>> taxoPairs = pairsOf(
        """
        Plants         -> kingdom:Plantae [Acta plantarum]
        Amphibians     -> kingdom:Animalia [Wikipedia], phylum:Chordata, class:Amphibia
        Reptiles       -> kingdom:Animalia [Wikipedia], phylum:Chordata, class:Reptilia
        Birds          -> kingdom:Animalia [Wikipedia], phylum:Chordata, class:Aves [EBN Italia 2019-02]
        Mammals        -> kingdom:Animalia [Wikipedia], phylum:Chordata, class:Mammalia
        Butterflies    -> kingdom:Animalia [Wikipedia], phylum:Arthropoda, order:Lepidoptera
        Invertebrates  -> kingdom:Animalia [Wikipedia], phylum:Arthropoda
        """);

    @Override
    public void onInit (@Nonnull final C1 c1, @Nonnull final String catalogName)
      {
        this.catalogName = catalogName;
        c1.createGroupCollection(catalogName, AAT);
        c1.createGroupCollection(AAT, COLL_ARCHITECTURE);
        c1.createGroupCollection(AAT, COLL_HIERARCHICAL);
        c1.createGroupCollection(AAT, COLL_LANDSCAPE);
        c1.createGroupCollection(AAT, COLL_VEHICLES);
        c1.createGroupCollection(catalogName, "Architecture Items");
        c1.createGroupCollection(catalogName, "Biology");
        c1.createFilterCollection("Biology", "dubious", "taxonomy:dubious");
        c1.createFilterCollection("Biology", "vetted", "taxonomy:vetted");
        c1.createFilterCollection("Biology", "unresolved", "taxonomy:unresolved");
        c1.createGroupCollection("Biology", "Vernacular names");
        c1.createGroupCollection("Biology", "Scientific names");
        c1.createGroupCollection("Vernacular names", "Plants");
        c1.createGroupCollection("Vernacular names", "Animals");
        c1.createGroupCollection("Animals", "Invertebrates");
        c1.createGroupCollection("Animals", "Amphibians");
        c1.createGroupCollection("Animals", "Reptiles");
        c1.createGroupCollection("Animals", "Birds");
        c1.createGroupCollection("Animals", "Mammals");
        c1.createGroupCollection("Invertebrates", "Butterflies");
        c1.createGroupCollection(catalogName, "Geography");
        geoPairs.stream().map(Pair::getA).sorted().map(s -> s.split("/")).forEach(s -> c1.createGroupCollection(s[0], s[1]));
        c1.setProgress(0); // reset base time
      }

    @Nonnull
    private String catalogName = "";

    @Override
    public boolean test (@Nonnull final C1 c1, @Nonnull final Keyword keyword)
      {
        return !keyword.getPath().getFirst().equals("Places");
      }

    @Override
    public void onKeyword (@Nonnull final C1 c1, @NonNull final KeywordContainer parent, @Nonnull final Keyword keyword)
      {
        var parentName = parent instanceof Keyword ? ((Keyword)parent).getName() : catalogName;
        var name = keyword.getName();
        final var path = keyword.getPath();
        final var root = path.getFirst();
        final var hasChildren = !keyword.children().isEmpty();
        final var originalName = name;

        if (root.equals(AAT))
          {
            // if (name.endsWith(" (hierarchy name)") || name.startsWith("<") || name.equals(AAT)) // FIXME: they
            //  have children
            if (name.equals(AAT))
              {
                return;
              }

            name = name.replaceAll("^aat:", "").replaceAll("#", " — ");
            final var flatParentName = path.contains(AAT_LANDSCAPE)
                                             ? COLL_LANDSCAPE
                                             : path.contains(AAT_VEHICLES)
                                               ? COLL_VEHICLES
                                               : COLL_ARCHITECTURE;
            c1.createFilterCollection(flatParentName, name, originalName);

            if (parentName.equals(AAT))
              {
                parentName = COLL_HIERARCHICAL;
              }
          }

        if (hasChildren)
          {
            c1.createGroupCollection(parentName, originalName, name);
          }

        c1.createFilterCollection(hasChildren ? originalName : parentName, hasChildren ? "_All" : name, originalName);

        if (originalName.contains(":"))
          {
            final var split = originalName.split(":");
            final var className = split[0];
            final var objectName = split[1];

            if (root.equals("geo tags"))
              {
                geoPairs.stream()
                        .filter(p -> p.b.contains(className))
                        .findFirst()
                        .ifPresent(p -> c1.createFilterCollection(p.a.split("/")[1],
                                                                   objectName + geoInfo(path),
                                                                   originalName,
                                                                   keyword.getParent().map(Keyword::getName).orElse("")));
              }
            else if (root.equals("Taxonomy") && (className.equals("scientific name")))
              {
                try
                  {
                    var vernacularNames = " (" + String.join(", ", sorted(keyword.getSynonyms())) + ")";
                    c1.createFilterCollection("Scientific names",objectName + vernacularNames, originalName);
                    var parent2 = taxoPairs.stream()
                                          .filter(p -> path.containsAll(p.b))
                                          .findFirst()
                                          .map(Pair::getA)
                                          .orElseThrow(() -> new RuntimeException("Can't assign parent:" + path));
                    keyword.getSynonyms().stream()
                           .map(s -> objectName.endsWith("?") ? s + "?" : s)
                           .forEach(s -> c1.createFilterCollection(parent2, s + " (" + objectName + ")", originalName));
                  }
                catch (RuntimeException e)
                  {
                    log.warn("{}", e.toString());
                  }
              }
          }
      }

    @Nonnull
    private static String geoInfo (@Nonnull final List<String> path)
      {
        var geoClasses = List.of("country", "province", "municipality");
        var last = path.getLast().split(":");
        return reversed(path).stream()
                   .map(s -> s.split(":"))
                   .filter(n -> geoClasses.contains(n[0]))
                   .filter(n -> !n[1].equals(last[1]))
                   .map(s -> s[1].replaceAll("Provincia di ", ""))
                   .distinct()
                   .collect(joining(", ", " (", ")"))
                   .replaceAll(" \\(\\)", "");
      }

    @Nonnull
    private static List<Pair<String, List<String>>> pairsOf (@Nonnull final String config)
      {
        return Arrays.stream(config.split("\n"))
                     .map(s -> s.split("->"))
                     .map(s -> Pair.of(s[0].trim(),
                                   Arrays.asList(s[1].split(",")).stream().map(String::trim).toList()))
                     .toList();
      }
  }

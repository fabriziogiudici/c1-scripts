package it.tidalwave.photography.tools;

import javax.annotation.Nonnull;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.support.DefaultTransactionStatus;
import javax.sql.DataSource;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j @NoArgsConstructor
public class LoggingDataSourceTransactionManager extends DataSourceTransactionManager
  {
    public LoggingDataSourceTransactionManager (@Nonnull final DataSource dataSource)
      {
        super(dataSource);
      }

    @Override
    protected void doBegin (@Nonnull final Object transaction, @Nonnull final TransactionDefinition definition)
      {
        log.debug("TX BEGIN");
        super.doBegin(transaction, definition);
      }

    @Override
    protected void doCommit (@Nonnull final DefaultTransactionStatus status)
      {
        log.debug("TX COMMIT");
        super.doCommit(status);
      }

    @Override
    protected void doRollback (@Nonnull final DefaultTransactionStatus status)
      {
        log.debug("TX ROLLBACK");
        super.doRollback(status);
      }
  }


/*
 * *********************************************************************************************************************
 *
 * Photography Scripts
 * http://stoppingdown.net
 *
 * Copyright (C) 2021 - 2023 by Tidalwave s.a.s. (http://tidalwave.it)
 *
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * git clone https://bitbucket.org/fabriziogiudici/c1-scripts
 *
 * *********************************************************************************************************************
 */
package it.tidalwave.photography.tools.prune;

import javax.annotation.Nonnull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import it.tidalwave.photography.tools.ConsoleListeners;
import it.tidalwave.photography.tools.Listeners;
import it.tidalwave.photography.tools.Settings;

/***********************************************************************************************************************
 *
 **********************************************************************************************************************/
@Configuration
public class CommonConfiguration
  {
    @Bean
    public Listeners listeners (@Nonnull final Settings settings)
      {
        return new ConsoleListeners(System.err, settings.getPhotos());
      }

    @Bean
    public Settings settings()
      {
        return new Settings();
      }

    @Bean
    public GeneralOptions generalOptions()
      {
        return new DefaultGeneralOptions();
      }
  }

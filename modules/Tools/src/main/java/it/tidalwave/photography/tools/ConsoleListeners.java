/*
 * *********************************************************************************************************************
 *
 * Photography Scripts
 * http://stoppingdown.net
 *
 * Copyright (C) 2021 - 2023 by Tidalwave s.a.s. (http://tidalwave.it)
 *
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * git clone https://bitbucket.org/fabriziogiudici/c1-scripts
 *
 * *********************************************************************************************************************
 */
package it.tidalwave.photography.tools;

import javax.annotation.Nonnull;
import java.time.Duration;
import java.util.Collection;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.io.PrintStream;
import java.nio.file.Path;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.AfterJob;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.core.annotation.OnSkipInProcess;
import org.springframework.batch.core.annotation.OnSkipInRead;
import it.tidalwave.io.FileUtils;
import it.tidalwave.photography.tools.model.Photo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/***********************************************************************************************************************
 *
 * Listeners for the console.
 *
 **********************************************************************************************************************/
@RequiredArgsConstructor @Slf4j
public class ConsoleListeners implements Listeners
  {
    private static final String ERASE_EOL = "\u001b[0K";

    private final Path home = Path.of(System.getProperty("user.home"));

    @Nonnull
    private final PrintStream ps;

    @Nonnull
    private final Path basePath;

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override @Nonnull
    public FileUtils.ScanListener<Collection<?>> newDiscoverListener()
      {
        return new FileUtils.FunctionalScanListener<>()
          {
            private final AtomicInteger count = new AtomicInteger(0);

            {
              setStarting(() -> ps.printf("  Discovering files...%s\n", ERASE_EOL));
              setProcessingRoot(r -> ps.printf("    Discovering in ~/%s ...%s\n", home.relativize(r), ERASE_EOL));
              setProgress((r, p) -> ps.printf("%d: %s%s\r", count.incrementAndGet(), basePath.relativize(p), ERASE_EOL));
              setCompleted(r -> ps.printf("%s\r    Found %d files.\n", ERASE_EOL, count.get()));
            }
          };
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override @Nonnull
    public FileUtils.ScanListener<TreeMap<String, Photo>> newLoadKeywordsListener()
      {
        return new FileUtils.FunctionalScanListener<>()
          {
            private final AtomicInteger count = new AtomicInteger(0);

            {
              setStarting(() -> ps.printf("Loading keywords...%s\n", ERASE_EOL));
              setProcessingRoot(r -> ps.printf("Loading from ~/%s ...%s\n", home.relativize(r), ERASE_EOL));
              setProgress((r, p) -> ps.printf("%d: Loading keywords from: ~/%s%s\r",
                                              count.incrementAndGet(),
                                              home.relativize(p),
                                              ERASE_EOL));
              setCompleted(r -> ps.printf("\r  Loaded %d sets of keywords.%s\n", count.get(), ERASE_EOL));
            }
          };
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override @Nonnull
    public FileUtils.ScanListener<Long> newDeleteListener()
      {
        return new FileUtils.FunctionalScanListener<>()
          {
            private final AtomicInteger count = new AtomicInteger();

            {
              setStarting(() -> ps.printf("  Deleting files...%s\n", ERASE_EOL));
              setProcessingRoot(r -> ps.printf("  Deleting files in ~/%s ...%s\n", home.relativize(r), ERASE_EOL));
              setProgress((r, p) -> ps.printf("    Delete #%d: ~/%s%s\n",
                                              count.incrementAndGet(),
                                              home.relativize(r.resolve(p)),
                                              ERASE_EOL));
              setCompleted(__ -> ps.printf("    Deleted %d files.%s\n", count.get(), ERASE_EOL));
            }
          };
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    class BatchListener
      {
        @AfterJob
        public void afterJob (@Nonnull final JobExecution jobExecution)
          {
            final var duration = Duration.between(jobExecution.getStartTime(), jobExecution.getEndTime()).getSeconds();
            ps.printf("Completed with status %s in %d' %02d\".%s\n",
                              jobExecution.getStatus(),
                              duration / 60,
                              duration % 60,
                              ERASE_EOL);
          }

        @BeforeStep
        public void beforeStep (@Nonnull final StepExecution stepExecution)
          {
            ps.printf("Starting step: %s%s\n", stepExecution.getStepName(), ERASE_EOL);
          }

        @OnSkipInRead
        public void onSkipInRead (@Nonnull final Throwable t)
          {
            t.printStackTrace(); // FIXME
          }

        @OnSkipInProcess
        public void skipped (@Nonnull final Photo photo, @Nonnull final Throwable t)
          {
            log.warn("Skipped: {} - {}", photo.getPath(), t.toString());
            ps.printf("  Skipped %s: %s%s\n", photo.getName(), t, ERASE_EOL);
          }
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override @Nonnull
    public Object newBatchListener ()
      {
        return new BatchListener();
      }
  }

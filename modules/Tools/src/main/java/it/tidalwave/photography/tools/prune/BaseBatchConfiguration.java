/*
 * *********************************************************************************************************************
 *
 * Photography Scripts
 * http://stoppingdown.net
 *
 * Copyright (C) 2021 - 2023 by Tidalwave s.a.s. (http://tidalwave.it)
 *
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * git clone https://bitbucket.org/fabriziogiudici/c1-scripts
 *
 * *********************************************************************************************************************
 */
package it.tidalwave.photography.tools.prune;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.SQLException;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.batch.BatchDataSourceScriptDatabaseInitializer;
import org.springframework.boot.autoconfigure.batch.BatchProperties;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import it.tidalwave.photography.tools.LoggingDataSourceTransactionManager;
import javax.sql.DataSource;
import it.tidalwave.util.PreferencesHandler;
import it.tidalwave.util.impl.DefaultPreferencesHandler;
import lombok.extern.slf4j.Slf4j;

/***********************************************************************************************************************
 *
 * The base configuration for Spring Batch.
 *
 **********************************************************************************************************************/
@Configuration
@EnableBatchProcessing(dataSourceRef = "dataSource", transactionManagerRef = "transactionManager")
@Slf4j
public class BaseBatchConfiguration
  {
    @Value("${throttleLimit:16}") // FIXME: set default to processor count
    protected int throttleLimit;

    @Value("${dbPathPrefix}")
    private String dbPathPrefix;

    @Value("${dbPathSuffix}")
    private String dbPathSuffix;

    /*@Override @Nonnull
    protected ExecutionContextSerializer getExecutionContextSerializer()
      {
        final var serializer = new Jackson2ExecutionContextStringSerializer();
        final var objectMapper = new ObjectMapper();
        // objectMapper.addMixIn()
        serializer.setObjectMapper(objectMapper);
        return serializer;
      }*/

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Bean
    public DataSource dataSource (@Nonnull final DataSourceProperties properties,
                                  @Nonnull final PreferencesHandler preferencesHandler)
            throws IOException
      {
        final var path = preferencesHandler.getAppFolder().resolve("spring-batch.db").toAbsolutePath();
        final var dbUrl = dbPathPrefix + path.toString() + dbPathSuffix;
        Files.createDirectories(path.getParent());
        properties.setUrl(dbUrl);
        return properties.initializeDataSourceBuilder().build();
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Bean @ConfigurationProperties(prefix = "spring.batch")
    public BatchProperties batchProperties()
      {
        return new BatchProperties();
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Bean // Needed to initialize Spring Batch database, used because spring.batch.job.enabled=false
    public BatchDataSourceScriptDatabaseInitializer batchDataSourceInitializer (@Nonnull final DataSource dataSource,
                                                                                @Nonnull final BatchProperties properties)
      {
        return new BatchDataSourceScriptDatabaseInitializer(dataSource, properties.getJdbc())
          {
            @Override
            public void afterPropertiesSet()
                    throws Exception
              {
                if (!databaseInitialised(dataSource))
                  {
                    super.afterPropertiesSet();
                  }
              }
          };
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Bean
    public DataSourceTransactionManager transactionManager (@Nonnull final DataSource dataSource)
      {
        return new LoggingDataSourceTransactionManager(dataSource);
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Bean
    public TaskExecutor taskExecutor()
      {
        final var executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(throttleLimit);
        executor.setMaxPoolSize(throttleLimit);
        return executor;
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Bean
    public PreferencesHandler preferencesHandler()
      {
        return new DefaultPreferencesHandler();
      }

    /*******************************************************************************************************************
     *
     * Checks whether Spring Batch database has been already initialised.
     *
     ******************************************************************************************************************/
    private static boolean databaseInitialised (@Nonnull final DataSource dataSource)
            throws SQLException
      {
        try (final var connection = dataSource.getConnection())
          {
            final var meta = connection.getMetaData();
            final var resultSet = meta.getTables(null, null, "BATCH_JOB_INSTANCE", new String[] {"TABLE"});
            final var dbInitialized = resultSet.next();
            log.info("Database initialized: {}", dbInitialized);
            return dbInitialized;
          }
      }
  }

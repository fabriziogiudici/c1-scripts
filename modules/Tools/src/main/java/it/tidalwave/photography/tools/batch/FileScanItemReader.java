/*
 * *********************************************************************************************************************
 *
 * Photography Scripts
 * http://stoppingdown.net
 *
 * Copyright (C) 2021 - 2023 by Tidalwave s.a.s. (http://tidalwave.it)
 *
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * git clone https://bitbucket.org/fabriziogiudici/c1-scripts
 *
 * *********************************************************************************************************************
 */
package it.tidalwave.photography.tools.batch;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.nio.file.FileVisitOption;
import java.nio.file.Path;
import org.springframework.batch.item.support.AbstractItemCountingItemStreamItemReader;
import it.tidalwave.io.FileUtils;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import static java.util.Comparator.*;
import static java.nio.file.FileVisitOption.FOLLOW_LINKS;

/***********************************************************************************************************************
 *
 * An item reader that scans files.
 *
 **********************************************************************************************************************/
@Builder @Slf4j
public class FileScanItemReader<T> extends AbstractItemCountingItemStreamItemReader<T>
  {
    @Nonnull
    private final String name;

    @Builder.Default @Nonnull
    private final List<Path> scanRoots = List.of();

    @Builder.Default @Nonnull
    private final Predicate<Path> filter = p -> true;

    @Builder.Default @Nonnull
    private Function<Path, T> mapper = p -> (T)p;

    @Builder.Default @Nonnull
    private final Collector<Path, ?, List<T>> collector = (Collector)Collectors.toList();

    @Builder.Default @Nonnull
    private final FileUtils.ScanListener<List<T>> listener = new FileUtils.FunctionalScanListener<>();

    @Builder.Default @Nonnull
    private FileVisitOption visitOption = FOLLOW_LINKS;

    private Iterator<T> iterator; // FIXME: remove from builder

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override
    protected void doOpen()
      {
        super.setName(name);
        final var items = FileUtils.<List<T>>scan(scanRoots)
                                    .withVisitOptions(visitOption)
                                    .withFilter(filter)
                                    .withListener(listener)
                                    .withSorting(comparing(Path::toAbsolutePath))
                                    .withCollector(collector)
                                    .run();
        iterator = items.iterator();
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override @Nullable
    protected T doRead()
      {
        return iterator.hasNext() ? iterator.next() : null;
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override
    protected void doClose()
      {
      }
  }

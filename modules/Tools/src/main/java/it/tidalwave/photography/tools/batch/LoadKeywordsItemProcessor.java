/*
 * *********************************************************************************************************************
 *
 * Photography Scripts
 * http://stoppingdown.net
 *
 * Copyright (C) 2021 - 2023 by Tidalwave s.a.s. (http://tidalwave.it)
 *
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * git clone https://bitbucket.org/fabriziogiudici/c1-scripts
 *
 * *********************************************************************************************************************
 */
package it.tidalwave.photography.tools.batch;

import javax.annotation.Nonnull;
import java.util.Optional;
import java.util.TreeMap;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.AfterStep;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemProcessor;
import it.tidalwave.io.FileUtils;
import it.tidalwave.photography.tools.model.Photo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/***********************************************************************************************************************
 *
 * An item processor that loads keywords.
 *
 **********************************************************************************************************************/
@RequiredArgsConstructor @Slf4j
public class LoadKeywordsItemProcessor implements ItemProcessor<Photo, Photo>
  {
    @Nonnull
    private final FileUtils.ScanListener<TreeMap<String, Photo>> listener;

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @BeforeStep
    public void beforeStep (@Nonnull final StepExecution __)
      {
        log.info("Loading keywords...");
        listener.starting();
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @AfterStep
    public void afterStep (@Nonnull final StepExecution __)
      {
        log.info("Keywords loaded.");
        listener.completed(Optional.empty()); // FIXME: optional
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override
    public Photo process (@Nonnull final Photo photo)
            throws Exception
      {
        log.debug("process({})", photo.getPath());
        final var path = photo.getPath();
        listener.progress(path.getParent(), path);
        photo.getMetadata().loadKeywords();
        return photo;
      }
  }


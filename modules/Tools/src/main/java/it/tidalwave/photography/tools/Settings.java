/*
 * *********************************************************************************************************************
 *
 * Photography Scripts
 * http://stoppingdown.net
 *
 * Copyright (C) 2021 - 2023 by Tidalwave s.a.s. (http://tidalwave.it)
 *
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * git clone https://bitbucket.org/fabriziogiudici/c1-scripts
 *
 * *********************************************************************************************************************
 */
package it.tidalwave.photography.tools;

import java.util.List;
import java.util.Set;
import java.nio.file.Path;
import lombok.Getter;

@Getter
public final class Settings
  {
    private final Path home = Path.of(System.getProperty("user.home"));

    private final Path photos = home.resolve("Personal/Photography/Photos");

    private final Path backgrounds = home.resolve("Pictures/Backgrounds");

    private final Path stoppingDownImages =
            home.resolve("Personal/WebSites/StoppingDown.net/ExternalMedia/stillimages");

    private final List<Path> photoRoots = List.of(photos.resolve("Digital"), photos.resolve("Incoming"));

    private final Path longTermArchivalRoots = photos.resolve("Archived/HEIC");

    private final Set<String> photoExtensions = Set.of("nef", "arw", "dng", "tif", "tiff", "jpg", "heic");
  }

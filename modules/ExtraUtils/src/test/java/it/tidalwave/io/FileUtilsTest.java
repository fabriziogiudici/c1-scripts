/*
 * *********************************************************************************************************************
 *
 * Photography Scripts
 * http://stoppingdown.net
 *
 * Copyright (C) 2021 - 2023 by Tidalwave s.a.s. (http://tidalwave.it)
 *
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * git clone https://bitbucket.org/fabriziogiudici/c1-scripts
 *
 * *********************************************************************************************************************
 */
package it.tidalwave.io;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import static java.util.stream.Collectors.*;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.*;
import static it.tidalwave.util.test.FileComparisonUtils.assertSameContents;

/***********************************************************************************************************************
 *
 *
 *
 **********************************************************************************************************************/
public class FileUtilsTest
  {
    private static final String TEST_COLLECTOR_TO_LIST = "test_collector_toList.txt";

    private static final String TEST_COLLECTOR_TO_LIST_EVENTS = "test_collector_toList-events.txt";

    private static final String TEST_COLLECTOR_COUNTING_EVENTS = "test_collector_counting-events.txt";

    private Path base;

    private Path files;

    private Path expectedResults;

    private Path actualResults;

    private StringWriter events;

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @BeforeClass
    public void init()
            throws IOException
      {
        base = Optional.ofNullable(System.getenv("TEST_SET_20241121_1034"))
                       .map(Path::of)
                       .orElseThrow(() -> new IOException("TEST_SET_20241121_1034 not configured"));

        if (!Files.exists(base))
          {
            throw new FileNotFoundException(base.toString());
          }

        files = base.resolve("files");
        expectedResults = base.resolve("expected-results");
        actualResults = Path.of("target/test-results/");
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @BeforeMethod
    public void setup()
            throws IOException
      {
        events = new StringWriter();
        Files.createDirectories(actualResults);
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Test
    public void test_with_collector()
            throws IOException
      {
        // WHEN
        var count = FileUtils.<Long>scan(files)
                             .withCollector(Collectors.counting())
                             .run();
        // THEN
        assertThat(count, is(97214L));
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Test
    public void test_with_filter_and_collector_1()
            throws IOException
      {
        // WHEN
        var count = FileUtils.<Long>scan(files)
                             .withFilter(Files::isRegularFile)
                             .withCollector(counting())
                             .run();
        // THEN
        assertThat(count, is(96222L));
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Test
    public void test_with_filter_and_collector_2()
            throws IOException
      {
        // WHEN
        var count = FileUtils.<Long>scan(files)
                             .withFilter(p -> p.getFileName().toString().endsWith(".NEF"))
                             .withCollector(counting())
                             .run();
        // THEN
        assertThat(count, is(26777L));
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Test
    public void test_with_filter_and_consumer_1()
            throws IOException
      {
        // GIVEN
        var files = new ArrayList<Path>();
        // WHEN
        FileUtils.<Void>scan(this.files)
                .withFilter(Files::isRegularFile)
                .withConsumer(files::add)
                .run();
        // THEN
        assertThat(files.size(), is(96222));
        var name = "test_with_filter_and_consumer_1-events.txt";
        var actualEventsPath = actualResults.resolve(name);
        var expectedEventsPath = expectedResults.resolve(name);
        Files.write(actualEventsPath, files.stream()
                .map(this.files::relativize)
                .map(Path::toString)
                .sorted()
                .toList());
        assertSameContents(expectedEventsPath, actualEventsPath);
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Test
    public void test_collector_counting()
            throws IOException
      {
        // WHEN
        var count = FileUtils.<Long>scan(files)
                             .withFilter(Files::isRegularFile)
                             .withSorting(Path::compareTo)
                             .withCollector(Collectors.counting())
                             .withListener(createListener(events))
                             .run();
        // THEN
        assertThat(count, is(96222L));
        var actualEventsPath = actualResults.resolve(TEST_COLLECTOR_COUNTING_EVENTS);
        var expectedEventsPath = expectedResults.resolve(TEST_COLLECTOR_COUNTING_EVENTS);
        Files.writeString(actualEventsPath, events.toString());
        assertSameContents(expectedEventsPath, actualEventsPath);
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Test
    public void test_collector_toList()
            throws IOException
      {
        // WHEN
        var paths = FileUtils.<List<Path>>scan(files)
                             .withFilter(Files::isRegularFile)
                             .withCollector(toList())
                             .withListener(createListener(events))
                             .run();
        // THEN
        var actualPath = actualResults.resolve(TEST_COLLECTOR_TO_LIST);
        var expectedPath = expectedResults.resolve(TEST_COLLECTOR_TO_LIST);
        Files.write(actualPath, paths.stream().map(Path::toString).sorted().toList());
        var actualEventsPath = actualResults.resolve(TEST_COLLECTOR_TO_LIST_EVENTS);
        var expectedEventsPath = expectedResults.resolve(TEST_COLLECTOR_TO_LIST_EVENTS);
        Files.writeString(actualEventsPath, events.toString());
        assertSameContents(expectedPath, actualPath);
        assertSameContents(expectedEventsPath, actualEventsPath);
      }

    /*******************************************************************************************************************
     *
     * Creates a listener that produces an event log.
     *
     ******************************************************************************************************************/
    @Nonnull
    private <T> FileUtils.ScanListener<T> createListener (@Nonnull final StringWriter sw)
      {
        return new FileUtils.FunctionalScanListener<T>()
          {
              {
                var pw = new PrintWriter(sw);
                setStarting(() -> pw.println("Starting"));
                setProcessingRoot(r -> pw.printf("Processing root: %s\n", base.relativize(r)));
                setProgress((r, p) -> pw.printf("Progress: %s\n", files.relativize(p)));
                setCompleted(r -> pw.printf("Completed\n"));
              }
          };
      }
  }

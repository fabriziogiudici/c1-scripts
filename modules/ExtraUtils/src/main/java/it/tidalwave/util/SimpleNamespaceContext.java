package it.tidalwave.util;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.xml.namespace.NamespaceContext;

public class SimpleNamespaceContext implements NamespaceContext
  {
    private final Map<String, String> uriToPrefixMap = new HashMap<>();
    private final Map<String, String> prefixToUriMap = new HashMap<>();

    @Override
    public String getNamespaceURI (String prefix)
      {
        System.err.println(prefix + " / " + prefixToUriMap);
        return prefixToUriMap.get(prefix);
      }

    @Override
    public String getPrefix (String namespaceURI)
      {
        System.err.println(namespaceURI + " /" + uriToPrefixMap);
        return uriToPrefixMap.get(namespaceURI);
      }

    @Override
    public Iterator<String> getPrefixes (String namespaceURI)
      {
        return prefixToUriMap.keySet().iterator();
      }

    public void register (@Nonnull final String prefix, @Nonnull final String uri)
      {
        uriToPrefixMap.put(uri, prefix);
        prefixToUriMap.put(prefix, uri);
      }
  }

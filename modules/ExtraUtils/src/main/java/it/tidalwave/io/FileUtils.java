/*
 * *********************************************************************************************************************
 *
 * Photography Scripts
 * http://stoppingdown.net
 *
 * Copyright (C) 2021 - 2023 by Tidalwave s.a.s. (http://tidalwave.it)
 *
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * git clone https://bitbucket.org/fabriziogiudici/c1-scripts
 *
 * *********************************************************************************************************************
 */
package it.tidalwave.io;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.With;
import static it.tidalwave.util.FunctionalCheckedExceptionWrappers.*;

/***********************************************************************************************************************
 *
 *
 *
 **********************************************************************************************************************/
public class FileUtils
  {
    /*******************************************************************************************************************
     *
     * An event listener for various phases in file scanning.
     *
     ******************************************************************************************************************/
    public static interface ScanListener<T>
      {
        public default void starting() {}
        public default void processingRoot (@Nonnull Path root) {}
        public default void progress (@Nonnull Path root, @Nonnull Path path) {}
        public default void completed (@Nonnull Optional<T> fileNames) {}
      }

    /*******************************************************************************************************************
     *
     * A functional specialisation of {@link ScanListener}.
     *
     ******************************************************************************************************************/
    @With @Setter @Getter @AllArgsConstructor(access = AccessLevel.PRIVATE) @NoArgsConstructor
    public static class FunctionalScanListener<T> implements ScanListener<T>
      {
        private Runnable starting = () -> {};
        private Consumer<Path> processingRoot = p -> {};
        private BiConsumer<Path, Path> progress = (r, p) -> {};
        private Consumer<Optional<T>> completed = t -> {};

        @Override
        public void starting()
          {
            starting.run();
          }

        @Override
        public void processingRoot (@Nonnull Path root)
          {
            processingRoot.accept(root);
          }

        @Override
        public void progress (@Nonnull Path root, @Nonnull Path path)
          {
            progress.accept(root, path);
          }

        @Override
        public void completed (@Nonnull Optional<T> result)
          {
            completed.accept(result);
          }
      }

    private static final ConcurrentMap<Path, List<Path>> CACHE = new ConcurrentHashMap<>();

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Nonnull
    public static <T> FileScanner<T> scan (@Nonnull final Path root, @Nonnull final FileVisitOption... options)
      {
        return new FileScanner<T>().withRoots(List.of(root));
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Nonnull
    public static <T> FileScanner<T> scan (@Nonnull final Collection<Path> roots,
                                           @Nonnull final FileVisitOption ... options)
      {
        return new FileScanner<T>().withRoots(roots);
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Nonnull
    public static List<Path> findFiles (@Nonnull Path folder)
      {
        return CACHE.computeIfAbsent(folder, _f(p -> Files.list(folder).toList())); // FIXME: stream
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Nonnull
    public static String extensionOf (@Nonnull final Path path)
      {
        var s = path.getFileName().toString();
        return s.contains(".") ? s.substring(s.lastIndexOf(".") + 1) : "";
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    @Nonnull
    public static String nameOf (@Nonnull final Path path)
      {
        var s = path.getFileName().toString();
        return s.contains(".") ? s.substring(0, s.lastIndexOf(".")) : s;
      }
  }

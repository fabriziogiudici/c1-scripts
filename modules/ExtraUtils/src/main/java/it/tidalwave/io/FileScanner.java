/*
 * *********************************************************************************************************************
 *
 * Photography Scripts
 * http://stoppingdown.net
 *
 * Copyright (C) 2021 - 2023 by Tidalwave s.a.s. (http://tidalwave.it)
 *
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * git clone https://bitbucket.org/fabriziogiudici/c1-scripts
 *
 * *********************************************************************************************************************
 */
package it.tidalwave.io;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Stream;
import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.With;
import static it.tidalwave.util.FunctionalCheckedExceptionWrappers.*;

/***********************************************************************************************************************
 *
 *
 *
 **********************************************************************************************************************/
@With @AllArgsConstructor(access = AccessLevel.PRIVATE) @NoArgsConstructor(access = AccessLevel.PACKAGE)
public class FileScanner<T>
  {
    @Nonnull
    private Collection<Path> roots = Collections.emptyList();

    @Nonnull
    private List<FileVisitOption> visitOptions_ = Collections.emptyList();

    private boolean parallelism;

    @Nonnull
    private Predicate<Path> filter = p -> true;

    @Nullable
    private Consumer<Path> consumer;

    @Nullable
    private Collector<Path, ?, T> collector;

    @Nullable
    private Comparator<Path> sorting;

    private FileUtils.ScanListener<T> listener = new FileUtils.FunctionalScanListener<>();

    /*******************************************************************************************************************
     *
     * A pair containing a root tree and a path inside that root.
     *
     ******************************************************************************************************************/
    private record RootAndPath (@Nonnull Path root, @Nonnull Path path)
      {
        @Nonnull
        static Stream<RootAndPath> walk (@Nonnull final Path root, @Nonnull final FileVisitOption ... options)
                throws IOException
          {
            return Files.walk(root, options).map(p -> new RootAndPath(root, p));
          }
      }

    /*******************************************************************************************************************
     *
     * Specifies the file visit options.
     *
     * @param     options   the options
     * @return    itself
     *
     ******************************************************************************************************************/
    @Nonnull
    public FileScanner<T> withVisitOptions (@Nonnull final FileVisitOption ... options)
      {
        return withVisitOptions_(Arrays.asList(options));
      }

    /*******************************************************************************************************************
     *
     * Runs the scanner.
     *
     * @return    a result, present only if a {@link Collector} has been associated to the scanner
     *
     * @see   #withCollector(Collector)
     *
     * Note that the {@link FileUtils.ScanListener#progress(Path, Path)} callback is invoked after sorting, if
     * requested. This means that e.g. a progress bar, in these circumstances, won't see the discovered files
     * incrementally, but all together after sorting. If this is not the desired behaviour, do not ask for sorting,
     * but collect the files first into
     * a collection, then sort on your own.
     *
     ******************************************************************************************************************/
    @Nonnull
    public T run()
      {
        final var options = visitOptions_.toArray(FileVisitOption[]::new);
        listener.starting();                                                        // call listener

        try (final Stream<RootAndPath> srap = (parallelism ? roots.parallelStream() : roots.stream())
                    .peek(listener::processingRoot)                                 // call listener for current root
                    .flatMap(_f(p -> RootAndPath.walk(p, options)))                 // walk current root
                    .filter(rap -> filter.test(rap.path)))                          // filter current path
          {
            final Stream<Path> sp1 = Optional.ofNullable(sorting)
                    .map(s -> sorted(srap, s)).orElse(srap)                         // eventually sort
                    .peek(rap -> listener.progress(rap.root, rap.path))             // call listener for current path
                    .map(rap -> rap.path);                                          // drop root
            try (final Stream<Path> sp2 = Optional.ofNullable(consumer)             // call consumer
                .map(sp1::peek).orElse(sp1))
              {
                final Optional<T> result = Optional.ofNullable(collector)
                    .map(sp2::collect).or(runForEachAndReturnEmptyOptional(sp2));   // call collector or a terminal op.
                listener.completed(result);                                         // call listener
                return result.orElse(null);
              }
          }
      }

    /*******************************************************************************************************************
     *
     * Return a sorted decorator of a {@code Stream<RootAndPath>}.
     *
     * @param     stream        the original stream
     * @param     comparator    the comparator
     * @return                  the sorted stream
     *
     ******************************************************************************************************************/
    @Nonnull
    private static Stream<RootAndPath> sorted (@Nonnull final Stream<RootAndPath> stream,
                                               @Nonnull final Comparator<Path> comparator)
      {
        return stream.sorted((rap1, rap2) -> comparator.compare(rap1.path, rap2.path));
      }

    /*******************************************************************************************************************
     *
     * Returns a supplier of a stream consumer, to make sure a terminal operation is present. The signature is designed
     * to match the parameter of {@link Optional.or()}.
     *
     * @param     stream        the stream
     * @return                  the supplier
     *
     ******************************************************************************************************************/
    @Nonnull
    private Supplier<Optional<? extends T>> runForEachAndReturnEmptyOptional (@Nonnull final Stream<Path> stream)
      {
        return () -> { stream.forEach(__ -> {}); return Optional.empty(); };
      }
  }

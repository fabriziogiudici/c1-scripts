/*
 * *********************************************************************************************************************
 *
 * Photography Scripts
 * http://stoppingdown.net
 *
 * Copyright (C) 2021 - 2023 by Tidalwave s.a.s. (http://tidalwave.it)
 *
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * git clone https://bitbucket.org/fabriziogiudici/c1-scripts
 *
 * *********************************************************************************************************************
 */
package it.tidalwave.photography.core.keywords;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import javax.xml.xpath.XPathExpressionException;
import org.xml.sax.SAXException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import static it.tidalwave.io.FileUtils.nameOf;
import static it.tidalwave.util.test.FileComparisonUtils.assertSameContents;

/***********************************************************************************************************************
 *
 **********************************************************************************************************************/
public class KeywordLoaderTest
  {
    public static final Path TEST_RESOURCES = Path.of("src/test/resources");

    public static final Path TEST_RESULTS = Path.of("target/test-results");

    private KeywordLoader underTest;

    @BeforeMethod
    public void setup()
      {
        underTest = new KeywordLoader();
      }

    @Test(dataProvider = "fileNames")
    public void test (@Nonnull final Path testFile)
            throws XPathExpressionException, IOException, SAXException
      {
        // WHEN
        var keywordSet = underTest.loadKeywords(TEST_RESOURCES.resolve("files").resolve(testFile));
        // THEN
        Files.createDirectories(TEST_RESULTS);
        var dumpFileName = nameOf(testFile) + ".txt";
        var actualFile = TEST_RESULTS.resolve(dumpFileName);
        var expectedFile = TEST_RESOURCES.resolve("expected-results").resolve(dumpFileName);

        try (var pw = new PrintWriter(Files.newBufferedWriter(actualFile)))
          {
            keywordSet.dump(pw::println);
          }

        assertSameContents(expectedFile, actualFile);
      }

    @DataProvider
    public static Object[][] fileNames()
            throws IOException
      {
        return Files.list(TEST_RESOURCES.resolve("files"))
                    .sorted()
                    .map(p -> new Object[] { p.getFileName() })
                    .toArray(Object[][]::new);
      }
  }

/*
 * *********************************************************************************************************************
 *
 * Photography Scripts
 * http://stoppingdown.net
 *
 * Copyright (C) 2021 - 2023 by Tidalwave s.a.s. (http://tidalwave.it)
 *
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * git clone https://bitbucket.org/fabriziogiudici/c1-scripts
 *
 * *********************************************************************************************************************
 */
package it.tidalwave.photography.core.keywords;

import java.util.List;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/***********************************************************************************************************************
 *
 **********************************************************************************************************************/
@Test
public class KeywordTest
  {
    private Keyword underTest;

    @BeforeMethod
    public void setup()
      {
        final var keywordSet = new KeywordSet();
        final var k1 = keywordSet.findOrCreateKeyword("k1");
        final var k2 = k1.findOrCreateKeyword("k2");
        final var k3 = k2.findOrCreateKeyword("k3");
        underTest = k3.findOrCreateKeyword("k4");
      }

    @Test
    public void toString_must_work()
      {
        // WHEN
        final var actualResult = underTest.toString();
        // THEN
        assertThat(actualResult, is("Keyword[k1/k2/k3/k4]"));
      }

    @Test
    public void test_getPathAsString()
      {
        // WHEN
        final var actualResult = underTest.getPathAsString();
        // THEN
        assertThat(actualResult, is("k1/k2/k3/k4"));
      }

    @Test
    public void test_getPath()
      {
        // WHEN
        final var actualResult = underTest.getPath();
        // THEN
        assertThat(actualResult, is(List.of("k1", "k2", "k3", "k4")));
      }
  }

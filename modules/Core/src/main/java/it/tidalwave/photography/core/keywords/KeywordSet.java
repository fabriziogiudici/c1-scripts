/*
 * *********************************************************************************************************************
 *
 * Photography Scripts
 * http://stoppingdown.net
 *
 * Copyright (C) 2021 - 2023 by Tidalwave s.a.s. (http://tidalwave.it)
 *
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * git clone https://bitbucket.org/fabriziogiudici/c1-scripts
 *
 * *********************************************************************************************************************
 */
package it.tidalwave.photography.core.keywords;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Optional;
import java.util.SortedSet;
import java.util.TreeSet;
import static java.util.Collections.unmodifiableList;

/***********************************************************************************************************************
 *
 * A set of keywords.
 *
 **********************************************************************************************************************/
public class KeywordSet extends KeywordContainerSupport
  {
    public static final KeywordSet EMPTY = new KeywordSet();

    /*******************************************************************************************************************
     * 
     * Returns the contained keywords.
     * 
     * @return      the keywords
     * 
     ******************************************************************************************************************/
    @Nonnull
    public SortedSet<Keyword> getKeywords()
      {
        return new TreeSet<>(keywordMapByName.values());
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override @Nonnull
    protected Keyword createKeyword (@Nonnull final String name, @Nonnull final List<String> synonyms)
      {
        return new Keyword(Optional.empty(), name, unmodifiableList(synonyms));
      }
  }

/*
 * *********************************************************************************************************************
 *
 * Photography Scripts
 * http://stoppingdown.net
 *
 * Copyright (C) 2021 - 2023 by Tidalwave s.a.s. (http://tidalwave.it)
 *
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * git clone https://bitbucket.org/fabriziogiudici/c1-scripts
 *
 * *********************************************************************************************************************
 */
package it.tidalwave.photography.core.keywords;

import javax.annotation.Nonnull;
import java.text.Collator;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import static java.util.Collections.unmodifiableList;
import static it.tidalwave.util.CollectionUtils.concat;

/***********************************************************************************************************************
 *
 * Models a PhotoSupreme keyword, with a name and a list of synonyms.
 *
 * The implementation is not immutable.
 *
 **********************************************************************************************************************/
@Getter @RequiredArgsConstructor
public class Keyword extends KeywordContainerSupport implements Comparable<Keyword>
  {
    @Nonnull
    private final Optional<Keyword> parent;

    @Nonnull
    private final String name;

    @Nonnull
    private final List<String> synonyms;

    /*******************************************************************************************************************
     *
     * Returns the path of this keyword.
     *
     * @return  the path
     *
     ******************************************************************************************************************/
    @Nonnull
    public List<String> getPath()
      {
        return concat(parent.map(Keyword::getPath).orElse(new ArrayList<>()), name);
      }

    /*******************************************************************************************************************
     *
     * Returns the path of this keyword.
     *
     * @return  the path
     *
     ******************************************************************************************************************/
    @Nonnull
    public String getPathAsString()
      {
        return parent.map(p -> p.getPathAsString() + "/").orElse("") + name;
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override
    public int compareTo (@Nonnull final Keyword other)
      {
        return Collator.getInstance().compare(this.getPathAsString().toLowerCase(Locale.ROOT),
                                              other.getPathAsString().toLowerCase(Locale.ROOT));
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override
    public <T> T visit (@NonNull final T context,
                        @NonNull final BiFunction<T, Keyword, T> callback,
                        @NonNull final BinaryOperator<T> accumulator)
      {
        return super.visit(callback.apply(context, this), callback, accumulator);
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override @Nonnull
    public String toString()
      {
        return "Keyword[" + getPathAsString() + (synonyms.isEmpty() ? "" : " " + synonyms) + "]";
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override @Nonnull
    protected Keyword createKeyword (@Nonnull final String name, @Nonnull final List<String> synonyms)
      {
        return new Keyword(Optional.of(this), name, unmodifiableList(synonyms));
      }
  }

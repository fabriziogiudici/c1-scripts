/*
 * *********************************************************************************************************************
 *
 * Photography Scripts
 * http://stoppingdown.net
 *
 * Copyright (C) 2021 - 2023 by Tidalwave s.a.s. (http://tidalwave.it)
 *
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * git clone https://bitbucket.org/fabriziogiudici/c1-scripts
 *
 * *********************************************************************************************************************
 */
package it.tidalwave.photography.core.keywords.impl;

import javax.annotation.Nonnull;
import java.util.Optional;
import java.util.function.Consumer;
import javax.xml.xpath.XPathExpressionException;
import org.w3c.dom.Node;

/***********************************************************************************************************************
 *
 *
 *
 **********************************************************************************************************************/
public interface XmpParser
  {
    public void forEachTagStructure (@Nonnull Consumer<Node> c)
            throws XPathExpressionException;

    public void forEachBagLi (@Nonnull Node parentNode, @Nonnull Consumer<Node> c)
            throws XPathExpressionException;

    public void forEachSubLabel (@Nonnull Node descrNode, @Nonnull Consumer<Node> c)
            throws XPathExpressionException;

    @Nonnull
    public Optional<Node> getDescription (@Nonnull Node parentNode)
            throws XPathExpressionException;

    @Nonnull
    public String getLabelNameText (@Nonnull Node descrNode)
            throws XPathExpressionException;

    @Nonnull
    public String getSynonymsText (@Nonnull Node descrNode)
            throws XPathExpressionException;
  }

/*
 * *********************************************************************************************************************
 *
 * Photography Scripts
 * http://stoppingdown.net
 *
 * Copyright (C) 2021 - 2023 by Tidalwave s.a.s. (http://tidalwave.it)
 *
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * git clone https://bitbucket.org/fabriziogiudici/c1-scripts
 *
 * *********************************************************************************************************************
 */
package it.tidalwave.photography.core.keywords;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

/***********************************************************************************************************************
 *
 * A support class for implementing both {@link KeywordSet} and {@link Keyword}.
 *
 **********************************************************************************************************************/
@RequiredArgsConstructor @ToString
public abstract class KeywordContainerSupport implements KeywordContainer
  {
    protected final Map<String, Keyword> keywordMapByName = new HashMap<>();

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Nonnull
    public synchronized Keyword findOrCreateKeyword (@Nonnull final String name, @Nonnull final List<String> synonyms)
      {
        return keywordMapByName.computeIfAbsent(name, __ -> createKeyword(name, synonyms));
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override @Nonnull
    public synchronized Optional<Keyword> findKeywordByName (@Nonnull final String name)
      {
        return Optional.ofNullable(keywordMapByName.get(name));
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override @Nonnull
    public synchronized Optional<Keyword> findKeywordByPath (@Nonnull final List<String> path)
      {
        return findKeywordByName(path.getFirst()).flatMap(k -> path.size() > 1
                                                ? k.findKeywordByPath(path.subList(1, path.size()))
                                                : Optional.of(k));
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Nonnull
    public synchronized SortedSet<Keyword> children()
      {
        return new TreeSet<>(keywordMapByName.values());
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    public synchronized void merge (@Nonnull final KeywordContainer other)
      {
        for (var otherKeyword : other.children())
          {
            var keyword = keywordMapByName.get(otherKeyword.getName());

            if (keyword == null)
              {
                otherKeyword.cloneIntoParent(this);
              }
            else
              {
                keyword.merge(otherKeyword);
              }
          }
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    public <T> T visit (@Nonnull final T context,
                        @Nonnull final BiFunction<T, Keyword, T> callback,
                        @Nonnull final BinaryOperator<T> accumulator)
      {
        final var contextHolder = new AtomicReference<>(context);
        children().forEach(c -> contextHolder.accumulateAndGet(c.visit(contextHolder.get(), callback, accumulator), accumulator));
        return contextHolder.get();
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    public void cloneIntoParent (@Nonnull final KeywordContainer parent)
      {
        visit(parent, KeywordContainer::findOrCreateKeyword);
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override
    public synchronized void dump (@Nonnull final Consumer<String> out)
      {
        record DumpContext (@Nonnull Consumer<String> out, @Nonnull String prefix)
          {
            @Nonnull
            private static DumpContext dumpKeyword (@Nonnull final DumpContext context, @Nonnull final Keyword keyword)
              {
                var synonyms = keyword.getSynonyms();
                context.out.accept(context.prefix + keyword.getName() + (synonyms.isEmpty() ? "" : " - " + synonyms));
                return new DumpContext(context.out, context.prefix + "    ");
              }
          }

        visit(new DumpContext(out, ""), DumpContext::dumpKeyword);
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override @Nonnull
    public synchronized List<String> toFullyQualifiedList()
      {
        record DumpContext (@Nonnull List<String> list, @Nonnull String prefix)
          {
            @Nonnull
            private static DumpContext dumpKeyword (@Nonnull final DumpContext context, @Nonnull final Keyword keyword)
              {
                final var s = context.prefix + "/" + keyword.getName();

                if (keyword.children().isEmpty())
                  {
                    context.list.add(s);
                  }

                return new DumpContext(context.list, s);
              }
          }

        return visit(new DumpContext(new ArrayList<>(), ""), DumpContext::dumpKeyword).list;
      }

    /*******************************************************************************************************************
     *
     * Creates a keyword instance.
     *
     * @param   name          the name of the keyword
     * @param   synonyms      the synonyms of the keyword
     * @return                the keyword
     *
     ******************************************************************************************************************/
    @Nonnull
    protected abstract Keyword createKeyword (@Nonnull String name, @Nonnull List<String> synonyms);
  }

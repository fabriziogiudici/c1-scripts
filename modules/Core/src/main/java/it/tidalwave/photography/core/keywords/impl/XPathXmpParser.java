/*
 * *********************************************************************************************************************
 *
 * Photography Scripts
 * http://stoppingdown.net
 *
 * Copyright (C) 2021 - 2023 by Tidalwave s.a.s. (http://tidalwave.it)
 *
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * git clone https://bitbucket.org/fabriziogiudici/c1-scripts
 *
 * *********************************************************************************************************************
 */
package it.tidalwave.photography.core.keywords.impl;

import javax.annotation.Nonnull;
import java.util.Optional;
import java.util.function.Consumer;
import java.io.IOException;
import java.io.InputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import javax.xml.xpath.XPathNodes;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;
import it.tidalwave.util.SimpleNamespaceContext;

/***********************************************************************************************************************
 *
 *
 *
 **********************************************************************************************************************/
public class XPathXmpParser implements XmpParser
  {
    private final Document document;
    private final DocumentBuilder builder;
    private final XPath xPath;
    private final XPathExpression exprToTagStructure;
    private final XPathExpression exprBagLi;
    private final XPathExpression exprDescription;
    private final XPathExpression exprLabelNameText;
    private final XPathExpression exprSynonymsText;
    private final XPathExpression exprSubLabels;

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    public XPathXmpParser (@Nonnull final InputStream is)
          throws SAXException, IOException
      {
        try
          {
            builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            xPath = XPathFactory.newInstance().newXPath();
            var namespaceContext = new SimpleNamespaceContext();
            namespaceContext.register("x", "adobe:ns:meta/");
            namespaceContext.register("rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
            namespaceContext.register("ics", "http://ns.idimager.com/ics/1.0/");
            xPath.setNamespaceContext(namespaceContext);
            // FIXME: "//x:xmpmeta/rdf:RDF/rdf:Description/ics:TagStructure";
            exprToTagStructure = xPath.compile("//*[local-name()='xmpmeta']/*[local-name()" +
                                               "='RDF']/*[local-name()" +
                                               "='Description']/*[local-name()='TagStructure']");
            exprBagLi = xPath.compile("*[local-name()='Bag']/*[local-name()='li']");
            exprDescription = xPath.compile("*[local-name()='Description']");
            exprLabelNameText = xPath.compile("*[local-name()='LabelName']/text()");
            exprSynonymsText = xPath.compile("*[local-name()='Synonyms']/text()");
            exprSubLabels = xPath.compile("*[local-name()='SubLabels']");
            document = builder.parse(is);
          }
        catch (ParserConfigurationException | XPathExpressionException e)
          {
            throw new ExceptionInInitializerError(e);
          }
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override
    public void forEachTagStructure (@Nonnull final Consumer<Node> c)
            throws XPathExpressionException
      {
        exprToTagStructure.evaluateExpression(document, XPathNodes.class).forEach(c);
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override
    public void forEachBagLi (@Nonnull final Node node, @Nonnull final Consumer<Node> c)
            throws XPathExpressionException
      {
        exprBagLi.evaluateExpression(node, XPathNodes.class).forEach(c);
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override
    public void forEachSubLabel (@Nonnull final Node node, @Nonnull final Consumer<Node> c)
            throws XPathExpressionException
      {
        exprSubLabels.evaluateExpression(node, XPathNodes.class).forEach(c);
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override @Nonnull
    public Optional<Node> getDescription (@Nonnull final Node node)
            throws XPathExpressionException
      {
        return Optional.ofNullable(exprDescription.evaluateExpression(node, Node.class));
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override @Nonnull
    public String getLabelNameText (@Nonnull final Node node)
            throws XPathExpressionException
      {
        return exprLabelNameText.evaluateExpression(node, String.class);
      }

    /*******************************************************************************************************************
     * {@inheritDoc}
     ******************************************************************************************************************/
    @Override @Nonnull
    public String getSynonymsText (@Nonnull final Node node)
            throws XPathExpressionException
      {
        return exprSynonymsText.evaluateExpression(node, String.class);
      }
  }

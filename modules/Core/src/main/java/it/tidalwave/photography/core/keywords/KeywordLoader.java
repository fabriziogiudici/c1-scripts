/*
 * *********************************************************************************************************************
 *
 * Photography Scripts
 * http://stoppingdown.net
 *
 * Copyright (C) 2021 - 2023 by Tidalwave s.a.s. (http://tidalwave.it)
 *
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * git clone https://bitbucket.org/fabriziogiudici/c1-scripts
 *
 * *********************************************************************************************************************
 */
package it.tidalwave.photography.core.keywords;

import javax.annotation.Nonnull;
import java.util.Arrays;
import java.util.List;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import it.tidalwave.photography.core.keywords.impl.XPathXmpParser;
import it.tidalwave.photography.core.keywords.impl.XmpParser;
import javax.xml.xpath.XPathExpressionException;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;
import static it.tidalwave.io.FileUtils.*;
import static it.tidalwave.util.FunctionalCheckedExceptionWrappers.*;

/***********************************************************************************************************************
 *
 * A utility that loads a {@link KeywordSet} from a given file.
 *
 **********************************************************************************************************************/
public class KeywordLoader
  {
    private XmpParser parser;

    /*******************************************************************************************************************
     *
     * Loads a {@link KeywordSet} from a file.
     *
     * @param path    the path of the file to load keywords from
     * @return        the {@code KeywordSet}
     *
     ******************************************************************************************************************/
    @Nonnull
    public synchronized KeywordSet loadKeywords (@Nonnull final Path path)
            throws IOException, XPathExpressionException, SAXException
      {
        var name = nameOf(path);
        var xmpPath = path.resolveSibling(name + ".xmp");
        var keywordSet = new KeywordSet();

        // FIXME: if .xmp not found, use XMP packet finder to search for an embedded XMP
        try (var is = Files.newInputStream(xmpPath))
          {
            parser = new XPathXmpParser(is);
            parser.forEachTagStructure(_c(node -> processBagLi(node, keywordSet)));
          }

        return keywordSet;
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    private void processBagLi (@Nonnull final Node parentNode, @Nonnull final KeywordContainer parent)
            throws XPathExpressionException
      {
        parser.forEachBagLi(parentNode, _c(node -> processItem(node, parent)));
      }

    /*******************************************************************************************************************
     *
     ******************************************************************************************************************/
    private void processItem (@Nonnull final Node parentNode, @Nonnull final KeywordContainer parent)
            throws XPathExpressionException
      {
        var descrNode = parser.getDescription(parentNode).orElse(parentNode);
        var labelName = parser.getLabelNameText(descrNode);
        var synonymsAsString = parser.getSynonymsText(descrNode);
        var synonyms = synonymsAsString.equals("")
                       ? List.<String>of()
                       : Arrays.stream(synonymsAsString.split(","))
                               .map(String::trim)
                               .map(s -> s.replaceAll("^\"|\"$", ""))
                               .toList();
        var keyword = parent.findOrCreateKeyword(labelName, synonyms);
        parser.forEachSubLabel(descrNode, _c(node -> processBagLi(node, keyword)));
      }
  }

/*
 * *********************************************************************************************************************
 *
 * Photography Scripts
 * http://stoppingdown.net
 *
 * Copyright (C) 2021 - 2023 by Tidalwave s.a.s. (http://tidalwave.it)
 *
 * *********************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * *********************************************************************************************************************
 *
 * git clone https://bitbucket.org/fabriziogiudici/c1-scripts
 *
 * *********************************************************************************************************************
 */
package it.tidalwave.photography.core.keywords;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.SortedSet;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Predicate;

/***********************************************************************************************************************
 *
 * A common set of methods for {@link KeywordSet} and {@link Keyword}, as both can contain other keywords.
 *
 **********************************************************************************************************************/
public interface KeywordContainer
  {
    /*******************************************************************************************************************
     *
     * Finds an existing keyword with the given name or create a new one.
     *
     * @param name      the name of the keyword
     * @return          the keyword
     *
     ******************************************************************************************************************/
    @Nonnull
    default public Keyword findOrCreateKeyword (@Nonnull String name)
      {
        return findOrCreateKeyword(name, List.of());
      }

    /*******************************************************************************************************************
     *
     * Finds an existing keyword with the given name.
     *
     * @param name      the name of the keyword
     * @return          the optional keyword
     *
     ******************************************************************************************************************/
    @Nonnull
    public Optional<Keyword> findKeywordByName (@Nonnull String name);

    /*******************************************************************************************************************
     *
     * Finds an existing keyword with the given path.
     *
     * @param path      the path of the keyword
     * @return          the optional keyword
     *
     ******************************************************************************************************************/
    @Nonnull
    public Optional<Keyword> findKeywordByPath (@Nonnull List<String> path);

    /*******************************************************************************************************************
     *
     * Finds an existing keyword with the given path.
     *
     * @param path      the path of the keyword
     * @return          the optional keyword
     *
     ******************************************************************************************************************/
    @Nonnull
    default public Optional<Keyword> findKeywordByPath (@Nonnull final String path)
      {
        return findKeywordByPath(Arrays.asList(path.split("/")));
      }

    /*******************************************************************************************************************
     *
     * Finds an existing keyword with the given name or create a new one.
     *
     * @param name      the name of the keyword
     * @param synonyms  the synonyms of the keyword
     * @return          the keyword
     *
     ******************************************************************************************************************/
    @Nonnull
    public Keyword findOrCreateKeyword (@Nonnull String name, @Nonnull List<String> synonyms);

    /*******************************************************************************************************************
     *
     * Finds an existing keyword with the same name of another keyword or create a new one with the same attributes.
     *
     * @param keyword   the keyword to clone
     * @return          the keyword
     *
     ******************************************************************************************************************/
    @Nonnull
    default public Keyword findOrCreateKeyword (@Nonnull final Keyword keyword)
      {
        return findOrCreateKeyword(keyword.getName(), keyword.getSynonyms());
      }

    /*******************************************************************************************************************
     *
     * Returns the child keywords of this object.
     *
     * @return          the keywords
     *
     ******************************************************************************************************************/
    @Nonnull
    public SortedSet<Keyword> children();

    /*******************************************************************************************************************
     *
     * Visits all the keywords in this container. A context can be provided. The callback must be a function accepting
     * the context and the currently visited keyword; it must return a (possibly new) context for proceeding. At each
     * round the old and new context are merged by calling the accumulator.
     *
     * @param context       the visit context
     * @param callback      the callback to invoke for each contained keyword
     * @param accumulator   the accumulator
     * @return              the context
     *
     ******************************************************************************************************************/
    public <T> T visit (@Nonnull final T context,
                        @Nonnull final BiFunction<T, Keyword, T> callback,
                        @Nonnull final BinaryOperator<T> accumulator);

    /*******************************************************************************************************************
     *
     * Visits all the keywords in this container. A context can be provided, that will be passed to all the callback
     * invocations. The callback must be a function accepting the context and the currently visited keyword; it must
     * return a (possibly new) context for proceeding.
     *
     * @param context   the visit context
     * @param callback  the callback to invoke for each contained keyword
     * @return          the context
     *
     ******************************************************************************************************************/
    public default <T> T visit (@Nonnull T context, @Nonnull BiFunction<T, Keyword, T> callback)
      {
        return visit(context, callback, (a, b) -> a);
      }

    /*******************************************************************************************************************
     *
     * Counts the keywords in this container that pass the given predicate.
     *
     * @param predicate the predicate
     * @return          the count
     *
     ******************************************************************************************************************/
    @Nonnegative
    public default int count (@Nonnull final Predicate<Keyword> predicate)
      {
        return visit(0, (c, k) -> predicate.test(k) ? 1 : 0, Integer::sum);
      }

    /*******************************************************************************************************************
     *
     * Clones this container and recursively all its contents as a child of the given parent.
     *
     * @param parent    the new parent
     *
     ******************************************************************************************************************/
    public void cloneIntoParent (@Nonnull KeywordContainer parent);

    /*******************************************************************************************************************
     *
     * Dumps this object with its contents to the given string consumer. It can be easily called as
     * {@code k.dump(System.out::println);}.
     *
     * @param out       the string consumer.
     *
     ******************************************************************************************************************/
    public void dump (@Nonnull Consumer<String> out);

    @Nonnull
    public List<String> toFullyQualifiedList ();
  }
